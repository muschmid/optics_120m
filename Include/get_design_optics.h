// Include file to read design optics transport matrix elements from MadX output file
Double_t Ebeam=6800.;
Double_t dz=8264.4, dz1=8267.0, dz2=8261.8;
Double_t emxb1=2.0E-6,emyb1=2.0E-6,emxb2=2.0E-6,emyb2=2.0E-6;
Double_t gammap=Ebeam/0.938;
Double_t betax_b1,betay_b1,betax_b2,betay_b2;
Double_t betax_rp1,betay_rp1,betax_rp2,betay_rp2,betax_rp3,betay_rp3,betax_rp4,betay_rp4;
Double_t Lx1,Lx2,Lx3,Lx4,Ly1,Ly2,Ly3,Ly4;
Double_t dLx1,dLx2,dLx3,dLx4;
Double_t pi=TMath::Pi();

TMatrixD Mx1(2,2),My1(2,2),Mx2(2,2),My2(2,2),Mx3(2,2),My3(2,2),Mx4(2,2),My4(2,2);

void get_design_optics(Int_t Iver){
    ifstream optifile;
    TString Opti,Dummy;
    
    Double_t sLHC,alfax_b1,alfay_b1,alfax_b2,alfay_b2;
    Double_t alfax_rp1,alfay_rp1,alfax_rp2,alfay_rp2,alfax_rp3,alfay_rp3,alfax_rp4,alfay_rp4;
    Double_t mux_rp1,muy_rp1,mux_rp2,muy_rp2,mux_rp3,muy_rp3,mux_rp4,muy_rp4;
    if(Iver==1) Opti="/home/stenzel/madx/Elastic13p6TeV/ilias_3_6_km/twiss/optics_5003.dat";
    if(Iver==2) Opti="/home/stenzel/madx/Elastic13p6TeV/ilias_3_6_km/twiss/optics_5012.dat";
    if(Iver==3) Opti="/home/stenzel/madx/Elastic13p6TeV/optics_120m/Include/optics_120m.dat";
    if(Iver==4) Opti="/home/stenzel/madx/Elastic13p6TeV/ilias_3_6_km/twiss/optics_5017.dat";
    cout << "Opening optics file" << Opti << "\n";    
    
    optifile.open(Opti);
    
    if(optifile.is_open()){
        optifile >> Dummy >> sLHC >> betax_b2 >> betay_b2;
        cout << " IP1 B2 beta*_x = " << betax_b2 << " beta*_y = " << betay_b2 << "\n";
        
        optifile >> Dummy >> sLHC >> betax_rp1 >> betay_rp1 >> alfax_rp1 >> alfay_rp1 >> mux_rp1 >> muy_rp1;
        cout << " B7L1 beta_x = " << betax_rp1 << " beta_y = " << betay_rp1 << " mu_x = " << mux_rp1 << " mu_y = " << muy_rp1 << "\n";
        optifile >> Dummy >> Mx1[0][0] >> Mx1[0][1] >> Mx1[1][0] >> Mx1[1][1];
        optifile >> Dummy >> My1[0][0] >> My1[0][1] >> My1[1][0] >> My1[1][1];
        
        optifile >> Dummy >> sLHC >> betax_rp2 >> betay_rp2 >> alfax_rp2 >> alfay_rp2 >> mux_rp2 >> muy_rp2;
        cout << " A7L1 beta_x = " << betax_rp2 << " beta_y = " << betay_rp2 << " mu_x = " << mux_rp2 << " mu_y = " << muy_rp2 << "\n";
        optifile >> Dummy >> Mx2[0][0] >> Mx2[0][1] >> Mx2[1][0] >> Mx2[1][1];
        optifile >> Dummy >> My2[0][0] >> My2[0][1] >> My2[1][0] >> My2[1][1];
        
        optifile >> Dummy >> sLHC >> betax_b1 >> betay_b1;
        cout << " IP1 B1 beta*_x = " << betax_b1 << " beta*_y = " << betay_b1 << "\n";
        
        optifile >> Dummy >> sLHC >> betax_rp3 >> betay_rp3 >> alfax_rp3 >> alfay_rp3 >> mux_rp3 >> muy_rp3;
        cout << " A7R1 beta_x = " << betax_rp3 << " beta_y = " << betay_rp3 << " mu_x = " << mux_rp3 << " mu_y = " << muy_rp3 << "\n";
        optifile >> Dummy >> Mx3[0][0] >> Mx3[0][1] >> Mx3[1][0] >> Mx3[1][1];
        optifile >> Dummy >> My3[0][0] >> My3[0][1] >> My3[1][0] >> My3[1][1];
        
        optifile >> Dummy >> sLHC >> betax_rp4 >> betay_rp4 >> alfax_rp4 >> alfay_rp4 >> mux_rp4 >> muy_rp4;
        cout << " B7R1 beta_x = " << betax_rp4 << " beta_y = " << betay_rp4 << " mu_x = " << mux_rp4 << " mu_y = " << muy_rp4 << "\n";
        optifile >> Dummy >> Mx4[0][0] >> Mx4[0][1] >> Mx4[1][0] >> Mx4[1][1];
        optifile >> Dummy >> My4[0][0] >> My4[0][1] >> My4[1][0] >> My4[1][1];        
    }
    else cout << "Can't open design optics file" << "\n";
    optifile.close();
    
	cout << " Mx1 = " << "\n";
	Mx1.Print();
	cout << " Mx2 = " << "\n";
	Mx2.Print();
	cout << " Mx3 = " << "\n";
	Mx3.Print();
	cout << " Mx4 = " << "\n";
	Mx4.Print();
	
	cout << " My1 = " << "\n";
	My1.Print();
	cout << " My2 = " << "\n";
	My2.Print();
	cout << " My3 = " << "\n";
	My3.Print();
	cout << " My4 = " << "\n";
	My4.Print();
    
    // reset vertical emittance to 3.5 mu for distance calculation
	Double_t emy=3.5E-6;
    Double_t sigma_rp1=sqrt(betay_rp1*emy/gammap)*1E6, d_rp1=2.*(3.5*sigma_rp1+200.);
    Double_t sigma_rp2=sqrt(betay_rp2*emy/gammap)*1E6, d_rp2=2.*(3.5*sigma_rp2+200.);
    Double_t sigma_rp3=sqrt(betay_rp3*emy/gammap)*1E6, d_rp3=2.*(3.5*sigma_rp3+200.);
    Double_t sigma_rp4=sqrt(betay_rp4*emy/gammap)*1E6, d_rp4=2.*(3.5*sigma_rp4+200.);
    cout << "Vertical beam size at RPs and resulting distance of detector edge at 3.5 sigma";
	cout << " B7L1 sigma = " << sigma_rp1 << " [mu], distance = " << d_rp1 << "\n";
	cout << " A7L1 sigma = " << sigma_rp2 << " [mu], distance = " << d_rp2 << "\n";
	cout << " A7R1 sigma = " << sigma_rp3 << " [mu], distance = " << d_rp3 << "\n";
	cout << " B7R1 sigma = " << sigma_rp4 << " [mu], distance = " << d_rp4 << "\n";
    // setting the lever arms used in trec
    Lx1=Mx1[0][1];
    Lx2=Mx2[0][1];
    Lx3=Mx3[0][1];
    Lx4=Mx4[0][1];
    Ly1=My1[0][1];
    Ly2=My2[0][1];
    Ly3=My3[0][1];
    Ly4=My4[0][1];
    dLx1=Mx1[1][1],dLx2=Mx2[1][1],dLx3=Mx3[1][1],dLx4=Mx4[1][1];

}
