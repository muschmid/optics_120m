//    Double_t Lumi[7]={21841.,7017.,41971.,121529.,44617.,55466.,49625.};// v1 Hasko from COOL nominal algo 
    Double_t Lumi[7]={21453.,6858.,41433.,121049.,44644.,55272.,49412.};// v2 Benedetto 13.13.2017 LUCID_BI_AND
    Double_t life[7]={0.998334,0.998281,0.998772,0.997047,0.998609,0.998905,0.9992};// life-fraction to be applied with v2
// change             -1.97%,-2.5%,-1.5%,-0.6%,-0.15%,-0.55%,-0.63%     
    Double_t dLumi[7]={56.,32.,78.,134.,81.,90.,83.};
    Double_t Eps1[7]={0.847,0.847,0.862,0.847,0.849,0.866,0.874};//
    Double_t Eps2[7]={0.820,0.832,0.836,0.822,0.825,0.845,0.855};
    Double_t dEps1[7]={0.002,0.0025,0.0015,0.001,0.0018,0.0015,0.0015};// stat.err bootstrap
    Double_t dEps2[7]={0.002,0.0025,0.0015,0.001,0.0018,0.0015,0.0015};
//    Double_t dEps1[7]={0.0011,0.0017,0.0006,0.0006,0.0006,0.0005,0.0005};// stat.err bootstrap
//    Double_t dEps2[7]={0.0013,0.0016,0.0006,0.0005,0.0007,0.0005,0.0005};
    Double_t lumi[7]={0};
    Double_t eps1[7]={0};
    Double_t eps2[7]={0};
void lumidat(Int_t runn){
    Bool_t syst_lumilo=false;
    Bool_t syst_lumihi=false;
    Bool_t syst_efflo=false;
    Bool_t syst_effhi=false;
    for (Int_t ii = 0; ii <7; ii++) {
        lumi[ii]=Lumi[ii];
        eps1[ii]=Eps1[ii];
        eps2[ii]=Eps2[ii];
    }
    
    if(syst_lumilo){
      for (Int_t ii = 0; ii <7; ii++) {
          lumi[ii]=Lumi[ii]*(1.-0.03);
      }
    }
    if(syst_lumihi){
      for (Int_t ii = 0; ii <7; ii++) {
          lumi[ii]=Lumi[ii]*(1.+0.03);
      }
    }
    if(syst_efflo){
      for (Int_t ii = 0; ii <7; ii++) {
          eps1[ii]=Eps1[ii]*(1.-0.01);
          eps2[ii]=Eps2[ii]*(1.-0.01);
      }
    }
    if(syst_effhi){
      for (Int_t ii = 0; ii <7; ii++) {
          eps1[ii]=Eps1[ii]*(1.+0.01);
          eps2[ii]=Eps2[ii]*(1.+0.01);
      }
    }
    
    if(runn==308979){
        dL=lumi[0]*life[0];
        ddL=0.03;
        edL=dLumi[0];
        eps[0]=eps1[0];
        eps[1]=eps2[0];
        deps[0]=dEps1[0];
        deps[1]=dEps2[0];
    }
    if(runn==308982){
        dL=lumi[1]*life[1];
        ddL=0.03;
        edL=dLumi[1];
        eps[0]=eps1[1];
        eps[1]=eps2[1];
        deps[0]=dEps1[1];
        deps[1]=dEps2[1];
    }
    if(runn==309010){
        dL=lumi[2]*life[2];
        ddL=0.03;
        edL=dLumi[2];
        eps[0]=eps1[2];
        eps[1]=eps2[2];
        deps[0]=dEps1[2];
        deps[1]=dEps2[2];
    }
    if(runn==309039){
        dL=lumi[3];
        ddL=0.03;
        edL=dLumi[3]*life[3];
        eps[0]=eps1[3];
        eps[1]=eps2[3];
        deps[0]=dEps1[3];
        deps[1]=dEps2[3];
    }
    if(runn==309074){
        dL=lumi[4]*life[4];
        ddL=0.03;
        edL=dLumi[4];
        eps[0]=eps1[4];
        eps[1]=eps2[4];
        deps[0]=dEps1[4];
        deps[1]=dEps2[4];
    }
    if(runn==309165){
        dL=lumi[5]*life[5];
        ddL=0.03;
        edL=dLumi[5];
        eps[0]=eps1[5];
        eps[1]=eps2[5];
        deps[0]=dEps1[5];
        deps[1]=dEps2[5];
    }
    if(runn==309166){
        dL=lumi[6]*life[6];
        ddL=0.03;
        edL=dLumi[6];
        eps[0]=eps1[6];
        eps[1]=eps2[6];
        deps[0]=dEps1[6];
        deps[1]=dEps2[6];
    }
    if(runn==111111) {// special combined
        Double_t xll=0.0,wgt1=0.0,wgt2=0.0;
        deps[0]=0.0,deps[1]=0.0;
        for(Int_t kk=0;kk<7;kk++){
	      xll=xll+lumi[kk]*life[kk];
           }
        for(Int_t kk=0;kk<7;kk++){
	      wgt1=wgt1+eps1[kk]*lumi[kk]/xll;
	      wgt2=wgt2+eps2[kk]*lumi[kk]/xll;
              deps[0]=deps[0]+dEps1[kk]*dEps1[kk]*lumi[kk]/xll*lumi[kk]/xll;
              deps[1]=deps[1]+dEps2[kk]*dEps2[kk]*lumi[kk]/xll*lumi[kk]/xll;
           }
       deps[0]=sqrt(deps[0]);
       deps[1]=sqrt(deps[1]);
       eps[0]=wgt1,eps[1]=wgt2,dL=xll,ddL=0.03,edL=223.; //stat.error on the luminosity from benedetto file.
       cout << "All combined lumi = " << xll << " 1/mb, recef arm 1 = " << eps[0] << " arm 2 = " << eps[1] << "\n"; 
    }
    xL=dL*xf*xt; // recorded luminosity 
    exL=edL*xf*xt;
    
}


