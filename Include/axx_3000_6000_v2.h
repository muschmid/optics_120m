   #include "Riostream.h"

   // axx_v6 for last Rafal alignment with run-dependent constants (mostly vertical offsets!)
// edge positions and vertical cuts on BS and edge
   Double_t edge[8]={135.000,135.012,135.034,134.948,135.008,134.995,135.005,135.021};
   Double_t yeo=0.06; // safety cut away from the edge
//   Double_t yeo=-2.; // no acceoptance cut!
//   Double_t yeo=0.; // no acceoptance cut!
   Double_t voff[4],hoff[4],vdist[4];
   
// Kristof cuts for the real beam screen position
// Don't know yet where we are 
// Double_t ybtr[8]={21.164,-19.759,22.297,-20.771, 21.300,-21.617, 20.320, -20.624}; // Beam screen position from Christian
Double_t ybtr[8]={21.,-21.0,22.,-22.0, 22.,-22., 21.0, -21.}; // Typical Beam screen position, guess at this point 
   
Double_t ybso=1.; // stay away 1 mm from the beam screen 
Double_t ybs[8]={
ybs[0]=ybtr[0]-ybso,
ybs[1]=ybtr[1]+ybso,
ybs[2]=ybtr[2]-ybso,
ybs[3]=ybtr[3]+ybso,
ybs[4]=ybtr[4]-ybso,
ybs[5]=ybtr[5]+ybso,
ybs[6]=ybtr[6]-ybso,
ybs[7]=ybtr[7]+ybso};

Double_t ye[8], yemc[8], yetr[8]; // yemc actually the same as for data right now, yetr true edge position
   
   void GetAlignmentV(Int_t runn){
     ifstream dfile,ofile;
     double dummy;
     TString Distance;
     TString Offset;
     if(runn==400001){
         Distance="/home/stenzel/madx/Elastic13p6TeV/Align/3_6km/v1/Distance.dat";
         Offset="/home/stenzel/madx/Elastic13p6TeV/Align/3_6km/v1/OffVert.dat";
     }
     if(runn==400002){
         Distance="/home/stenzel/madx/Elastic14TeV/optics_test/EventSelection/align/v1_3_6/Distance.dat";
         Offset="/home/stenzel/madx/Elastic14TeV/optics_test/EventSelection/align/v1_3_6/OffVert.dat";
     }
     if(runn==400003){
         Distance="/home/stenzel/madx/Elastic14TeV/optics_test/EventSelection/align/v1_6_6/Distance.dat";
         Offset="/home/stenzel/madx/Elastic14TeV/optics_test/EventSelection/align/v1_6_6/OffVert.dat";
     }
     
     dfile.open(Distance);
     
//     dfile.open("/home/stenzel/madx/Elastic13TeV/optics_25km/EventSelection/align/Distance.dat");
     
     if(dfile.is_open()){
      for (int i=0;i<4;i++){
       dfile >> vdist[i];
       cout << " Distance read station " << i << " = " << vdist[i] << "\n";
      }
     }
     else cout << "Can't open distance file" << "\n";
     dfile.close();
     
     ofile.open(Offset);
     
     if(ofile.is_open()){     
      for (int i=0;i<4;i++){
       ofile >> dummy >> voff[i] >> dummy;
       cout << " Offset read station " << i << " = " << voff[i] << "\n";
      }
     }
     else cout << "Can't open vertical offset file" << "\n";
     ofile.close();
     
     // setting the edge cut 
     ye[0]=vdist[0]/2+yeo-voff[0];
     ye[1]=-vdist[0]/2-yeo-voff[0];
     ye[2]=vdist[1]/2+yeo-voff[1];
     ye[3]=-vdist[1]/2-yeo-voff[1];
     ye[4]=vdist[2]/2+yeo-voff[2];
     ye[5]=-vdist[2]/2-yeo-voff[2];
     ye[6]=vdist[3]/2+yeo-voff[3];
     ye[7]=-vdist[3]/2-yeo-voff[3]; 

     yemc[0]=vdist[0]/2-voff[0]+yeo;
     yemc[1]=-vdist[0]/2-voff[0]-yeo;
     yemc[2]=vdist[1]/2-voff[1]+yeo;
     yemc[3]=-vdist[1]/2-voff[1]-yeo;
     yemc[4]=vdist[2]/2-voff[2]+yeo;
     yemc[5]=-vdist[2]/2-voff[2]-yeo;
     yemc[6]=vdist[3]/2-voff[3]+yeo;
     yemc[7]=-vdist[3]/2-voff[3]-yeo; 
     
     yetr[0]=vdist[0]/2-voff[0];
     yetr[1]=-vdist[0]/2-voff[0];
     yetr[2]=vdist[1]/2-voff[1];
     yetr[3]=-vdist[1]/2-voff[1];
     yetr[4]=vdist[2]/2-voff[2];
     yetr[5]=-vdist[2]/2-voff[2];
     yetr[6]=vdist[3]/2-voff[3];
     yetr[7]=-vdist[3]/2-voff[3]; 
   }

// event selection stuff

Bool_t axx(Double_t x, Double_t y){
// cuts on x vs theta_x
   Double_t sx=0.22,sy=65.9,theta=-4.2E-3; // values for 13 TeV 2.5km optics
   Double_t xp=x*cos(theta)+y*sin(theta);
   Double_t yp=-x*sin(theta)+y*cos(theta);
   Double_t tx=3.5*sx, ty=3.5*sy; // standard cut at 3.5
//   Double_t tx=5.*sx, ty=5.*sy; // cut opened
//   Double_t tx=7.*sx, ty=7.*sy; // cuts wide open
   Bool_t tmp=false;
   if( (xp*xp/tx/tx + yp*yp/ty/ty) < 1.) tmp=true;
//   if( (xp*xp/tx/tx + yp*yp/ty/ty) > 1.) tmp=true;
   if(noselect) tmp=true; //switch off
//   tmp=true; //switch off
return(tmp);
}

Bool_t axxL(Double_t x, Double_t y){
// cuts on x vs theta_x on the left side
   Double_t sx=0.242,sy=59.3,theta=-1.38E-3; // values for 13 TeV 2.5km optics
   Double_t xp=x*cos(theta)+y*sin(theta);
   Double_t yp=-x*sin(theta)+y*cos(theta);
   Double_t tx=3.5*sx, ty=3.5*sy; // standard cut at 3.5
//   Double_t tx=5.*sx, ty=5.*sy; // cut opened
//   Double_t tx=7.*sx, ty=7.*sy; // cuts wide open
   Bool_t tmp=false;
   if( (xp*xp/tx/tx + yp*yp/ty/ty) < 1.) tmp=true;
//   if( (xp*xp/tx/tx + yp*yp/ty/ty) > 1.) tmp=true;
   if(noselect) tmp=true; //switch off
//   tmp=true; //switch off
return(tmp);
}

Bool_t axxR(Double_t x, Double_t y){
// cuts on x vs theta_x on the right side
   Double_t sx=0.24,sy=59.5,theta=-1.96E-3; // values for 13 TeV 2.5km optics
   Double_t xp=x*cos(theta)+y*sin(theta);
   Double_t yp=-x*sin(theta)+y*cos(theta);
   Double_t tx=3.5*sx, ty=3.5*sy; // standard cut at 3.5
//   Double_t tx=5.*sx, ty=5.*sy; // cut opened
//   Double_t tx=7.*sx, ty=7.*sy; // cuts wide open
   Bool_t tmp=false;
   if( (xp*xp/tx/tx + yp*yp/ty/ty) < 1.) tmp=true;
//   if( (xp*xp/tx/tx + yp*yp/ty/ty) > 1.) tmp=true;
   if(noselect) tmp=true; //switch off
//   tmp=true; //switch off
return(tmp);
}

Bool_t ayy(Double_t x, Double_t y){
   Double_t sl=-1.0,olo=-1.0,oup=1.0; // expect y1 and y2 in mm normally 3 mm
//   Double_t sl=-1.,olo=-1.5,oup=1.5; // expect y1 and y2 in mm normally 3 mm
//   Double_t sl=-1.,olo=-5.0,oup=5.0; // open cuts
   Bool_t tmp=false;
   if( (y > (olo+sl*x)) && (y < (oup+sl*x))) tmp=true;
   if(noselect) tmp=true; //switch off
return(tmp);
}

Bool_t axo(Double_t xl, Double_t xr){ // cut on x_left vs x_right outer stations 
   Double_t sx=0.267,sy=1.17,theta=0.785; // expect x1 and x2 in mm
   Double_t xp=xl*cos(theta)+xr*sin(theta);
   Double_t yp=-xl*sin(theta)+xr*cos(theta);
   Double_t tx=3.5*sx, ty=3.5*sy; // 3.5 sigma nominal
//   Double_t tx=4.5*sx, ty=4.5*sy; 
   Bool_t tmp=false;
   if( (xp*xp/tx/tx + yp*yp/ty/ty) < 1.) tmp=true;
   if(noselect) tmp=true; //switch off
return(tmp);
}

Bool_t axi(Double_t xl, Double_t xr){ // cut on x_left vs x_right inner stations 
   Double_t sx=0.291,sy=0.404,theta=0.785; // expect x1 and x2 in mm
   Double_t xp=xl*cos(theta)+xr*sin(theta);
   Double_t yp=-xl*sin(theta)+xr*cos(theta);
   Double_t tx=3.5*sx, ty=3.5*sy; // 3.5 sigma nominal
//   Double_t tx=4.5*sx, ty=4.5*sy; // 3.5 sigma nominal
//   Double_t tx=5.0*sx, ty=5.0*sy; // wide open
   Bool_t tmp=false;
   if( (xp*xp/tx/tx + yp*yp/ty/ty) < 1.) tmp=true;
   if(noselect) tmp=true; //switch off
return(tmp);
}

Bool_t ayty(Double_t x, Double_t y){ // function to select events with right y vs thetay correlation 
   Double_t sl=-11.5,olo=-20.0,oup=20.0; // expect y1 and y2 in mm normally 20
   Bool_t tmp=false;
   if( (y > (olo+sl*x)) && (y < (oup+sl*x))) tmp=true;
   if(noselect) tmp=true; // switch off
     //tmp=true; // switch off anyhow
return(tmp);
}

Bool_t aytyL(Double_t x, Double_t y){ // function to select events with right y vs thetay correlation 
   Double_t sl=-8.1,olo=-20.0,oup=20.0; // expect y1 and y2 in mm normally 20
   Bool_t tmp=false;
   if( (y > (olo+sl*x)) && (y < (oup+sl*x))) tmp=true;
   if(noselect) tmp=true; // switch off
     //tmp=true; // switch off anyhow
return(tmp);
}

Bool_t aytyR(Double_t x, Double_t y){ // function to select events with right y vs thetay correlation 
   Double_t sl=-8.1,olo=-20.0,oup=20.0; // expect y1 and y2 in mm normally 20
   Bool_t tmp=false;
   if( (y > (olo+sl*x)) && (y < (oup+sl*x))) tmp=true;
   if(noselect) tmp=true; // switch off
     //tmp=true; // switch off anyhow
return(tmp);
}

Double_t fphi(Double_t *x, Double_t *par){
 Double_t phi1=0.7,phi2=2.4;
  if(x[0]<-phi2 || x[0] > phi2 || (x[0]>-phi1 && x[0]<phi1)){
    TF1::RejectPoint();
    return 0;
  }
  return par[0]+par[1]*cos(2*x[0]);
}

