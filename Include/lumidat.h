//    Double_t Lumi[3]={360000.,241000.,180000.};
    Double_t Lumi[3]={360000.,232000.,180000.};// 13.5 TeV run 400002
    Double_t dLumi[3]={36.,24.1,18.9}; //fake lumi error
    Double_t life[3]={1.0,1.0,1.0};// life-fraction to be applied with v2
    Double_t Eps1[3]={1.0,1.0,1.0};// values Hasko v1 Nov 2019 n=3  
    Double_t Eps2[3]={1.0,1.0,1.0};
    Double_t dEps1[3]={0.001,0.001,0.001};// stat.err bootstrap 23.05.2018
    Double_t dEps2[3]={0.001,0.001,0.001}; // checked on 29.06, still OK
    Double_t lumi[3]={0};
    Double_t eps1[3]={0};
    Double_t eps2[3]={0};
    Double_t dell=0.0;
void lumidat(Int_t runn){
    Bool_t syst_lumilo=false;
    Bool_t syst_lumihi=false;
    Bool_t syst_efflo=false;
    Bool_t syst_effhi=false;
    for (Int_t ii = 0; ii <3; ii++) {
        lumi[ii]=Lumi[ii];
        eps1[ii]=Eps1[ii];
        eps2[ii]=Eps2[ii];
    }
    
    if(syst_lumilo){
      for (Int_t ii = 0; ii <3; ii++) {
          lumi[ii]=Lumi[ii]*(1.-0.03);
      }
    }
    if(syst_lumihi){
      for (Int_t ii = 0; ii <3; ii++) {
          lumi[ii]=Lumi[ii]*(1.+0.03);
      }
    }
    if(syst_efflo){
      for (Int_t ii = 0; ii <3; ii++) {
          eps1[ii]=Eps1[ii]*(1.-0.01);
          eps2[ii]=Eps2[ii]*(1.+0.01);
      }
    }
    if(syst_effhi){
      for (Int_t ii = 0; ii <3; ii++) {
          eps1[ii]=Eps1[ii]*(1.+0.01);
          eps2[ii]=Eps2[ii]*(1.-0.01);
      }
    }
    
    if(runn==400001){
        dL=lumi[0]*life[0];
        ddL=0.03;
        edL=dLumi[0];
        eps[0]=eps1[0];
        eps[1]=eps2[0];
        deps[0]=dEps1[0];
        deps[1]=dEps2[0];
    }
    if(runn==400002){
        dL=lumi[1]*life[1];
        ddL=0.03;
        edL=dLumi[1];
        eps[0]=eps1[1];
        eps[1]=eps2[1];
        deps[0]=dEps1[1];
        deps[1]=dEps2[1];
    }
    if(runn==400003){
        dL=lumi[2]*life[2];
        ddL=0.03;
        edL=dLumi[2];
        eps[0]=eps1[2];
        eps[1]=eps2[2];
        deps[0]=dEps1[2];
        deps[1]=dEps2[2];
    }
    xL=dL*xf*xt; // recorded luminosity 
    exL=edL*xf*xt;
    
}


