// all global setting and constants of the total cross section analysis are defined here
Double_t dL,edL,ddL,xL,exL; // actual values are set in lumidat.h
//Double_t ddL=0.0131; // Syst lumi error 8 TeV
//Double_t dL=92060.0; // MC luminosity in 1/mb
//Double_t edL=314.; // statistical error 
//Double_t dL=1.; // no lumi, dN/dt
//Double_t edL=100.0;  

Double_t xf=1.000; // life fraction set to one as already applied to the luminosity
//const Double_t xt=0.96957; // some event lost pythia-->Ntuple (trigger efficiency) 4m file
Double_t xt=0.9998; // trigger efficiency
//Double_t xL=dL*xf*xt; // recorded luminosity 
//Double_t exL=edL*xf*xt;
//Double_t eps[2]={0.844,0.821},deps[2]={0.005,0.005}; // full efficiency 
Double_t eps[2]={0}; // actual values are set in lumidat.h
Double_t deps[2]={0.002,0.002}; // statistical error on RecoEff, values are set in lumidat.h  
Double_t seps[2]={0.01,0.01}; //Systematic Errors on RecoEff
Double_t ddE=0.01; // average reconstruction efficiency syst.err.

 Bool_t noselect=true;
 Bool_t Rafal=false,Optics=true,Matthieu=true,transp=false,smear=true;
 Bool_t covariance = true;
 Bool_t MCG4=false,MCTB=false,MCy=false;
 Bool_t MCoffset=false;
 Bool_t offt=false; // time-dependent x-offset, correction wrt chi2
 Bool_t offty=false; // time-dependent x-offset, correction wrt chi2
 Bool_t classic=false,light=true;
 Double_t xpi = 3.14159265359;
 Double_t conv=0.389379; // (hbar c)**2 conversion to mbarn
 Double_t rho=0.10; // error 0.0034
 Bool_t arm1=false,arm2=false; 
 
// Double_t tlow=0.000302742,thigh=0.100057; // -2 bins  
// Double_t tlow=0.000392573,thigh=0.100057; // -1 bin  
// Double_t tlow=0.000542937,thigh=0.100057; // nominal 
 Double_t tlow=0.000542937,thigh=0.197666; // Ct2 fit
//Double_t tlow=0.000819479,thigh=0.100057;  
//Double_t tlow=0.00123897,thigh=0.100057;  
//Double_t tlow=0.00178402,thigh=0.100057; 
//Double_t tlow=0.0024679,thigh=0.100057; // high t 
//Double_t tlow=0.00329989,thigh=0.100057; // high t 
//Double_t tlow=0.00679393,thigh=0.100057; // high t 
//Double_t tlow=0.00828953,thigh=0.100057; // high t 
//Double_t tlow=0.00996022,thigh=0.100057; // high t 
// Double_t tlow=0.100057 ,thigh=0.8; // Dip test 
// Double_t tlow=0.402161,thigh=0.8; // Dip test 
// Double_t tlow=0.207635 ,thigh=0.8; // Dip test 
 
 

Double_t txlow=0.01,txhigh=0.18,tylow=0.009,tyhigh=0.050;
