#include "Riostream.h"


Float_t EDGE_POS[8] ={-135.,135.012,-135.034,134.948,-135.008,134.995,-135.005,135.021};
Float_t center_x[8];
Float_t center_y[8]={-135.,135.,-135.,135.,-135.,135.,-135.,135.};
Float_t RotMat[8][2][2];
Float_t off_y[8];
Float_t Distance[8];

void Fill_Correction(Int_t runn)
{
Float_t DIST_OD[4]; 
Float_t rot_z[8];
Float_t dummy;
Int_t dum;

ifstream horiz,verti,disti;

     horiz.open("/home/stenzel/madx/Elastic13p6TeV/optics_120m/Align/OffHoriz.dat");
//     horiz.open(Form("/home/stenzel/madx/Elastic13TeV/optics_25km/EventSelection/align/v4/align_2500m13TeV_%d_004_10_opt_minuit/OffHoriz.dat",runn));
     if(horiz.is_open()){
      for (int i=0;i<8;i++){
  	   horiz >> dum >> center_x[i] >> dummy >> rot_z[i] >> dummy;
  	  }
     }
    else cout << "Can't open horizontal offset + rotation file" << "\n";
    horiz.close();
      
    for (int i=0;i<8;i++){
        RotMat[i][0][0] = cos(rot_z[i]);
        RotMat[i][0][1] = -1.*sin(rot_z[i]);
        RotMat[i][1][0] = sin(rot_z[i]);
        RotMat[i][1][1] = cos(rot_z[i]);
	}
  
    verti.open("/home/stenzel/madx/Elastic13p6TeV/optics_120m/Align/OffVert.dat");
//    verti.open(Form("/home/stenzel/madx/Elastic13TeV/optics_25km/EventSelection/align/v4/align_2500m13TeV_%d_004_10_opt_minuit/OffVert.dat",runn));
     if(verti.is_open()){  
      for (int i=0;i<4;i++){
  	    verti >> dum >> off_y[2*i] >> dummy;
        off_y[2*i+1]= off_y[2*i];
      }
  	 }
    else cout << "Can't open vertical offset file" << "\n";
    verti.close();
  
     disti.open("/home/stenzel/madx/Elastic13p6TeV/optics_120m/Align/Distance.dat");
//     disti.open(Form("/home/stenzel/madx/Elastic13TeV/optics_25km/EventSelection/align/v4/align_2500m13TeV_%d_004_10_opt_minuit/Distance.dat",runn));
     if(disti.is_open()){  	
      for (int i=0;i<4;i++){
	   disti >> DIST_OD[i];
	   Distance[2*i] = DIST_OD[i]/2.;
	   Distance[2*i+1] = -1.*DIST_OD[i]/2.;  	
	  }
     }
     else cout << "Can't open vertical distance file" << "\n";
     disti.close();

}

Float_t Correct_Coordinate(Int_t run_num,Int_t det,Int_t opt, Float_t x, Float_t y)
{
Float_t x_input, y_input;
Int_t NotUsed=run_num; // but could be used at some point
x_input = x;
y_input = y;
Float_t sign;

if (det%2==0) sign=-1.;
else sign=1.;
 x =  RotMat[det][0][0]*(x_input-center_x[det])+RotMat[det][0][1]*(y_input-center_y[det]);
 y =  RotMat[det][1][0]*(x_input-center_x[det])+RotMat[det][1][1]*(y_input-center_y[det])-sign*(sign*EDGE_POS[det]-135.)+Distance[det]-off_y[det];
 if (opt==0) return x;
 else return y;
}
Float_t Apply_Rotation(Int_t det, Int_t opt, Float_t x, Float_t y, Float_t Angle) {
	
	Float_t x_input, y_input;
	x_input = x;
	y_input = y;
	
	x =  cos(Angle)*x_input-sin(Angle)*(y_input-(Distance[det]-off_y[det]));
	y =  sin(Angle)*x_input+cos(Angle)*(y_input-(Distance[det]-off_y[det]))+(Distance[det]-off_y[det]);
 	if (opt==0) return x;
 	else return y;
}
