/**
 *  Macro description:
 *
 *	The procedure in this header does track matching and calculates the best track candidates for 4/4 events only
 *      A simplified procedure is used in this version selecting the candidate which minimizes the event acolinearity.
 *	It runs at analysis runtime, requireing 
 * 
 * 		Int_t           Detector[12][8];
 *		Float_t         x_Det[12][8];   
 *		Float_t         y_Det[12][8]; 
 *		Int_t 		BestTrackCand[8];
 *              Int_t           NU[12][8];
 *              Int_t           NV[12][20];
 *		Int_t           numtrack;
 *		 
 *	where x and y coordinates are in beam coordinate system, so it requires corrected coordinates.
 *	The procedure will modify the BestTrackCand[8] array for any given event with the best track number for each detector.
 *	
 *	At runtime execude once ComputeSig();
 *	And for each given event call GetBestTrackSimple(BestTrackCand,Detector,x_Det,y_Det,numtrack); 
 *
 *
 *  @date    2017-05-05
 *  @author  Matthieu Heller, Kristof Kreutzfeldt, Christian Heinz, Hasko Stenzel
 *
 **/

#include "TMath.h"
#include "TMathBase.h"

    Double_t sx=0.55,sy13=0.051,sy24=0.046,sy57=0.043,sy68=0.044;
    Double_t sxi=0.382,sxo=0.353,syi=0.117,syo=0.115;
    Double_t LxA,LxC,LyA,LyC;
    
    void SetSigmaLeff(Double_t Mx1, Double_t Mx2, Double_t Mx3, Double_t Mx4,
                      Double_t My1, Double_t My2, Double_t My3, Double_t My4){
                LxA=Mx1/Mx2,LxC=Mx4/Mx3;
                LyA=My1/My2,LyC=My4/My3;
                cout << "BestTrackSimple LxA = " << LxA << " LxC = " << LxC << " LyA = " << LyA << " LyC = " << LyC << "\n";
                cout << "Sigmas applied for 2500m optics." << "\n";
    }

void GetBestTrackSimple( Int_t BestTrackCand_internal[8] , Int_t Detector_internal[12][8] , Float_t x_A[12][8] , Float_t y_A[12][8] , Int_t numtrack_internal, Int_t NU[12][8], Int_t NV[12][8]){

Int_t cnt=0;

Float_t Chi2[2];		//write chi^2 for both arms to tree ([0]: arm 1368, [1]: arm 2457)
Float_t Chi2x[2];
Float_t Chi2y[2];

Float_t chiTOT;
Float_t chiTOT_X;
Float_t chiTOT_Y;

Float_t minChi1=1e9;
Float_t minChi2=1e9;
Float_t minChi_y=1e9;
Bool_t arm1=false;
Bool_t arm2=false;
Float_t ac=0.0;
//                cout << "BestTrackSimple LxA = " << LxA << " LxC = " << LxC << " LyA = " << LyA << " LyC = " << LyC << "\n";

        if(Detector_internal[0][0]==1 && Detector_internal[0][2]==1 && Detector_internal[0][5]==1 && Detector_internal[0][7]==1) arm1=true;
        if(Detector_internal[0][1]==1 && Detector_internal[0][3]==1 && Detector_internal[0][4]==1 && Detector_internal[0][6]==1) arm2=true;
        if(!arm1 && !arm2){
            cout << "BestTrackSimple: No arm reconstructed!" << "\n";
            return;
        }
        
        if(arm1){
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
            for (int i4=0;i4<numtrack_internal;i4++) {
              if(Detector_internal[i1][0]==1 && Detector_internal[i2][2]==1 && Detector_internal[i3][5]==1 && Detector_internal[i4][7]==1 
              && NU[i1][0]>=NumUV_Cut && NV[i1][0]>=NumUV_Cut && NU[i2][2]>=NumUV_Cut && NV[i2][2]>=NumUV_Cut 
              && NU[i3][5]>=NumUV_Cut && NV[i3][5]>=NumUV_Cut && NU[i4][7]>=NumUV_Cut && NV[i4][7]>=NumUV_Cut 
            ){ 
 //               if(x_A[i1][0]==0.0) cout << "Error track " << i1 << " is empty!" << "\n";
                ac=
                pow((x_A[i1][0]+x_A[i4][7])/sxo,2)+pow((x_A[i2][2]+x_A[i3][5])/sxi,2)+
                pow((y_A[i1][0]+y_A[i4][7])/syo,2)+pow((y_A[i2][2]+y_A[i3][5])/syi,2)+
                pow((x_A[i1][0]-LxA*x_A[i2][2])/sx,2)+pow((x_A[i4][7]-LxC*x_A[i3][5])/sx,2)+
                pow((y_A[i1][0]-LyA*y_A[i2][2])/sy13,2)+pow((y_A[i4][7]-LyC*y_A[i3][5])/sy68,2);
                if(ac<minChi1) {
                    minChi1=ac;
                    BestTrackCand_internal[0]=i1;
                    BestTrackCand_internal[2]=i2;
                    BestTrackCand_internal[5]=i3;
                    BestTrackCand_internal[7]=i4;
                }
              }
            }
            }
            }
            }
//             cout << " chi2 = " << minChi1 << " x1 = " << x_A[BestTrackCand_internal[0]][0] << " x3 = " << x_A[BestTrackCand_internal[2]][2] <<  "\n";
        }
        ac=0.0;
        
        if(arm2){
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
            for (int i4=0;i4<numtrack_internal;i4++) {
              if(Detector_internal[i1][1]==1 && Detector_internal[i2][3]==1 && Detector_internal[i3][4]==1 && Detector_internal[i4][6]==1
              && NU[i1][1]>=NumUV_Cut && NV[i1][1]>=NumUV_Cut && NU[i2][3]>=NumUV_Cut && NV[i2][3]>=NumUV_Cut 
              && NU[i3][4]>=NumUV_Cut && NV[i3][4]>=NumUV_Cut && NU[i4][6]>=NumUV_Cut && NV[i4][6]>=NumUV_Cut                   
            ){  
                ac=
                pow((x_A[i1][1]+x_A[i4][6])/sxo,2)+pow((x_A[i2][3]+x_A[i3][4])/sxi,2)+
                pow((y_A[i1][1]+y_A[i4][6])/syo,2)+pow((y_A[i2][3]+y_A[i3][4])/syi,2)+
                pow((x_A[i1][1]-LxA*x_A[i2][3])/sx,2)+pow((x_A[i4][6]-LxC*x_A[i3][4])/sx,2)+
                pow((y_A[i1][1]-LyA*y_A[i2][3])/sy24,2)+pow((y_A[i4][6]-LyC*y_A[i3][4])/sy57,2);
                if(ac<minChi2) {
                    minChi2=ac;
                    BestTrackCand_internal[1]=i1;
                    BestTrackCand_internal[3]=i2;
                    BestTrackCand_internal[4]=i3;
                    BestTrackCand_internal[6]=i4;
                }
              }
            }
            }
            }
            }
                
        }
        

}

void GetBestTrackRE(Int_t ID, Int_t BestTrackCand_internal[8] , Int_t Detector_internal[12][8] , Float_t x_A[12][8] , Float_t y_A[12][8] , Int_t numtrack_internal, Int_t NU[12][8], Int_t NV[12][8]){

Int_t cnt=0;

Float_t minChi=1e9;
Float_t ac=0.0;

        if(ID<2 || ID>45){
            cout << "BestTrackRE ID invalid = " << ID << " valid ID range 2-23 " << "\n";
            return;
        }
        
        if(ID==2){
            // ALFA 3 6 8 
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
              if(Detector_internal[i1][2]==1 && Detector_internal[i2][5]==1 && Detector_internal[i3][7]==1 
              && NU[i1][2]>=NumUV_Cut && NV[i1][2]>=NumUV_Cut && NU[i2][5]>=NumUV_Cut && NV[i2][5]>=NumUV_Cut && NU[i3][7]>=NumUV_Cut && NV[i3][7]>=NumUV_Cut){
                ac=
                pow((x_A[i1][2]+x_A[i2][2])/sxi,2)+
                pow((y_A[i1][2]+y_A[i2][5])/syi,2)+
                pow((x_A[i3][7]-LxC*x_A[i2][5])/sx,2)+
                pow((y_A[i3][7]-LyC*y_A[i2][5])/sy68,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[2]=i1;
                    BestTrackCand_internal[5]=i2;
                    BestTrackCand_internal[7]=i3;
                }
              }
            }
            }
            }
        }
        
        if(ID==4){
            // ALFA 1 3 6 
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
              if(Detector_internal[i1][0]==1 && Detector_internal[i2][2]==1 && Detector_internal[i3][5]==1 
              && NU[i1][0]>=NumUV_Cut && NV[i1][0]>=NumUV_Cut && NU[i2][2]>=NumUV_Cut && NV[i2][2]>=NumUV_Cut 
              && NU[i3][5]>=NumUV_Cut && NV[i3][5]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i2][2]+x_A[i3][5])/sxi,2)+
                pow((y_A[i2][2]+y_A[i3][5])/syi,2)+
                pow((x_A[i1][0]-LxA*x_A[i2][2])/sx,2)+
                pow((y_A[i1][0]-LyA*y_A[i2][2])/sy13,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[0]=i1;
                    BestTrackCand_internal[2]=i2;
                    BestTrackCand_internal[5]=i3;
                }
              }
            }
            }
            }
        }
        
        if(ID==3){
            // ALFA 4 5 7 
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
              if(Detector_internal[i1][3]==1 && Detector_internal[i2][4]==1 && Detector_internal[i3][6]==1
              && NU[i1][3]>=NumUV_Cut && NV[i1][3]>=NumUV_Cut 
              && NU[i2][4]>=NumUV_Cut && NV[i2][4]>=NumUV_Cut && NU[i3][6]>=NumUV_Cut && NV[i3][6]>=NumUV_Cut                   
            ){  
                ac=
                pow((x_A[i1][3]+x_A[i2][4])/sxi,2)+
                pow((y_A[i1][3]+y_A[i2][4])/syi,2)+
                pow((x_A[i3][6]-LxC*x_A[i2][4])/sx,2)+
                pow((y_A[i3][6]-LyC*y_A[i2][4])/sy57,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[3]=i1;
                    BestTrackCand_internal[4]=i2;
                    BestTrackCand_internal[6]=i3;
                }
              }
            }
            }
            }
        }
        
        if(ID==5){
            // ALFA 2 4 5
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
              if(Detector_internal[i1][1]==1 && Detector_internal[i2][3]==1 && Detector_internal[i3][4]==1
              && NU[i1][1]>=NumUV_Cut && NV[i1][1]>=NumUV_Cut && NU[i2][3]>=NumUV_Cut && NV[i2][3]>=NumUV_Cut 
              && NU[i3][4]>=NumUV_Cut && NV[i3][4]>=NumUV_Cut                   
            ){  
                ac=
                pow((x_A[i2][3]+x_A[i3][4])/sxi,2)+
                pow((y_A[i2][3]+y_A[i3][4])/syi,2)+
                pow((x_A[i1][1]-LxA*x_A[i2][3])/sx,2)+
                pow((y_A[i1][1]-LyA*y_A[i2][3])/sy24,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[1]=i1;
                    BestTrackCand_internal[3]=i2;
                    BestTrackCand_internal[4]=i3;
                }
              }
            }
            }
            }
                
        }
        
        if(ID==6){
            // ALFA 1 3 8
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
              if(Detector_internal[i1][0]==1 && Detector_internal[i2][2]==1 && Detector_internal[i3][7]==1 
              && NU[i1][0]>=NumUV_Cut && NV[i1][0]>=NumUV_Cut && NU[i2][2]>=NumUV_Cut && NV[i2][2]>=NumUV_Cut 
              && NU[i3][7]>=NumUV_Cut && NV[i3][7]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][0]+x_A[i3][7])/sxo,2)+
                pow((y_A[i1][0]+y_A[i3][7])/syo,2)+
                pow((x_A[i1][0]-LxA*x_A[i2][2])/sx,2)+
                pow((y_A[i1][0]-LyA*y_A[i2][2])/sy13,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[0]=i1;
                    BestTrackCand_internal[2]=i2;
                    BestTrackCand_internal[7]=i3;
                }
              }
            }
            }
            }
        }
        
        if(ID==8){
            // ALFA 1 6 8
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
              if(Detector_internal[i1][0]==1 && Detector_internal[i2][5]==1 && Detector_internal[i3][7]==1 
              && NU[i1][0]>=NumUV_Cut && NV[i1][0]>=NumUV_Cut  
              && NU[i2][5]>=NumUV_Cut && NV[i2][5]>=NumUV_Cut && NU[i3][7]>=NumUV_Cut && NV[i3][7]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][0]+x_A[i3][7])/sxo,2)+
                pow((y_A[i1][0]+y_A[i3][7])/syo,2)+
                pow((x_A[i3][7]-LxC*x_A[i2][5])/sx,2)+
                pow((y_A[i3][7]-LyC*y_A[i2][5])/sy68,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[0]=i1;
                    BestTrackCand_internal[5]=i2;
                    BestTrackCand_internal[7]=i3;
                }
              }
            }
            }
            }
        }
        
        if(ID==7){
            // ALFA 2 4 7
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
              if(Detector_internal[i1][1]==1 && Detector_internal[i2][3]==1 &&  Detector_internal[i3][6]==1
              && NU[i1][1]>=NumUV_Cut && NV[i1][1]>=NumUV_Cut && NU[i2][3]>=NumUV_Cut && NV[i2][3]>=NumUV_Cut 
              && NU[i3][6]>=NumUV_Cut && NV[i3][6]>=NumUV_Cut                   
            ){  
                ac=
                pow((x_A[i1][1]+x_A[i3][6])/sxo,2)+
                pow((y_A[i1][1]+y_A[i3][6])/syo,2)+
                pow((x_A[i1][1]-LxA*x_A[i2][3])/sx,2)+
                pow((y_A[i1][1]-LyA*y_A[i2][3])/sy24,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[1]=i1;
                    BestTrackCand_internal[3]=i2;
                    BestTrackCand_internal[6]=i3;
                }
              }
            }
            }
            }
                
        }

        if(ID==9){
            // ALFA 2 5 7
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
              if(Detector_internal[i1][1]==1 && Detector_internal[i2][4]==1 && Detector_internal[i3][6]==1
              && NU[i1][1]>=NumUV_Cut && NV[i1][1]>=NumUV_Cut  
              && NU[i2][4]>=NumUV_Cut && NV[i2][4]>=NumUV_Cut && NU[i3][6]>=NumUV_Cut && NV[i3][6]>=NumUV_Cut                   
            ){  
                ac=
                pow((x_A[i1][1]+x_A[i3][6])/sxo,2)+
                pow((y_A[i1][1]+y_A[i3][6])/syo,2)+
                pow((x_A[i3][6]-LxC*x_A[i2][4])/sx,2)+
                pow((y_A[i3][6]-LyC*y_A[i2][4])/sy57,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[1]=i1;
                    BestTrackCand_internal[4]=i2;
                    BestTrackCand_internal[6]=i3;
                }
              }
            }
            }
            }                
        }
                
        if(ID==10){
            // ALFA 1 3 
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][0]==1 && Detector_internal[i2][2]==1
              && NU[i1][0]>=NumUV_Cut && NV[i1][0]>=NumUV_Cut && NU[i2][2]>=NumUV_Cut && NV[i2][2]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][0]-LxA*x_A[i2][2])/sx,2)+
                pow((y_A[i1][0]-LyA*y_A[i2][2])/sy13,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[0]=i1;
                    BestTrackCand_internal[2]=i2;
                }
              }
            }
            }
        }
        
        if(ID==11){
            // ALFA 6 8 
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][5]==1 && Detector_internal[i2][7]==1 
              && NU[i1][5]>=NumUV_Cut && NV[i1][5]>=NumUV_Cut && NU[i2][7]>=NumUV_Cut && NV[i2][7]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i2][7]-LxC*x_A[i1][5])/sx,2)+
                pow((y_A[i2][7]-LyC*y_A[i1][5])/sy68,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[5]=i1;
                    BestTrackCand_internal[7]=i2;
                }
              }
            }
            }
        }
        
        if(ID==12){
            // ALFA 2 4
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][1]==1 && Detector_internal[i2][3]==1
              && NU[i1][1]>=NumUV_Cut && NV[i1][1]>=NumUV_Cut && NU[i2][3]>=NumUV_Cut && NV[i2][3]>=NumUV_Cut 
            ){  
                ac=
                pow((x_A[i1][1]-LxA*x_A[i2][3])/sx,2)+
                pow((y_A[i1][1]-LyA*y_A[i2][3])/sy24,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[1]=i1;
                    BestTrackCand_internal[3]=i2;
                }
              }
            }
            }
                
        }
        
        if(ID==13){
            // ALFA 5 7
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][4]==1 && Detector_internal[i2][6]==1
              && NU[i1][4]>=NumUV_Cut && NV[i1][4]>=NumUV_Cut && NU[i2][6]>=NumUV_Cut && NV[i2][6]>=NumUV_Cut                   
            ){  
                ac=
                pow((x_A[i2][6]-LxC*x_A[i1][4])/sx,2)+
                pow((y_A[i2][6]-LyC*y_A[i1][4])/sy57,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[4]=i1;
                    BestTrackCand_internal[6]=i2;
                }
              }
            }
            }
                
        }

        if(ID==14){
            // ALFA 3 6
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][2]==1 && Detector_internal[i2][5]==1 
              && NU[i1][2]>=NumUV_Cut && NV[i1][2]>=NumUV_Cut  
              && NU[i2][5]>=NumUV_Cut && NV[i2][5]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][2]+x_A[i2][5])/sxi,2)+
                pow((y_A[i1][2]+y_A[i2][5])/syi,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[2]=i1;
                    BestTrackCand_internal[5]=i2;
                }
              }
            }
            }
        }

        if(ID==15){
            // ALFA 4 5
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][3]==1 && Detector_internal[i2][4]==1 
              && NU[i1][3]>=NumUV_Cut && NV[i1][3]>=NumUV_Cut  
              && NU[i2][4]>=NumUV_Cut && NV[i2][4]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][3]+x_A[i2][4])/sxi,2)+
                pow((y_A[i1][3]+y_A[i2][4])/syi,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[3]=i1;
                    BestTrackCand_internal[4]=i2;
                }
              }
            }
            }
        }
        if(ID==31){
            // ALFA 1 8
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][0]==1 && Detector_internal[i2][7]==1 
              && NU[i1][0]>=NumUV_Cut && NV[i1][0]>=NumUV_Cut  
              && NU[i2][7]>=NumUV_Cut && NV[i2][7]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][0]+x_A[i2][7])/sxo,2)+
                pow((y_A[i1][0]+y_A[i2][7])/syo,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[0]=i1;
                    BestTrackCand_internal[7]=i2;
                }
              }
            }
            }
        }
        if(ID==32){
            // ALFA 1 6
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][0]==1 && Detector_internal[i2][5]==1 
              && NU[i1][0]>=NumUV_Cut && NV[i1][0]>=NumUV_Cut  
              && NU[i2][5]>=NumUV_Cut && NV[i2][5]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][0]+x_A[i2][5])/sxo,2)+
                pow((y_A[i1][0]+y_A[i2][5])/syo,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[0]=i1;
                    BestTrackCand_internal[5]=i2;
                }
              }
            }
            }
        }
        if(ID==33){
            // ALFA 3 8
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][2]==1 && Detector_internal[i2][7]==1 
              && NU[i1][2]>=NumUV_Cut && NV[i1][2]>=NumUV_Cut  
              && NU[i2][7]>=NumUV_Cut && NV[i2][7]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][2]+x_A[i2][7])/sxo,2)+
                pow((y_A[i1][2]+y_A[i2][7])/syo,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[0]=i1;
                    BestTrackCand_internal[7]=i2;
                }
              }
            }
            }
        }
        if(ID==34){
            // ALFA 2 7
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][1]==1 && Detector_internal[i2][6]==1 
              && NU[i1][1]>=NumUV_Cut && NV[i1][1]>=NumUV_Cut  
              && NU[i2][6]>=NumUV_Cut && NV[i2][6]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][1]+x_A[i2][6])/sxo,2)+
                pow((y_A[i1][1]+y_A[i2][6])/syo,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[1]=i1;
                    BestTrackCand_internal[6]=i2;
                }
              }
            }
            }
        }
        if(ID==35){
            // ALFA 2 5
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][1]==1 && Detector_internal[i2][4]==1 
              && NU[i1][1]>=NumUV_Cut && NV[i1][1]>=NumUV_Cut  
              && NU[i2][4]>=NumUV_Cut && NV[i2][4]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][1]+x_A[i2][4])/sxo,2)+
                pow((y_A[i1][1]+y_A[i2][4])/syo,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[1]=i1;
                    BestTrackCand_internal[4]=i2;
                }
              }
            }
            }
        }
        if(ID==36){
            // ALFA 4 7
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
              if(Detector_internal[i1][3]==1 && Detector_internal[i2][6]==1 
              && NU[i1][3]>=NumUV_Cut && NV[i1][3]>=NumUV_Cut  
              && NU[i2][6]>=NumUV_Cut && NV[i2][6]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][3]+x_A[i2][6])/sxo,2)+
                pow((y_A[i1][3]+y_A[i2][6])/syo,2);
                if(ac<minChi) {
                    minChi=ac;
                    BestTrackCand_internal[3]=i1;
                    BestTrackCand_internal[6]=i2;
                }
              }
            }
            }
        }
        
        /*
        if(arm1){
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
            for (int i4=0;i4<numtrack_internal;i4++) {
              if(Detector_internal[i1][0]==1 && Detector_internal[i2][2]==1 && Detector_internal[i3][5]==1 && Detector_internal[i4][7]==1 
              && NU[i1][0]>=NumUV_Cut && NV[i1][0]>=NumUV_Cut && NU[i2][2]>=NumUV_Cut && NV[i2][2]>=NumUV_Cut 
              && NU[i3][5]>=NumUV_Cut && NV[i3][5]>=NumUV_Cut && NU[i4][7]>=NumUV_Cut && NV[i4][7]>=NumUV_Cut 
            ){ 
                ac=
                pow((x_A[i1][0]+x_A[i4][7])/sxo,2)+pow((x_A[i2][2]+x_A[i3][5])/sxi,2)+
                pow((y_A[i1][0]+y_A[i4][7])/syo,2)+pow((y_A[i2][2]+y_A[i3][5])/syi,2)+
                pow((x_A[i1][0]-LxA*x_A[i2][2])/sx,2)+pow((x_A[i4][7]-LxC*x_A[i3][5])/sx,2)+
                pow((y_A[i1][0]-LyA*y_A[i2][2])/sy13,2)+pow((y_A[i4][7]-LyC*y_A[i3][5])/sy68,2);
                if(ac<minChi1) {
                    minChi1=ac;
                    BestTrackCand_internal[0]=i1;
                    BestTrackCand_internal[2]=i2;
                    BestTrackCand_internal[5]=i3;
                    BestTrackCand_internal[7]=i4;
                }
              }
            }
            }
            }
            }
        }
        ac=0.0;
        
        if(arm2){
            for (int i1=0;i1<numtrack_internal;i1++) {
            for (int i2=0;i2<numtrack_internal;i2++) {
            for (int i3=0;i3<numtrack_internal;i3++) {
            for (int i4=0;i4<numtrack_internal;i4++) {
              if(Detector_internal[i1][1]==1 && Detector_internal[i2][3]==1 && Detector_internal[i3][4]==1 && Detector_internal[i4][6]==1
              && NU[i1][1]>=NumUV_Cut && NV[i1][1]>=NumUV_Cut && NU[i2][3]>=NumUV_Cut && NV[i2][3]>=NumUV_Cut 
              && NU[i3][4]>=NumUV_Cut && NV[i3][4]>=NumUV_Cut && NU[i4][6]>=NumUV_Cut && NV[i4][6]>=NumUV_Cut                   
            ){  
                ac=
                pow((x_A[i1][1]+x_A[i4][6])/sxo,2)+pow((x_A[i2][3]+x_A[i3][4])/sxi,2)+
                pow((y_A[i1][1]+y_A[i4][6])/syo,2)+pow((y_A[i2][3]+y_A[i3][4])/syi,2)+
                pow((x_A[i1][1]-LxA*x_A[i2][3])/sx,2)+pow((x_A[i4][6]-LxC*x_A[i3][4])/sx,2)+
                pow((y_A[i1][1]-LyA*y_A[i2][3])/sy24,2)+pow((y_A[i4][6]-LyC*y_A[i3][4])/sy57,2);
                if(ac<minChi2) {
                    minChi2=ac;
                    BestTrackCand_internal[1]=i1;
                    BestTrackCand_internal[3]=i2;
                    BestTrackCand_internal[4]=i3;
                    BestTrackCand_internal[6]=i4;
                }
              }
            }
            }
            }
            }
                
        }
*/        

}



