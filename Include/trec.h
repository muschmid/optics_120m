
// t-reconstruction routine
// input: x/y track coordinates from 4 detectors 1-4, left B7L1 - right B7R1, in mm
//	: theta_x/y local angles left/right in murad
// Calculated: t values according to subtraction, local angle, lattice, local subtraction
//	     : tx and ty for subtraction and local angle
//	     : theta* in x/y, all combined from subtraction, inner/outer for subtraction
//	     : theta* in x/y, left and right using subtraction method
//	     : theta* in x/y for local angle, only inner/outer combined, by construction

Double_t tcomb,tcomb_loc,tcomb_lat,tcomb_side;
Double_t tx_loc,ty_loc,tx_sub,ty_sub;
Double_t Theta_x,Theta_y;
Double_t Theta_x_inner,Theta_y_inner,Theta_x_outer,Theta_y_outer;
Double_t Theta_x_l,Theta_y_l,Theta_x_r,Theta_y_r;
Double_t Theta_x_loc,Theta_y_loc;
Double_t Theta_y_1,Theta_y_2,Theta_y_3,Theta_y_4;
Double_t Theta_x_1,Theta_x_2,Theta_x_3,Theta_x_4;
Double_t Theta_y_loc_l,Theta_y_loc_r;
Double_t vx1,vx2,vx3,vx4,vx;
TMatrixD Tx1(2,2),Tx2(2,2),Tx3(2,2),Tx4(2,2),Ty1(2,2),Ty2(2,2),Ty3(2,2),Ty4(2,2);

void trec(Double_t x1, Double_t x2, Double_t x3, Double_t x4,
	  Double_t y1, Double_t y2, Double_t y3, Double_t y4,
	  Double_t pxl, Double_t pyl, Double_t pxr, Double_t pyr){
//	 cout << "trec: pxl = " << pxl << " pxr = " << pxr << " pyl = " << pyl << " pyr " << pyr << "\n"; 

	// standard subtraction
	 Double_t tx1=(x2/1000.-x3/1000.)/(Lx2+Lx3);
	 Double_t ty1=(y2/1000.-y3/1000.)/(Ly2+Ly3);
	 Double_t tx2=(x1/1000.-x4/1000.)/(Lx1+Lx4);
	 Double_t ty2=(y1/1000.-y4/1000.)/(Ly1+Ly4);

	 // individual t-values, if ever needed
	 
	 Double_t tty1=y1/1000./Ly1;
	 Double_t ttx1=x1/1000./Lx1;
	 Double_t tty2=y2/1000./Ly2;
	 Double_t ttx2=x2/1000./Lx2;
	 Double_t tty3=y3/1000./Ly3;
	 Double_t ttx3=x3/1000./Lx3;
	 Double_t tty4=y4/1000./Ly4;
	 Double_t ttx4=x4/1000./Lx4;

	// scattering angles, all combined, inner/outer, subtraction
	 
         Theta_x=(ttx1-ttx4+ttx2-ttx3)*0.25*1E6;
         Theta_x_outer=(ttx1-ttx4)*0.5*1E6;
         Theta_x_inner=(ttx2-ttx3)*0.5*1E6;
	     Theta_y=(tty1-tty4+tty2-tty3)*0.25*1E6;
         Theta_y_outer=(tty1-tty4)*0.5*1E6;
         Theta_y_inner=(tty2-tty3)*0.5*1E6;
         Theta_y_1=tty1*1E6;
         Theta_y_2=tty2*1E6;
         Theta_y_3=tty3*1E6;
         Theta_y_4=tty4*1E6;
         Theta_x_1=ttx1*1E6;
         Theta_x_2=ttx2*1E6;
         Theta_x_3=ttx3*1E6;
         Theta_x_4=ttx4*1E6;
	 
	 // scattering angle using local angle reconstruction

         Theta_x_loc=(pxl*1.E-6 - pxr*1.E-6)/(Tx1[1][1]+Tx3[1][1]);
         Theta_y_loc=(pyl*1.E-6 - pyr*1.E-6)/(Ty1[1][1]+Ty3[1][1]);
         Theta_y_loc_l=pyl*1.E-6/Ty1[1][1];
         Theta_y_loc_r=pyr*1.E-6/Ty3[1][1];
	 
	// scattering angles left/right from subtraction
	 
        Theta_x_l=(ttx1+ttx2)*0.5*1E6;
        Theta_y_l=(tty1+tty2)*0.5*1E6;
        Theta_x_r=(ttx3+ttx4)*0.5*1E6;
        Theta_y_r=(tty3+tty4)*0.5*1E6;

// scattering angles according to the lattice reconstruction, per detector

         Double_t trx1=Mx1[1][0]*x1*1.E-3+Mx1[1][1]*pxl*1.E-6;
         Double_t trx2=Mx2[1][0]*x2*1.E-3+Mx2[1][1]*pxl*1.E-6;
         Double_t trx3=Mx3[1][0]*x3*1.E-3+Mx3[1][1]*pxr*1.E-6;
         Double_t trx4=Mx4[1][0]*x4*1.E-3+Mx4[1][1]*pxr*1.E-6;
         Double_t try1=My1[1][0]*y1*1.E-3+My1[1][1]*pyl*1.E-6;
         Double_t try2=My2[1][0]*y2*1.E-3+My2[1][1]*pyl*1.E-6;
         Double_t try3=My3[1][0]*y3*1.E-3+My3[1][1]*pyr*1.E-6;
         Double_t try4=My4[1][0]*y4*1.E-3+My4[1][1]*pyr*1.E-6;
         Double_t tx1_lattice = (trx1 + trx2)/2., tx2_lattice = (trx3 + trx4)/2.;
         Double_t ttx_lattice = (tx1_lattice-tx2_lattice)/2.;
         Double_t ty1_lattice = (try1 + try2)/2., ty2_lattice = (try3 + try4)/2.;
         Double_t tty_lattice = (ty1_lattice-ty2_lattice)/2.;
	 
	// reconstructed vertex in x through lattice (inverted transport matrix)

         vx1=Mx1[0][0]*x1*1.E3+Mx1[0][1]*pxl;
         vx2=Mx2[0][0]*x2*1.E3+Mx2[0][1]*pxl;
         vx3=Mx3[0][0]*x3*1.E3+Mx3[0][1]*pxr;
         vx4=Mx4[0][0]*x4*1.E3+Mx4[0][1]*pxr;
         vx=(vx1+vx2+vx3+vx4)/4.;
	 
	// t-reconstruction

	// standard subtraction
	 
         Double_t tr1=(tx1*tx1+ty1*ty1)*Ebeam*Ebeam; // from inner stations
         Double_t tr2=(tx2*tx2+ty2*ty2)*Ebeam*Ebeam; // from outer stations
         tcomb=(tr1+tr2)/2.; // standard subtraction 
         Double_t tyc=(ty1+ty2)/2.; // Inner-outer combination only in y
         if(light) tcomb=(tx2*tx2+tyc*tyc)*Ebeam*Ebeam; // subtraction light outer
//         cout << "trec subtraction = " << (tr1+tr2)/2. << " subtraction light = " << tcomb << "\n";
         tx_sub=(tx1*tx1+tx2*tx2)*Ebeam*Ebeam/2.; // tx from subtraction
         ty_sub=(ty1*ty1+ty2*ty2)*Ebeam*Ebeam/2.; // ty from subtraction

	// local angle in x and subtraction in y
	 tx_loc=Theta_x_loc*Theta_x_loc*Ebeam*Ebeam;
	 ty_loc=Theta_y_loc*Theta_y_loc*Ebeam*Ebeam;
	 tcomb_loc=tx_loc+ty_sub;

	// Lattice 
	 Double_t Theta_x_lattice=(trx1+trx2-trx3-trx4)/4.;
//	 Double_t tx_lat=(tx1_lattice*tx1_lattice+tx2_lattice*tx2_lattice)*Ebeam*Ebeam/2.;
	 Double_t tx_lat=ttx_lattice*ttx_lattice*Ebeam*Ebeam;
	 Double_t ty_lat=tty_lattice*tty_lattice*Ebeam*Ebeam;
	 tcomb_lat=tx_lat+ty_lat;
       
       // new by-side reconstruction method, aska local subtraction 
       
         Double_t txs1=(Tx1[0][0]*x2-Tx2[0][0]*x1)*1000./(Tx1[0][0]*Tx2[0][1]-Tx2[0][0]*Tx1[0][1]);
         Double_t txs2=(Tx4[0][0]*x3-Tx3[0][0]*x4)*1000./(Tx4[0][0]*Tx3[0][1]-Tx3[0][0]*Tx4[0][1]);
         Double_t txsc=0.5*(txs1-txs2);
         Double_t tx_side=(txsc*txsc*1E-12)*Ebeam*Ebeam;
         
         Double_t tys1=(Ty1[0][0]*y2-Ty2[0][0]*y1)*1000./(Ty1[0][0]*Ty2[0][1]-Ty2[0][0]*Ty1[0][1]);
         Double_t tys2=(Ty4[0][0]*y3-Ty3[0][0]*y4)*1000./(Ty4[0][0]*Ty3[0][1]-Ty3[0][0]*Ty4[0][1]);
         Double_t tysc=0.5*(tys1-tys2);
         Double_t ty_side=(tysc*tysc*1E-12)*Ebeam*Ebeam;
         
         tcomb_side=tx_side+ty_side;
	 Theta_x_loc=Theta_x_loc*1.E6; // return in microrad 
	 Theta_y_loc=Theta_y_loc*1.E6; // return in microrad 
	 Theta_y_loc_l=Theta_y_loc_l*1.E6; // return in microrad 
	 Theta_y_loc_r=Theta_y_loc_r*1.E6; // return in microrad 
//	 cout << "trec: subtraction = " << tcomb << " local angle = " << tcomb_loc << " lattice = " << tcomb_lat << " local subtraction " << tcomb_side << "\n"; 
}

void trecL(Double_t x1, Double_t x2, Double_t x3, Double_t x4,
	  Double_t y1, Double_t y2, Double_t y3, Double_t y4,
	  Int_t Icase){
    
    // special version of t-reconstruction, only subtraction method, for lower topologies 3/4, 2/4, 1+1/4, 
    // case identified by Icase
        Bool_t opt=true;
        Double_t tx,ty,tx1,tx2,ty1,ty2,tr1,tr2;
        Double_t txs,tys,tx_side,ty_side,tcomb_side,tx_sub,ty_sub;
         if(Icase==2 || Icase==3){ // 3/4i C-Side
            txs=(Tx4[0][0]*x3-Tx3[0][0]*x4)*1000./(Tx4[0][0]*Tx3[0][1]-Tx3[0][0]*Tx4[0][1]);
            tx_side=(txs*txs*1E-12)*Ebeam*Ebeam;         
            tys=(Ty4[0][0]*y3-Ty3[0][0]*y4)*1000./(Ty4[0][0]*Ty3[0][1]-Ty3[0][0]*Ty4[0][1]);
            ty_side=(tys*tys*1E-12)*Ebeam*Ebeam;
            tcomb_side=tx_side+ty_side;
            tx=(x2/1000.-x3/1000.)/(Lx2+Lx3);
            ty=(y2/1000.-y3/1000.)/(Ly2+Ly3);
            tx_sub=tx*tx*Ebeam*Ebeam;
            ty_sub=ty*ty*Ebeam*Ebeam;
            tcomb=(tx*tx+ty*ty)*Ebeam*Ebeam; 
            if(opt) tcomb=ty_sub+tx_side;
         }
         if(Icase==4 || Icase==5){ // 3/4i A-side
            txs=(Tx1[0][0]*x2-Tx2[0][0]*x1)*1000./(Tx1[0][0]*Tx2[0][1]-Tx2[0][0]*Tx1[0][1]);
            tx_side=(txs*txs*1E-12)*Ebeam*Ebeam;
            tys=(Ty1[0][0]*y2-Ty2[0][0]*y1)*1000./(Ty1[0][0]*Ty2[0][1]-Ty2[0][0]*Ty1[0][1]);
            ty_side=(tys*tys*1E-12)*Ebeam*Ebeam;
            tcomb_side=tx_side+ty_side;
            tx=(x2/1000.-x3/1000.)/(Lx2+Lx3);
            ty=(y2/1000.-y3/1000.)/(Ly2+Ly3);
            tx_sub=tx*tx*Ebeam*Ebeam;
            ty_sub=ty*ty*Ebeam*Ebeam;
            tcomb=(tx*tx+ty*ty)*Ebeam*Ebeam;   
            if(opt) tcomb=ty_sub+tx_side;
         }
         if(Icase==6 || Icase==7 || Icase==8 || Icase==9){ // 3/4o
            tx=(x1/1000.-x4/1000.)/(Lx1+Lx4);
            ty=(y1/1000.-y4/1000.)/(Ly1+Ly4);
            tcomb=(tx*tx+ty*ty)*Ebeam*Ebeam;   
         }
         if(Icase==10 || Icase==12){ // 2/4
            txs=(Tx1[0][0]*x2-Tx2[0][0]*x1)*1000./(Tx1[0][0]*Tx2[0][1]-Tx2[0][0]*Tx1[0][1]);
            tx_side=(txs*txs*1E-12)*Ebeam*Ebeam;
            tys=(Ty1[0][0]*y2-Ty2[0][0]*y1)*1000./(Ty1[0][0]*Ty2[0][1]-Ty2[0][0]*Ty1[0][1]);
            ty_side=(tys*tys*1E-12)*Ebeam*Ebeam;
            tx1=x1/1000./Lx1;
            ty1=y1/1000./Ly1;
            tr1=(tx1*tx1+ty1*ty1)*Ebeam*Ebeam;   
            tx2=x2/1000./Lx2;
            ty2=y2/1000./Ly2;
            tr2=(tx2*tx2+ty2*ty2)*Ebeam*Ebeam;   
            tcomb=(tr1+tr2)/2.; // standard subtraction 
            tcomb_side=tx_side+ty_side;
            if(opt) tcomb=tcomb_side;
         }
         if(Icase==11 || Icase==13){ // 2/4
            txs=(Tx4[0][0]*x3-Tx3[0][0]*x4)*1000./(Tx4[0][0]*Tx3[0][1]-Tx3[0][0]*Tx4[0][1]);
            tx_side=(txs*txs*1E-12)*Ebeam*Ebeam;         
            tys=(Ty4[0][0]*y3-Ty3[0][0]*y4)*1000./(Ty4[0][0]*Ty3[0][1]-Ty3[0][0]*Ty4[0][1]);
            ty_side=(tys*tys*1E-12)*Ebeam*Ebeam;
            tcomb_side=tx_side+ty_side;
            tx1=x3/1000./Lx3;
            ty1=y3/1000./Ly3;
            tr1=(tx1*tx1+ty1*ty1)*Ebeam*Ebeam;   
            tx2=x4/1000./Lx4;
            ty2=y4/1000./Ly4;
            tr2=(tx2*tx2+ty2*ty2)*Ebeam*Ebeam;   
            tcomb=(tr1+tr2)/2.; // standard subtraction
            if(opt) tcomb=tcomb_side;
         }
         if(Icase==14 || Icase==15){ // 1+1/4i
            tx=(x2/1000.-x3/1000.)/(Lx2+Lx3);
            ty=(y2/1000.-y3/1000.)/(Ly2+Ly3);
            tcomb=(tx*tx+ty*ty)*Ebeam*Ebeam;   
         }

} 


