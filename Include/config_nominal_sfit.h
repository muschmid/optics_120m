// all global setting and constants of the total cross section analysis are defined here
Double_t dL,edL,ddL,xL,exL; // actual values are set in lumidat.h
//Double_t ddL=0.0131; // Syst lumi error 8 TeV
//Double_t dL=92060.0; // MC luminosity in 1/mb
//Double_t edL=314.; // statistical error 
//Double_t dL=1.; // no lumi, dN/dt
//Double_t edL=100.0;  

Double_t xf=1.000; // life fraction set to one as already applied to the luminosity
//const Double_t xt=0.96957; // some event lost pythia-->Ntuple (trigger efficiency) 4m file
Double_t xt=1.000; // trigger efficiency
//Double_t xL=dL*xf*xt; // recorded luminosity 
//Double_t exL=edL*xf*xt;
//Double_t eps[2]={0.844,0.821},deps[2]={0.005,0.005}; // full efficiency 
Double_t eps[2]={0}; // actual values are set in lumidat.h
Double_t deps[2]={0.001,0.001}; // statistical error on RecoEff, values are set in lumidat.h  
Double_t seps[2]={0.005,0.005}; //Systematic Errors on RecoEff
Double_t ddE=0.005; // average reconstruction efficiency syst.err.

 Bool_t noselect=false;
 Bool_t Rafal=false,Optics=true,Matthieu=false,transp=false,smear=true;
 Bool_t covariance = true;
 Bool_t MCG4=false,MCTB=false,MCy=false,RScale_low=false,RScale_high=false;
 Bool_t MCoffset=false; //offsets are so small with ELSA 08.20.2028 that I decide to not include anymore. to be verified with later versions 
 Bool_t offt=false; // time-dependent x-offset, correction wrt chi2
 Bool_t offty=false; // time-dependent x-offset, correction wrt chi2
 Bool_t classic=false,light=true;
 Double_t xpi = 3.14159265359;
 Double_t conv=0.389379; // (hbar c)**2 conversion to mbarn
 Double_t rho=0.10; // error 0.0034
 Bool_t arm1=false,arm2=false; 
 
// Double_t tlow=0.00030,thigh=0.197666; // 400003
// Double_t tlow=0.0004,thigh=0.197666; // 400002 14 TeV
// Double_t tlow=0.0005,thigh=0.197666; // 400001
 Double_t tlow=0.00035,thigh=0.197666; // 400002 13.5 TeV
 
 

Double_t txlow=0.01,txhigh=0.18,tylow=0.009,tyhigh=0.050;
