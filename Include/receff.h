/**
 *  Macro description:
 *
 *	Calculate reconstruction efficiency per arm 
 *  Input:
 * 
 * 		Int_t            IA =1,2; arm1 arm2 
 *		NID[26]          number of events per topology   
 * 
 *  Output
 *	    Double_t           receff
 *
 *  @date    2019-07-19
 *  @author  Hasko Stenzel
 *
 **/

#include "TMath.h"
#include "TMathBase.h"

Double_t receff(Int_t IA , Double_t NID[26], Double_t BOF[26], Double_t xEOF[26]){

    Int_t const Ncase=26;
    Double_t yield[Ncase]={0.0};
    for(Int_t ii=0; ii<Ncase; ii++){
        if(xEOF[ii]>0.0){
            yield[ii]=(NID[ii]-BOF[ii])/xEOF[ii]; // Background subtraction, assume BOF contains the sum of EventMixing and DPE background.  
            if(yield[ii]<0.0) yield[ii]=0.0; // reset negative cross section
        }
    }
    // probabilistic calculation of lowest topologies 
    yield[16]=yield[6]*yield[8]/yield[0]+yield[4]*yield[8]/yield[0]+yield[2]*yield[6]/yield[0];
    yield[17]=yield[7]*yield[9]/yield[1]+yield[5]*yield[9]/yield[1]+yield[3]*yield[7]/yield[1];
    yield[18]=yield[2]*yield[11]/yield[0];
    yield[19]=yield[4]*yield[10]/yield[0];
    yield[20]=yield[3]*yield[12]/yield[1];
    yield[21]=yield[5]*yield[13]/yield[1];
    yield[22]=yield[6]*yield[11]/yield[0]+yield[8]*yield[10]/yield[0];
    yield[23]=yield[7]*yield[12]/yield[1]+yield[9]*yield[12]/yield[1];
    yield[24]=yield[10]*yield[11]/yield[0];
    yield[25]=yield[12]*yield[13]/yield[1];
    
    Double_t result=0.0;
    if(IA<1 || IA >202) cout << " receff: no arm selected, need arm=1,2 or partial 3-6" << "\n";
    
    // total receff arm1
    if(IA==1){
        if(yield[0]==0){
            cout << " receff: no 4/4 events arm 1" << "\n";
        }
        else{
            result=yield[0]/
            (yield[0]+yield[2]+yield[4]+yield[6]+yield[8]+yield[10]+yield[11]+yield[14]+yield[16]+yield[18]+yield[19]+yield[22]+yield[24]);
        }
    }
    // total receff arm2
    if(IA==2){
        if(yield[1]==0){
            cout << " receff: no 4/4 events arm 2" << "\n";
        }
        else{
            result=yield[1]/
            (yield[1]+yield[3]+yield[5]+yield[7]+yield[9]+yield[12]+yield[13]+yield[15]+yield[17]+yield[20]+yield[21]+yield[23]+yield[25]);
        }
    }
    // partial 3/4 receff arm1
    if(IA==3){
        if(yield[0]==0){
            cout << " receff: no 4/4 events arm 1" << "\n";
        }
        else{
            result=yield[0]/
            (yield[0]+yield[2]+yield[4]+yield[6]+yield[8]);
        }
    }
    // partial 3/4 receff arm2
    if(IA==4){
        if(yield[1]==0){
            cout << " receff: no 4/4 events arm 2" << "\n";
        }
        else{
            result=yield[1]/
            (yield[1]+yield[3]+yield[5]+yield[7]+yield[9]);
        }
    }
    // partial 2/4 receff arm1
    if(IA==5){
        if(yield[0]==0){
            cout << " receff: no 4/4 events arm 1" << "\n";
        }
        else{
            result=yield[0]/
            (yield[0]+yield[10]+yield[11]);
        }
    }
    // partial 2/4 receff arm2
    if(IA==6){
        if(yield[1]==0){
            cout << " receff: no 4/4 events arm 2" << "\n";
        }
        else{
            result=yield[1]/
            (yield[1]+yield[12]+yield[13]);
        }
    }
    // epsilon_rec 2/4 case 10
    if(IA==110){
        if(yield[10]==0){
            cout << " receff: no 2/4 events arm 1 case 10" << "\n";
        }
        else{
            result=yield[10]/
            (yield[0]+yield[2]+yield[4]+yield[6]+yield[8]+yield[10]+yield[11]+yield[14]+yield[16]+yield[18]+yield[19]+yield[22]+yield[24]);
        }
    }
    // epsilon rec 2/4 case 11 
    if(IA==111){
        if(yield[11]==0){
            cout << " receff: no 2/4 events arm 1 case 11" << "\n";
        }
        else{
            result=yield[11]/
            (yield[0]+yield[2]+yield[4]+yield[6]+yield[8]+yield[10]+yield[11]+yield[14]+yield[16]+yield[18]+yield[19]+yield[22]+yield[24]);
        }
    }
    // epsilon rec 2/4 case 12 
    if(IA==112){
        if(yield[12]==0){
            cout << " receff: no 2/4 events arm 2 case 12" << "\n";
        }
        else{
            result=yield[12]/
            (yield[1]+yield[3]+yield[5]+yield[7]+yield[9]+yield[12]+yield[13]+yield[15]+yield[17]+yield[20]+yield[21]+yield[23]+yield[25]);
        }
    }
    // epsilon rec 2/4 case 13 
    if(IA==113){
        if(yield[13]==0){
            cout << " receff: no 2/4 events arm 2 case 13" << "\n";
        }
        else{
            result=yield[13]/
            (yield[1]+yield[3]+yield[5]+yield[7]+yield[9]+yield[12]+yield[13]+yield[15]+yield[17]+yield[20]+yield[21]+yield[23]+yield[25]);
        }
    }
    
    // reconstructed arm1
    if(IA==101){
        if(yield[0]==0){
            cout << " receff: no 4/4 events arm 1" << "\n";
        }
        else{
            result=yield[0];
        }
    }
    // failed arm1
    if(IA==102){
        if(yield[0]==0){
            cout << " receff: no 4/4 events arm 1" << "\n";
        }
        else{
            result=
            (yield[0]+yield[2]+yield[4]+yield[6]+yield[8]+yield[10]+yield[11]+yield[14]+yield[16]+yield[18]+yield[19]+yield[22]+yield[24]);
        }
    }
    // reconstructed arm2
    if(IA==201){
        if(yield[1]==0){
            cout << " receff: no 4/4 events arm 2" << "\n";
        }
        else{
            result=yield[1];
        }
    }
    // reconstructed arm2
    if(IA==202){
        if(yield[1]==0){
            cout << " receff: no 4/4 events arm 2" << "\n";
        }
        else{
            result=
            (yield[1]+yield[3]+yield[5]+yield[7]+yield[9]+yield[12]+yield[13]+yield[15]+yield[17]+yield[20]+yield[21]+yield[23]+yield[25]);
        }
    }
    return(result);
}




