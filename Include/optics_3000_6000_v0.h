// design optics 2.5km at 13 TeV
// v2 betay phase advance matched at 241m + horizontal away from 90. 
Double_t Ebeam=7000.;
Double_t dz=8264.4, dz1=8267.0, dz2=8261.8;
Double_t emxb1=2.0E-6,emyb1=2.0E-6,emxb2=2.0E-6,emyb2=2.0E-6; //  emittance at ALFA run guess
Double_t gammap=Ebeam/0.938;

Double_t betax_b1=3000.;
Double_t alfax_b1=0.0;
Double_t betay_b1=6000.;
Double_t alfay_b1=0.0; 
Double_t betax_b2=3000.;
Double_t alfax_b2=0.0;
Double_t betay_b2=6000.;
Double_t alfay_b2=0.0;

//  "XRP.A7R1.B1"    237.398 
Double_t betax_rp1=398.9620983;
Double_t alfax_rp1=5.198031041;
Double_t mux_rp1=0.500852947;

Double_t betay_rp1=20.14768722;
Double_t alfay_rp1=-0.1796984011;
Double_t muy_rp1=0.3033733506; 

// "XRP.B7R1.B1"                  245.65 

Double_t betax_rp2=317.9007934;
Double_t alfax_rp2=4.618063038;
Double_t mux_rp2=0.5045437623;

Double_t betay_rp2=26.60961831;
Double_t alfay_rp2=-0.6028071798;
Double_t muy_rp2=0.3614140163;

// "XRP.A7L1.B2"                  237.398

Double_t betax_rp3=409.0083635;
Double_t alfax_rp3=5.341711197;
Double_t mux_rp3=0.5006263639;

Double_t betay_rp3=23.74216287;
Double_t alfay_rp3=-0.4148312982;
Double_t muy_rp3=0.3375843196; 

// "XRP.B7L1.B2"                  245.65

Double_t betax_rp4=325.7088882;
Double_t alfax_rp4=4.745413442;
Double_t mux_rp4=0.5042275965;

Double_t betay_rp4=33.96009386;
Double_t alfay_rp4=-0.8225059499;
Double_t muy_rp4=0.3845490316;
Double_t pi=3.141592654;

Double_t Lx3=sqrt(betax_rp1*betax_b1)*sin(mux_rp1*2.*pi);
Double_t Ly3=sqrt(betay_rp1*betay_b1)*sin(muy_rp1*2.*pi);
Double_t Lx4=sqrt(betax_rp2*betax_b1)*sin(mux_rp2*2.*pi);
Double_t Ly4=sqrt(betay_rp2*betay_b1)*sin(muy_rp2*2.*pi);

Double_t Lx2=sqrt(betax_rp3*betax_b2)*sin(mux_rp3*2.*pi);
Double_t Ly2=sqrt(betay_rp3*betay_b2)*sin(muy_rp3*2.*pi);
Double_t Lx1=sqrt(betax_rp4*betax_b2)*sin(mux_rp4*2.*pi);
Double_t Ly1=sqrt(betay_rp4*betay_b2)*sin(muy_rp4*2.*pi);

Double_t LB2y=(Ly2-Ly1)/(dz/1000.);
Double_t LB1y=(Ly4-Ly3)/(dz/1000.);

Double_t LB2x=(Lx2-Lx1)/(dz/1000.);
Double_t LB1x=(Lx4-Lx3)/(dz/1000.);


// Transport Matrix elements

Double_t ax3[4]={sqrt(betax_rp1/betax_b1)*(cos(mux_rp1*2.*pi)+alfax_b1*sin(mux_rp1*2.*pi)), Lx3,
                 ((alfax_b1-alfax_rp1)*cos(mux_rp1*2.*pi)-(1.+alfax_b1*alfax_rp1)*sin(mux_rp1*2.*pi))/sqrt(betax_rp1*betax_b1),
                   sqrt(betax_b1/betax_rp1)*(cos(mux_rp1*2.*pi)-alfax_rp1*sin(mux_rp1*2.*pi))};

Double_t ay3[4]={sqrt(betay_rp1/betay_b1)*(cos(muy_rp1*2.*pi)+alfay_b1*sin(muy_rp1*2.*pi)), Ly3,
                 ((alfay_b1-alfay_rp1)*cos(muy_rp1*2.*pi)-(1.+alfay_b1*alfay_rp1)*sin(muy_rp1*2.*pi))/sqrt(betay_rp1*betay_b1),
                   sqrt(betay_b1/betay_rp1)*(cos(muy_rp1*2.*pi)-alfay_rp1*sin(muy_rp1*2.*pi))};

Double_t ax4[4]={sqrt(betax_rp2/betax_b1)*(cos(mux_rp2*2.*pi)+alfax_b1*sin(mux_rp2*2.*pi)), Lx4,
                 ((alfax_b1-alfax_rp2)*cos(mux_rp2*2.*pi)-(1.+alfax_b1*alfax_rp2)*sin(mux_rp2*2.*pi))/sqrt(betax_rp2*betax_b1),
                   sqrt(betax_b1/betax_rp2)*(cos(mux_rp2*2.*pi)-alfax_rp2*sin(mux_rp2*2.*pi))};

Double_t ay4[4]={sqrt(betay_rp2/betay_b1)*(cos(muy_rp2*2.*pi)+alfay_b1*sin(muy_rp2*2.*pi)), Ly4,
                 ((alfay_b1-alfay_rp2)*cos(muy_rp2*2.*pi)-(1.+alfay_b1*alfay_rp2)*sin(muy_rp2*2.*pi))/sqrt(betay_rp2*betay_b1),
                   sqrt(betay_b1/betay_rp2)*(cos(muy_rp2*2.*pi)-alfay_rp2*sin(muy_rp2*2.*pi))};

Double_t ax2[4]={sqrt(betax_rp3/betax_b2)*(cos(mux_rp3*2.*pi)+alfax_b2*sin(mux_rp3*2.*pi)), Lx2,
                 ((alfax_b2-alfax_rp3)*cos(mux_rp3*2.*pi)-(1+alfax_b2*alfax_rp3)*sin(mux_rp3*2.*pi))/sqrt(betax_rp3*betax_b2),
                   sqrt(betax_b2/betax_rp3)*(cos(mux_rp3*2.*pi)-alfax_rp3*sin(mux_rp3*2.*pi))};

Double_t ay2[4]={sqrt(betay_rp3/betay_b2)*(cos(muy_rp3*2.*pi)+alfay_b2*sin(muy_rp3*2.*pi)), Ly2,
                 ((alfay_b2-alfay_rp3)*cos(muy_rp3*2.*pi)-(1.+alfay_b2*alfay_rp3)*sin(muy_rp3*2.*pi))/sqrt(betay_rp3*betay_b2),
                   sqrt(betay_b2/betay_rp3)*(cos(muy_rp3*2.*pi)-alfay_rp3*sin(muy_rp3*2.*pi))};

Double_t ax1[4]={sqrt(betax_rp4/betax_b2)*(cos(mux_rp4*2.*pi)+alfax_b2*sin(mux_rp4*2.*pi)), Lx1,
                 ((alfax_b2-alfax_rp4)*cos(mux_rp4*2.*pi)-(1.+alfax_b2*alfax_rp4)*sin(mux_rp4*2.*pi))/sqrt(betax_rp4*betax_b2),
                   sqrt(betax_b2/betax_rp4)*(cos(mux_rp4*2.*pi)-alfax_rp4*sin(mux_rp4*2.*pi))};

Double_t ay1[4]={sqrt(betay_rp4/betay_b2)*(cos(muy_rp4*2.*pi)+alfay_b2*sin(muy_rp4*2.*pi)), Ly1,
                 ((alfay_b2-alfay_rp4)*cos(muy_rp4*2.*pi)-(1.+alfay_b2*alfay_rp4)*sin(muy_rp4*2.*pi))/sqrt(betay_rp4*betay_b2),
                   sqrt(betay_b2/betay_rp4)*(cos(muy_rp4*2.*pi)-alfay_rp4*sin(muy_rp4*2.*pi))};

TArrayD Ax1(4,ax1),Ay1(4,ay1),Ax2(4,ax2),Ay2(4,ay2),Ax3(4,ax3),Ay3(4,ay3),Ax4(4,ax4),Ay4(4,ay4);
TMatrixD Mx1(2,2),My1(2,2),Mx2(2,2),My2(2,2),Mx3(2,2),My3(2,2),Mx4(2,2),My4(2,2);

Double_t dLx1=ax1[3],dLx2=ax2[3],dLx3=ax3[3],dLx4=ax4[3];


