  //Run Header
  
  // Declaration of leaf types
  Int_t           Fill_num;
  Int_t           Run_num;
  Bool_t          Trigger_set[8][6];
  Int_t           Latency[8][3];
  vector<string>* mainDet_gainMode;
  Int_t           Threshold[8][3];
  Int_t           Gain[8][5];
  Double_t         TransformDetRP[8][4][4];
  Double_t         Proton_momentum;

  //Event Header

  UInt_t          Evt_num;
  Int_t           BCId;
  Int_t           TimeStp;
  Int_t           TimeStp_ns;
  Int_t           Scaler[8];
  UInt_t          Lum_block;
  Bool_t          TrigPat[8][16];
  Int_t           QDC_Trig[8][2];
  Bool_t          LVL1TrigSig[256];
  Bool_t          LVL2TrigSig[256];
  Bool_t          HLTrigSig[256];
  Bool_t          FiberHitsMD[8][20][64];
  Bool_t          FiberHitsODPos[8][3][30];
  Bool_t          FiberHitsODNeg[8][3][30];
  Int_t           PotPos;
  Int_t           Temperature;
  Int_t           Volt;
  Int_t           RadDose;
  Int_t           BeamLossMon;
  Int_t           TRate;
  Int_t           BPM;
  Bool_t          DQFlag[25];
  Int_t           MultiMD[8][20];
  Int_t           MultiODPos[8][3];
  Int_t           MultiODNeg[8][3];
 
  //Tracking Data
  const int MAXNUMTRACKS     = 100;

  // Declaration of leaf types
  Int_t           NumTrack;
  Bool_t          RecFlag[MAXNUMTRACKS][8];   //[NumTrack]
  Int_t           Detector[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         x_Det[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         y_Det[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         x_Pot[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         y_Pot[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         x_Stat[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         y_Stat[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         x_LHC[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         y_LHC[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         x_LHC_Corr[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         y_LHC_Corr[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         x_Beam[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         y_Beam[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         OverU[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         OverV[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         OverY[MAXNUMTRACKS][8];   //[NumTrack]
  Int_t           NU[MAXNUMTRACKS][8];   //[NumTrack]
  Int_t           NV[MAXNUMTRACKS][8];   //[NumTrack]
  Int_t           NY[MAXNUMTRACKS][8];   //[NumTrack]
  Int_t           Fib_SelMD[MAXNUMTRACKS][8][20];   //[NumTrack]
  Int_t           Fib_SelOD[MAXNUMTRACKS][8][3];   //[NumTrack]
  Double_t         intercept_Det[MAXNUMTRACKS][8];   //[NumTrack]
  Double_t         slope_Det[MAXNUMTRACKS][8];   //[NumTrack]

  //Global Tracks

  // Declaration of leaf types
  Int_t           NumGlobTrack;
  Double_t         intercept_LHC[1];   //[NumGlobTrack]
  Double_t         slope_LHC[1];   //[NumGlobTrack]
  Int_t           Track[2];
  Int_t           Pot[2];

  //COOL
	struct BLM
	{
		Int_t iTimeStp;
		Int_t iTimeStp_ns;
		Double_t blm1310ms[6];
	};

	struct HVChan
	{
		Int_t iTimeStp;
		Int_t iTimeStp_ns;
		Double_t Actual_VMeas[216];
		Double_t Actual_IMeas[216];
	};

	struct LocMon
	{
		Int_t iTimeStp;
		Int_t iTimeStp_ns;
		Double_t Temperature_sensor1[8];
		Double_t Temperature_sensor2[8];
		Double_t Temperature_sensor3[8];
		Double_t Temperature_sensor4[8];
		Double_t Temperature_sensor5[8];
	};

	struct Mov
	{
		Int_t iTimeStp;
		Int_t iTimeStp_ns;
		Double_t positionRO_lvdt[8];
		Double_t positionRO_motor[8];
	};

	struct Rad
	{
		Int_t iTimeStp;
		Int_t iTimeStp_ns;
		Double_t dose_value[4];
		Double_t fluence_value[4];
		Double_t temperature_value[4];
	};

	struct Rate
	{
		Int_t iTimeStp;
		Int_t iTimeStp_ns;
		Double_t Trate[8];
	};

