/**
 *  Macro description:
 *
 *	The procedure in this header does track matching and calculates the best track candidates for 4/4, 3/4, 2/4 and 1+1/4 events.
 *	It runs at analysis runtime, requireing 
 * 
 * 		Int_t           Detector[12][8];
 *		Float_t         x_Det[12][8];   
 *		Float_t         y_Det[12][8]; 
 *		Int_t 		BestTrackCand[8];
 *		Int_t           numtrack;
 *		 
 *	whereas x and y coordinates are in beam coordinate system, so it requires corrected coordinates.
 *	The procedure will modify the BestTrackCand[8] array for any given event with the best track number for each detector.
 *	
 *	At runtime execude once ComputeSig();
 *	And for each given event call GetBestTrackCombination(BestTrackCand,Detector,x_Det,y_Det,numtrack,runmode); whereas runmode = 0 for x+y matching, 1 for x only matching and 2 for y only matching. 0 and 1 are yet untested in this version.
 *
 *
 *  @date    2016-12-14
 *  @author  Matthieu Heller, Kristof Kreutzfeldt, Christian Heinz
 *
 **/

#include "TMath.h"
#include "TMathBase.h"

const Double_t sigtetaY0257=3.379;
const Double_t sigtetaY1346=3.338;
const Double_t sigtetaX0257=39.49;
const Double_t sigtetaX1346=39.17;

Float_t Leffx[8];
Float_t Leffy[8];

Float_t sigdiv2x[8][8];
Float_t sigdiv2y[8][8];

Float_t chix[8];
Float_t chiy[8];
Float_t chi[8];

const int maxnumtracks = 12;

Float_t sig2x[maxnumtracks][8][8];
Float_t sig2y[maxnumtracks][8][8];

Float_t RLeffx[8][8];
Float_t sigRLeffx[8][8];
Float_t RLeffy[8][8];
Float_t sigRLeffy[8][8];

Float_t sigres2[8];

void ComputeSig()
{

Leffy[0]=2.483452e+02*1000.; Leffy[1]=Leffy[0];
Leffy[2]=2.757912e+02*1000.; Leffy[3]=Leffy[2];
Leffy[4]=2.763695e+02*1000.; Leffy[5]=Leffy[4];
Leffy[6]=2.487637e+02*1000.; Leffy[7]=Leffy[6];

Leffx[0]=-1.569538e+01*1000.; Leffx[1]=Leffx[0];
Leffx[2]=-1.029734e+01*1000.; Leffx[3]=Leffx[2];
Leffx[4]=-1.045646e+01*1000.; Leffx[5]=Leffx[4];
Leffx[6]=-1.588127e+01*1000.; Leffx[7]=Leffx[6];

for (Int_t dn=0; dn< 8; dn++) //sigdiv2x calculation
	{
	if(dn==0 || dn==2 || dn==5 || dn==7)
		{
		for (Int_t k=0; k< 8; k++)
			{
			if((k==0 || k==2 || k==5 || k==7) && k!=dn)
				{
				if((dn==0 && k==2) || (dn==2 && k==0) || (dn==5 && k==7) || (dn==7 && k==5)) sigdiv2x[dn][k]=0; //there is no divergance uncertainty for detectors on the same side
				else sigdiv2x[dn][k]=TMath::Power(sigtetaX0257*Leffx[dn]/1.E6,2);
				}
			}
		}

	if(dn==1 || dn==3 || dn==4 || dn==6)
		{
		for (Int_t k=0; k< 8; k++)
			{
			if((k==1 || k==3 || k==4 || k==6) && k!=dn)
				{
				if((dn==1 && k==3) || (dn==3 && k==1) || (dn==4 && k==6) || (dn==6 && k==4)) sigdiv2x[dn][k]=0; //there is no divergance uncertainty for detectors on the same side
				else sigdiv2x[dn][k]=TMath::Power(sigtetaX1346*Leffx[dn]/1.E6,2);
				}
			}
		}
	}

for (Int_t dn=0; dn< 8; dn++) //sigdiv2y calculation
	{
	if(dn==0 || dn==2 || dn==5 || dn==7)
		{
		for (Int_t k=0; k< 8; k++)
			{
			if((k==0 || k==2 || k==5 || k==7) && k!=dn)
				{
				if((dn==0 && k==2) || (dn==2 && k==0) || (dn==5 && k==7) || (dn==7 && k==5)) sigdiv2y[dn][k]=0; //there is no divergance uncertainty for detectors on the same side
				else sigdiv2y[dn][k]=TMath::Power(sigtetaY0257*Leffy[dn]/1.E6,2);
				}
			}
		}

	if(dn==1 || dn==3 || dn==4 || dn==6)
		{
		for (Int_t k=0; k< 8; k++)
			{
			if((k==1 || k==3 || k==4 || k==6) && k!=dn)
				{
				if((dn==1 && k==3) || (dn==3 && k==1) || (dn==4 && k==6) || (dn==6 && k==4)) sigdiv2y[dn][k]=0; //there is no divergance uncertainty for detectors on the same side
				else sigdiv2y[dn][k]=TMath::Power(sigtetaY1346*Leffy[dn]/1.E6,2);
				}
			}
		}
	}

for (Int_t dn=0; dn< 8; dn++)
	{
	if(dn==0 ||dn==1 ||dn==6 || dn==7) sigres2[dn]=0.00091;			//old values from Karel were wrong! (0.0016 and 0.0009)
	else sigres2[dn]=0.00075;						//current values from tuned fast mc [in mm^2]
	}

//Ratios for X lever arms
//These values are determined from data, like described in the backup note (ALFA constraints)
//Some values, were indicated, are not determined from data, but the last effective optics values are used and the systematic uncertainty is guessed.
RLeffx[2][0]=-1.029734e+01/-1.569538e+01; sigRLeffx[2][0]=0.1;	// M_12_x 237/241 B2	(effective optics, sigma estimated)
RLeffx[0][2]=1/RLeffx[2][0]; sigRLeffx[0][2]=0.001;
RLeffx[0][7]=1./1.0151; sigRLeffx[0][7]=0.0049; 	// M_12_x (241 m) B2/B1
RLeffx[7][0]=1/RLeffx[0][7]; sigRLeffx[7][0]=sigRLeffx[0][7]/(RLeffx[0][7]*RLeffx[0][7]);
RLeffx[5][7]=-1.045646e+01/-1.588127e+01; sigRLeffx[5][7]=0.1; 	// M_12_x 237/241 B1	(effective optics, sigma estimated)
RLeffx[7][5]=1/RLeffx[5][7]; sigRLeffx[7][5]=0.001;
RLeffx[2][5]=1./1.0199;  sigRLeffx[2][5]=0.0050; 	// M_12_x (237 m) B2/B1
RLeffx[5][2]=1/RLeffx[2][5]; sigRLeffx[5][2]=sigRLeffx[2][5]/(RLeffx[2][5]*RLeffx[2][5]);

RLeffx[0][5]=1/2.*(RLeffx[0][2]*RLeffx[2][5]+RLeffx[0][7]*RLeffx[7][5]);	sigRLeffx[0][5]=1/2.*TMath::Sqrt(TMath::Power(sigRLeffx[0][2]*RLeffx[2][5],2)+TMath::Power(RLeffx[0][2]*sigRLeffx[2][5],2)+TMath::Power(sigRLeffx[0][7]*RLeffx[7][5],2)+ TMath::Power(RLeffx[0][7]*sigRLeffx[7][5],2));
RLeffx[5][0]=1/RLeffx[0][5]; 												sigRLeffx[5][0]=sigRLeffx[0][5]/(RLeffx[0][5]*RLeffx[0][5]);
RLeffx[2][7]=1/2.*(RLeffx[2][0]*RLeffx[0][7]+RLeffx[2][5]*RLeffx[5][7]);	sigRLeffx[2][7]=1/2.*TMath::Sqrt(TMath::Power(sigRLeffx[2][7]*RLeffx[0][7],2)+TMath::Power(RLeffx[2][7]*sigRLeffx[0][7],2)+TMath::Power(sigRLeffx[2][5]*RLeffx[5][7],2)+ TMath::Power(RLeffx[2][5]*sigRLeffx[5][7],2));
RLeffx[7][2]=1/RLeffx[2][7]; 												sigRLeffx[7][2]=sigRLeffx[2][7]/(RLeffx[2][7]*RLeffx[2][7]);


RLeffx[3][1]=RLeffx[2][0]; sigRLeffx[3][1]=sigRLeffx[2][0]; RLeffx[1][3]=RLeffx[0][2]; sigRLeffx[1][3]=sigRLeffx[0][2];
RLeffx[1][6]=RLeffx[0][7]; sigRLeffx[1][6]=sigRLeffx[0][7]; RLeffx[6][1]=RLeffx[7][0]; sigRLeffx[6][1]=sigRLeffx[7][0];
RLeffx[4][6]=RLeffx[5][7]; sigRLeffx[4][6]=sigRLeffx[5][7]; RLeffx[6][4]=RLeffx[7][5]; sigRLeffx[6][4]=sigRLeffx[7][5];
RLeffx[3][4]=RLeffx[2][5]; sigRLeffx[3][4]=sigRLeffx[2][5]; RLeffx[4][3]=RLeffx[5][2]; sigRLeffx[4][3]=sigRLeffx[5][2];

RLeffx[1][4]=RLeffx[0][5]; 	sigRLeffx[1][4]=sigRLeffx[0][5];
RLeffx[4][1]=RLeffx[5][0]; 	sigRLeffx[4][1]=sigRLeffx[5][0];
RLeffx[3][6]=RLeffx[2][7];	sigRLeffx[3][6]=sigRLeffx[2][7];
RLeffx[6][3]=RLeffx[7][2]; 	sigRLeffx[6][3]=sigRLeffx[7][2];


//Ratios for Y lever arms
RLeffy[2][0]=1.1107; sigRLeffy[2][0]=0.0009; 	// M_12_y 237/241 B2
RLeffy[0][2]=1/RLeffy[2][0]; 	sigRLeffy[0][2]=sigRLeffy[2][0]/(RLeffy[2][0]*RLeffy[2][0]);
RLeffy[0][7]=1./1.0014; sigRLeffy[0][7]=0.0018; 	// M_12_y (241 m) B2/B1
RLeffy[7][0]=1/RLeffy[0][7]; 	sigRLeffy[7][0]=sigRLeffy[0][7]/(RLeffy[0][7]*RLeffy[0][7]);
RLeffy[5][7]=1.1109; sigRLeffy[5][7]=0.0014; 	// M_12_y 237/241 B1
RLeffy[7][5]=1/RLeffy[5][7]; 	sigRLeffy[7][5]=sigRLeffy[5][7]/(RLeffy[5][7]*RLeffy[5][7]);
RLeffy[2][5]=1./1.0017; sigRLeffy[2][5]=0.0013; 	// M_12_y (237 m) B2/B1
RLeffy[5][2]=1/RLeffy[2][5];  	sigRLeffy[5][2]=sigRLeffy[2][5]/(RLeffy[2][5]*RLeffy[2][5]);

RLeffy[0][5]=1/2.*(RLeffy[0][2]*RLeffy[2][5]+RLeffy[0][7]*RLeffy[7][5]); 	sigRLeffy[0][5]=1/2.*TMath::Sqrt(TMath::Power(sigRLeffy[0][2]*RLeffy[2][5],2)+TMath::Power(RLeffy[0][2]*sigRLeffy[2][5],2)+TMath::Power(sigRLeffy[0][7]*RLeffy[7][5],2)+ TMath::Power(RLeffy[0][7]*sigRLeffy[7][5],2));
RLeffy[5][0]=1/RLeffy[0][5]; 												sigRLeffy[5][0]=sigRLeffy[0][5]/(RLeffy[0][5]*RLeffy[0][5]);
RLeffy[2][7]=1/2.*(RLeffy[2][0]*RLeffy[0][7]+RLeffy[2][5]*RLeffy[5][7]);	sigRLeffy[2][7]=1/2.*TMath::Sqrt(TMath::Power(sigRLeffy[2][7]*RLeffy[0][7],2)+TMath::Power(RLeffy[2][7]*sigRLeffy[0][7],2)+TMath::Power(sigRLeffy[2][5]*RLeffy[5][7],2)+ TMath::Power(RLeffy[2][5]*sigRLeffy[5][7],2));
RLeffy[7][2]=1/RLeffy[2][7]; 												sigRLeffy[7][2]=sigRLeffy[2][7]/(RLeffy[2][7]*RLeffy[2][7]);

RLeffy[3][1]=RLeffy[2][0]; sigRLeffy[3][1]=sigRLeffy[2][0]; RLeffy[1][3]=RLeffy[0][2]; 	sigRLeffy[1][3]=sigRLeffy[0][2];
RLeffy[1][6]=RLeffy[0][7]; sigRLeffy[1][6]=sigRLeffy[0][7]; RLeffy[6][1]=RLeffy[7][0]; 	sigRLeffy[6][1]=sigRLeffy[7][0];
RLeffy[4][6]=RLeffy[5][7]; sigRLeffy[4][6]=sigRLeffy[5][7]; RLeffy[6][4]=RLeffy[7][5]; 	sigRLeffy[6][4]=sigRLeffy[7][5];
RLeffy[3][4]=RLeffy[2][5]; sigRLeffy[3][4]=sigRLeffy[2][5]; RLeffy[4][3]=RLeffy[5][2];	sigRLeffy[4][3]=sigRLeffy[5][2];

RLeffy[1][4]=RLeffy[0][5]; 	sigRLeffy[1][4]=sigRLeffy[0][5];
RLeffy[4][1]=RLeffy[5][0]; 	sigRLeffy[4][1]=sigRLeffy[5][0];
RLeffy[3][6]=RLeffy[2][7];	sigRLeffy[3][6]=sigRLeffy[2][7];
RLeffy[6][3]=RLeffy[7][2]; 	sigRLeffy[6][3]=sigRLeffy[7][2];
}




void GetBestTrackCombination( Int_t BestTrackCand_internal[8] , Int_t Detector_internal[12][8] , Float_t x_A[12][8] , Float_t y_A[12][8] , Int_t numtrack_internal, Int_t runmode_internal){

Int_t cnt=0;

Float_t Chi2[2];		//write chi^2 for both arms to tree ([0]: arm 1368, [1]: arm 2457)
Float_t Chi2x[2];
Float_t Chi2y[2];

Float_t chiTOT;
Float_t chiTOT_X;
Float_t chiTOT_Y;

Float_t minChi=1e9;
Float_t minChi_x=1e9;
Float_t minChi_y=1e9;

	for (int i=0;i<8;i++)
		{
		//~ BestTrackCand_internal[i]=99;
		BestTrackCand_internal[i]=0;
		}
		
		
			{
			for (int i=0;i<numtrack_internal;i++)
				{
				for (Int_t dn=0; dn< 8; dn++)
					{
					if(dn==0 || dn==2 || dn==5 || dn==7) //sigma_y calculations
						{
						for (Int_t dm=0; dm< 8; dm++)
							{
							if((dm==0 || dm==2 || dm==5 || dm==7) && dm!=dn)
								{
								sig2y[i][dn][dm]=sigres2[dn]+sigres2[dm]*TMath::Power(RLeffy[dn][dm],2)+TMath::Power(y_A[i][dm]*sigRLeffy[dn][dm],2)+sigdiv2y[dn][dm];
								sig2x[i][dn][dm]=sigres2[dn]+sigres2[dm]*TMath::Power(RLeffx[dn][dm],2)+TMath::Power(x_A[i][dm]*sigRLeffx[dn][dm],2)+sigdiv2x[dn][dm];
								}
							}
						}
					}
				}
			if (Detector_internal[0][0]==1 && Detector_internal[0][2]==1 && Detector_internal[0][5]==1 && Detector_internal[0][7]==1)	// 4/4 elastic
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						for (int k=0;k<numtrack_internal;k++)
							{
							for (int l=0;l<numtrack_internal;l++)
								{
								if (Detector_internal[i][0]==1 && Detector_internal[j][2]==1 && Detector_internal[k][5]==1 && Detector_internal[l][7]==1)
									{
									chiy[5]=TMath::Power(-(y_A[i][0]*RLeffy[5][0])-(y_A[k][5]),2)/sig2y[i][5][0]+
											TMath::Power(-(y_A[j][2]*RLeffy[5][2])-(y_A[k][5]),2)/sig2y[j][5][2]+
											TMath::Power((y_A[l][7]*RLeffy[5][7])-(y_A[k][5]),2)/sig2y[l][5][7];

									chiy[7]=TMath::Power(-(y_A[i][0]*RLeffy[7][0])-(y_A[l][7]),2)/sig2y[i][7][0]+
											TMath::Power(-(y_A[j][2]*RLeffy[7][2])-(y_A[l][7]),2)/sig2y[j][7][2]+
											TMath::Power((y_A[k][5]*RLeffy[7][5])-(y_A[l][7]),2)/sig2y[k][7][5];

									chiy[2]=TMath::Power((y_A[i][0]*RLeffy[2][0])-(y_A[j][2]),2)/sig2y[i][2][0]+
											TMath::Power(-(y_A[k][5]*RLeffy[2][5])-(y_A[j][2]),2)/sig2y[k][2][5]+
											TMath::Power(-(y_A[l][7]*RLeffy[2][7])-(y_A[j][2]),2)/sig2y[l][2][7];

									chiy[0]=TMath::Power((y_A[j][2]*RLeffy[0][2])-(y_A[i][0]),2)/sig2y[j][0][2]+
											TMath::Power(-(y_A[k][5]*RLeffy[0][5])-(y_A[i][0]),2)/sig2y[k][0][5]+
											TMath::Power(-(y_A[l][7]*RLeffy[0][7])-(y_A[i][0]),2)/sig2y[l][0][7];

									chix[5]=TMath::Power(-(x_A[i][0]*RLeffx[5][0])-(x_A[k][5]),2)/sig2x[i][5][0]+
											TMath::Power(-(x_A[j][2]*RLeffx[5][2])-(x_A[k][5]),2)/sig2x[j][5][2]+
											TMath::Power((x_A[l][7]*RLeffx[5][7])-(x_A[k][5]),2)/sig2x[l][5][7];

									chix[7]=TMath::Power(-(x_A[i][0]*RLeffx[7][0])-(x_A[l][7]),2)/sig2x[i][7][0]+
											TMath::Power(-(x_A[j][2]*RLeffx[7][2])-(x_A[l][7]),2)/sig2x[j][7][2]+
											TMath::Power((x_A[k][5]*RLeffx[7][5])-(x_A[l][7]),2)/sig2x[k][7][5];

									chix[2]=TMath::Power((x_A[i][0]*RLeffx[2][0])-(x_A[j][2]),2)/sig2x[i][2][0]+
											TMath::Power(-(x_A[k][5]*RLeffx[2][5])-(x_A[j][2]),2)/sig2x[k][2][5]+
											TMath::Power(-(x_A[l][7]*RLeffx[2][7])-(x_A[j][2]),2)/sig2x[l][2][7];

									chix[0]=TMath::Power((x_A[j][2]*RLeffx[0][2])-(x_A[i][0]),2)/sig2x[j][0][2]+
											TMath::Power(-(x_A[k][5]*RLeffx[0][5])-(x_A[i][0]),2)/sig2x[k][0][5]+
											TMath::Power(-(x_A[l][7]*RLeffx[0][7])-(x_A[i][0]),2)/sig2x[l][0][7];

									chi[0]=chix[0]+chiy[0];
									chi[2]=chix[2]+chiy[2];
									chi[5]=chix[5]+chiy[5];
									chi[7]=chix[7]+chiy[7];

									chiTOT=chi[0]+chi[2]+chi[5]+chi[7];
									chiTOT_X=chix[0]+chix[2]+chix[5]+chix[7];
									chiTOT_Y=chiy[0]+chiy[2]+chiy[5]+chiy[7];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
										{
										minChi=chiTOT;
										minChi_x=chiTOT_X;
										minChi_y=chiTOT_Y;

										BestTrackCand_internal[0]=i;
										BestTrackCand_internal[2]=j;
										BestTrackCand_internal[5]=k;
										BestTrackCand_internal[7]=l;
										cnt++;
										}
									} // End of 4 Detector_internal selection
								} // End og loop over l
							} // End og loop over k
						} // End og loop over j
					} // End og loop over i
				} // End of 4/4 Detector_internal selection

			if (!(Detector_internal[0][0]==1) && Detector_internal[0][2]==1 && Detector_internal[0][5]==1 && Detector_internal[0][7]==1)	// 3/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						for (int k=0;k<numtrack_internal;k++)
							{
							if (Detector_internal[i][2]==1 && Detector_internal[j][5]==1 && Detector_internal[k][7]==1)
								{
								chiy[5]=TMath::Power(-(y_A[i][2]*RLeffy[5][2])-(y_A[j][5]),2)/sig2y[i][5][2]+
										TMath::Power((y_A[k][7]*RLeffy[5][7])-(y_A[j][5]),2)/sig2y[k][5][7];
								chiy[7]=TMath::Power(-(y_A[i][2]*RLeffy[7][2])-(y_A[k][7]),2)/sig2y[i][7][2]+
										TMath::Power((y_A[j][5]*RLeffy[7][5])-(y_A[k][7]),2)/sig2y[j][7][5];
								chiy[2]=TMath::Power(-(y_A[j][5]*RLeffy[2][5])-(y_A[i][2]),2)/sig2y[j][2][5]+
										TMath::Power(-(y_A[k][7]*RLeffy[2][7])-(y_A[i][2]),2)/sig2y[k][2][7];
								chix[5]=TMath::Power(-(x_A[i][2]*RLeffx[5][2])-(x_A[j][5]),2)/sig2x[i][5][2]+
										TMath::Power((x_A[k][7]*RLeffx[5][7])-(x_A[j][5]),2)/sig2x[k][5][7];
								chix[7]=TMath::Power(-(x_A[i][2]*RLeffx[7][2])-(x_A[k][7]),2)/sig2x[i][7][2]+
										TMath::Power((x_A[j][5]*RLeffx[7][5])-(x_A[k][7]),2)/sig2x[j][7][5];
								chix[2]=TMath::Power(-(x_A[j][5]*RLeffx[2][5])-(x_A[i][2]),2)/sig2x[j][2][5]+
										TMath::Power(-(x_A[k][7]*RLeffx[2][7])-(x_A[i][2]),2)/sig2x[k][2][7];

								chi[2]=chix[2]+chiy[2];
								chi[5]=chix[5]+chiy[5];
								chi[7]=chix[7]+chiy[7];

								chiTOT=chi[2]+chi[5]+chi[7];
								chiTOT_X=chix[2]+chix[5]+chix[7];
								chiTOT_Y=chiy[2]+chiy[5]+chiy[7];
								
									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
									{
									minChi=chiTOT;
									minChi_x=chiTOT_X;
									minChi_y=chiTOT_Y;
									BestTrackCand_internal[2]=i;
									BestTrackCand_internal[5]=j;
									BestTrackCand_internal[7]=k;
									}
								} // End of 4 Detector_internal selection
							} // End og loop over k
						} // End og loop over j
					} // End og loop over i
				} // End of 3/4 Detector_internal selection

			if (Detector_internal[0][0]==1 && !(Detector_internal[0][2]==1) && Detector_internal[0][5]==1 && Detector_internal[0][7]==1)	// 3/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						for (int k=0;k<numtrack_internal;k++)
							{
							if (Detector_internal[i][0]==1 && Detector_internal[j][5]==1 && Detector_internal[k][7]==1)
								{
								chiy[5]=TMath::Power(-(y_A[i][0]*RLeffy[5][0])-(y_A[j][5]),2)/sig2y[i][5][0]+
										TMath::Power((y_A[k][7]*RLeffy[5][7])-(y_A[j][5]),2)/sig2y[k][5][7];
								chiy[7]=TMath::Power(-(y_A[i][0]*RLeffy[7][0])-(y_A[k][7]),2)/sig2y[i][7][0]+
										TMath::Power((y_A[j][5]*RLeffy[7][5])-(y_A[k][7]),2)/sig2y[j][7][5];
								chiy[0]=TMath::Power(-(y_A[j][5]*RLeffy[0][5])-(y_A[i][0]),2)/sig2y[j][0][5]+
										TMath::Power(-(y_A[k][7]*RLeffy[0][7])-(y_A[i][0]),2)/sig2y[k][0][7];
								chix[5]=TMath::Power(-(x_A[i][0]*RLeffx[5][0])-(x_A[j][5]),2)/sig2x[i][5][0]+
										TMath::Power((x_A[k][7]*RLeffx[5][7])-(x_A[j][5]),2)/sig2x[k][5][7];
								chix[7]=TMath::Power(-(x_A[i][0]*RLeffx[7][0])-(x_A[k][7]),2)/sig2x[i][7][0]+
										TMath::Power((x_A[j][5]*RLeffx[7][5])-(x_A[k][7]),2)/sig2x[j][7][5];
								chix[0]=TMath::Power(-(x_A[j][5]*RLeffx[0][5])-(x_A[i][0]),2)/sig2x[j][0][5]+
										TMath::Power(-(x_A[k][7]*RLeffx[0][7])-(x_A[i][0]),2)/sig2x[k][0][7];

								chi[0]=chix[0]+chiy[0];
								chi[5]=chix[5]+chiy[5];
								chi[7]=chix[7]+chiy[7];

								chiTOT=chi[0]+chi[5]+chi[7];
								chiTOT_X=chix[0]+chix[5]+chix[7];
								chiTOT_Y=chiy[0]+chiy[5]+chiy[7];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
									{
									minChi=chiTOT;
									minChi_x=chiTOT_X;
									minChi_y=chiTOT_Y;
									BestTrackCand_internal[0]=i;
									BestTrackCand_internal[5]=j;
									BestTrackCand_internal[7]=k;
									}
								} // End of 4 Detector_internal selection
							} // End og loop over k
						} // End og loop over j
					} // End og loop over i
				} // End of 3/4 Detector_internal selection

			if (Detector_internal[0][0]==1 && Detector_internal[0][2]==1 && !(Detector_internal[0][5]==1) && Detector_internal[0][7]==1)	// 3/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						for (int k=0;k<numtrack_internal;k++)
							{
							if (Detector_internal[i][0]==1 && Detector_internal[j][2]==1 && Detector_internal[k][7]==1)
								{
								chiy[7]=TMath::Power(-(y_A[i][0]*RLeffy[7][0])-(y_A[k][7]),2)/sig2y[i][7][0]+
										TMath::Power(-(y_A[j][2]*RLeffy[7][2])-(y_A[k][7]),2)/sig2y[j][7][2];
								chiy[2]=TMath::Power((y_A[i][0]*RLeffy[2][0])-(y_A[j][2]),2)/sig2y[i][2][0]+
										TMath::Power(-(y_A[k][7]*RLeffy[2][7])-(y_A[j][2]),2)/sig2y[k][2][7];
								chiy[0]=TMath::Power((y_A[j][2]*RLeffy[0][2])-(y_A[i][0]),2)/sig2y[j][0][2]+
										TMath::Power(-(y_A[k][7]*RLeffy[0][7])-(y_A[i][0]),2)/sig2y[k][0][7];
								chix[7]=TMath::Power(-(x_A[i][0]*RLeffx[7][0])-(x_A[k][7]),2)/sig2x[i][7][0]+
										TMath::Power(-(x_A[j][2]*RLeffx[7][2])-(x_A[k][7]),2)/sig2x[j][7][2];
								chix[2]=TMath::Power((x_A[i][0]*RLeffx[2][0])-(x_A[j][2]),2)/sig2x[i][2][0]+
										TMath::Power(-(x_A[k][7]*RLeffx[2][7])-(x_A[j][2]),2)/sig2x[k][2][7];
								chix[0]=TMath::Power((x_A[j][2]*RLeffx[0][2])-(x_A[i][0]),2)/sig2x[j][0][2]+
										TMath::Power(-(x_A[k][7]*RLeffx[0][7])-(x_A[i][0]),2)/sig2x[k][0][7];

								chi[0]=chix[0]+chiy[0];
								chi[2]=chix[2]+chiy[2];
								chi[7]=chix[7]+chiy[7];

								chiTOT=chi[0]+chi[2]+chi[7];
								chiTOT_X=chix[0]+chix[2]+chix[7];
								chiTOT_Y=chiy[0]+chiy[2]+chiy[7];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
									{
									minChi=chiTOT;
									minChi_x=chiTOT_X;
									minChi_y=chiTOT_Y;
									BestTrackCand_internal[0]=i;
									BestTrackCand_internal[2]=j;
									BestTrackCand_internal[7]=k;
									}
								} // End of 4 Detector_internal selection
							} // End og loop over k
						} // End og loop over j
					} // End og loop over i
				} // End of 3/4 Detector_internal selection

			if (Detector_internal[0][0]==1 && Detector_internal[0][2]==1 && Detector_internal[0][5]==1 && !(Detector_internal[0][7]==1))	// 3/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						for (int k=0;k<numtrack_internal;k++)
							{
							if (Detector_internal[i][0]==1 && Detector_internal[j][2]==1 && Detector_internal[k][5]==1)
								{
								chiy[5]=TMath::Power(-(y_A[i][0]*RLeffy[5][0])-(y_A[k][5]),2)/sig2y[i][5][0]+
										TMath::Power(-(y_A[j][2]*RLeffy[5][2])-(y_A[k][5]),2)/sig2y[j][5][2];
								chiy[2]=TMath::Power((y_A[i][0]*RLeffy[2][0])-(y_A[j][2]),2)/sig2y[i][2][0]+
										TMath::Power(-(y_A[k][5]*RLeffy[2][5])-(y_A[j][2]),2)/sig2y[k][2][5];
								chiy[0]=TMath::Power((y_A[j][2]*RLeffy[0][2])-(y_A[i][0]),2)/sig2y[j][0][2]+
										TMath::Power(-(y_A[k][5]*RLeffy[0][5])-(y_A[i][0]),2)/sig2y[k][0][5];
								chix[5]=TMath::Power(-(x_A[i][0]*RLeffx[5][0])-(x_A[k][5]),2)/sig2x[i][5][0]+
										TMath::Power(-(x_A[j][2]*RLeffx[5][2])-(x_A[k][5]),2)/sig2x[j][5][2];
								chix[2]=TMath::Power((x_A[i][0]*RLeffx[2][0])-(x_A[j][2]),2)/sig2x[i][2][0]+
										TMath::Power(-(x_A[k][5]*RLeffx[2][5])-(x_A[j][2]),2)/sig2x[k][2][5];
								chix[0]=TMath::Power((x_A[j][2]*RLeffx[0][2])-(x_A[i][0]),2)/sig2x[j][0][2]+
										TMath::Power(-(x_A[k][5]*RLeffx[0][5])-(x_A[i][0]),2)/sig2x[k][0][5];

								chi[0]=chix[0]+chiy[0];
								chi[2]=chix[2]+chiy[2];
								chi[5]=chix[5]+chiy[5];

								chiTOT=chi[0]+chi[2]+chi[5];
								chiTOT_X=chix[0]+chix[2]+chix[5];
								chiTOT_Y=chiy[0]+chiy[2]+chiy[5];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
									{
									minChi=chiTOT;
									minChi_x=chiTOT_X;
									minChi_y=chiTOT_Y;
									BestTrackCand_internal[0]=i;
									BestTrackCand_internal[2]=j;
									BestTrackCand_internal[5]=k;
									}
								} // End of 4 Detector_internal selection
							} // End og loop over k
						} // End og loop over j
					} // End og loop over i
				} // End of 3/4 Detector_internal selection

			if (Detector_internal[0][0]==1 && Detector_internal[0][2]==1 && !(Detector_internal[0][5]==1) && !(Detector_internal[0][7]==1))	// 2/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						if (Detector_internal[i][0]==1 && Detector_internal[j][2]==1)
							{
							chix[2]=TMath::Power((x_A[i][0]*RLeffx[2][0])-(x_A[j][2]),2)/sig2x[i][2][0];
							chix[0]=TMath::Power((x_A[j][2]*RLeffx[0][2])-(x_A[i][0]),2)/sig2x[j][0][2];
							chiy[2]=TMath::Power((y_A[i][0]*RLeffy[2][0])-(y_A[j][2]),2)/sig2y[i][2][0];
							chiy[0]=TMath::Power((y_A[j][2]*RLeffy[0][2])-(y_A[i][0]),2)/sig2y[j][0][2];

							chi[0]=chix[0]+chiy[0];
							chi[2]=chix[2]+chiy[2];

							chiTOT=chi[0]+chi[2];
							chiTOT_X=chix[0]+chix[2];
							chiTOT_Y=chiy[0]+chiy[2];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;
								BestTrackCand_internal[0]=i;
								BestTrackCand_internal[2]=j;
								}
							} // End of Detector_internal selection
						} // End og loop over j
					} // End og loop over i
				} // End of 2/4 Detector_internal selection

			if (!(Detector_internal[0][0]==1) && !(Detector_internal[0][2]==1) && Detector_internal[0][5]==1 && Detector_internal[0][7]==1)	// 2/4
				{
				for (int k=0;k<numtrack_internal;k++)
					{
					for (int l=0;l<numtrack_internal;l++)
						{
						if (Detector_internal[k][5]==1 && Detector_internal[l][7]==1)
							{
							chix[5]=TMath::Power((x_A[l][7]*RLeffx[5][7])-(x_A[k][5]),2)/sig2x[l][5][7];
							chix[7]=TMath::Power((x_A[k][5]*RLeffx[7][5])-(x_A[l][7]),2)/sig2x[k][7][5];
							chiy[5]=TMath::Power((y_A[l][7]*RLeffy[5][7])-(y_A[k][5]),2)/sig2y[l][5][7];
							chiy[7]=TMath::Power((y_A[k][5]*RLeffy[7][5])-(y_A[l][7]),2)/sig2y[k][7][5];

							chi[5]=chix[5]+chiy[5];
							chi[7]=chix[7]+chiy[7];

							chiTOT=chi[5]+chi[7];
							chiTOT_X=chix[5]+chix[7];
							chiTOT_Y=chiy[5]+chiy[7];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;

								BestTrackCand_internal[5]=k;
								BestTrackCand_internal[7]=l;
								}
							} //End of Detector_internal check on the two reco tracks
						} // End og loop over l
					} // End og loop over k
				} // End of 2/4 Detector_internal selection

			if (Detector_internal[0][0]==1 && !(Detector_internal[0][2]==1) && Detector_internal[0][5]==1 && !(Detector_internal[0][7]==1))	// 1+1/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						if (Detector_internal[i][0]==1 && Detector_internal[j][5]==1)
							{
							chix[5]=TMath::Power(-(x_A[i][0]*RLeffx[5][0])-(x_A[j][5]),2)/sig2x[i][5][0];
							chix[0]=TMath::Power(-(x_A[j][5]*RLeffx[0][5])-(x_A[i][0]),2)/sig2x[j][0][5];
							chiy[5]=TMath::Power(-(y_A[i][0]*RLeffy[5][0])-(y_A[j][5]),2)/sig2y[i][5][0];
							chiy[0]=TMath::Power(-(y_A[j][5]*RLeffy[0][5])-(y_A[i][0]),2)/sig2y[j][0][5];

							chi[0]=chix[0]+chiy[0];
							chi[5]=chix[5]+chiy[5];

							chiTOT=chi[0]+chi[5];
							chiTOT_X=chix[0]+chix[5];
							chiTOT_Y=chiy[0]+chiy[5];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;
								BestTrackCand_internal[0]=i;
								BestTrackCand_internal[5]=j;
								}
							} // End of Detector_internal selection
						} // End og loop over j
					} // End og loop over i
				} // End of 1+1/4 Detector_internal selection

			if (Detector_internal[0][0]==1 && !(Detector_internal[0][2]==1) && !(Detector_internal[0][5]==1) && Detector_internal[0][7]==1)	// 1+1/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						if (Detector_internal[i][0]==1 && Detector_internal[j][7]==1)
							{
							chix[7]=TMath::Power(-(x_A[i][0]*RLeffx[7][0])-(x_A[j][7]),2)/sig2x[i][7][0];
							chix[0]=TMath::Power(-(x_A[j][7]*RLeffx[0][7])-(x_A[i][0]),2)/sig2x[j][0][7];
							chiy[7]=TMath::Power(-(y_A[i][0]*RLeffy[7][0])-(y_A[j][7]),2)/sig2y[i][7][0];
							chiy[0]=TMath::Power(-(y_A[j][7]*RLeffy[0][7])-(y_A[i][0]),2)/sig2y[j][0][7];

							chi[0]=chix[0]+chiy[0];
							chi[7]=chix[7]+chiy[7];

							chiTOT=chi[0]+chi[7];
							chiTOT_X=chix[0]+chix[7];
							chiTOT_Y=chiy[0]+chiy[7];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;
								BestTrackCand_internal[0]=i;
								BestTrackCand_internal[7]=j;
								}
							} // End of Detector_internal selection
						} // End og loop over j
					} // End og loop over i
				} // End of 1+1/4 Detector_internal selection

			if (!(Detector_internal[0][0]==1) && Detector_internal[0][2]==1 && Detector_internal[0][5]==1 && !(Detector_internal[0][7]==1))	// 1+1/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						if (Detector_internal[i][2]==1 && Detector_internal[j][5]==1)
							{
							chix[5]=TMath::Power(-(x_A[i][2]*RLeffx[5][2])-(x_A[j][5]),2)/sig2x[i][5][2];
							chix[2]=TMath::Power(-(x_A[j][5]*RLeffx[2][5])-(x_A[i][2]),2)/sig2x[j][2][5];
							chiy[5]=TMath::Power(-(y_A[i][2]*RLeffy[5][2])-(y_A[j][5]),2)/sig2y[i][5][2];
							chiy[2]=TMath::Power(-(y_A[j][5]*RLeffy[2][5])-(y_A[i][2]),2)/sig2y[j][2][5];

							chi[2]=chix[2]+chiy[2];
							chi[5]=chix[5]+chiy[5];

							chiTOT=chi[2]+chi[5];
							chiTOT_X=chix[2]+chix[5];
							chiTOT_Y=chiy[2]+chiy[5];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;
								BestTrackCand_internal[2]=i;
								BestTrackCand_internal[5]=j;
								}
							} // End of Detector_internal selection
						} // End og loop over j
					} // End og loop over i
				} // End of 1+1/4 Detector_internal selection

			if (!(Detector_internal[0][0]==1) && Detector_internal[0][2]==1 && !(Detector_internal[0][5]==1) && Detector_internal[0][7]==1)	// 1+1/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						if (Detector_internal[i][2]==1 && Detector_internal[j][7]==1)
							{
							chix[7]=TMath::Power(-(x_A[i][2]*RLeffx[7][2])-(x_A[j][7]),2)/sig2x[i][7][2];
							chix[2]=TMath::Power(-(x_A[j][7]*RLeffx[2][7])-(x_A[i][2]),2)/sig2x[j][2][7];
							chiy[7]=TMath::Power(-(y_A[i][2]*RLeffy[7][2])-(y_A[j][7]),2)/sig2y[i][7][2];
							chiy[2]=TMath::Power(-(y_A[j][7]*RLeffy[2][7])-(y_A[i][2]),2)/sig2y[j][2][7];

							chi[2]=chix[2]+chiy[2];
							chi[7]=chix[7]+chiy[7];

							chiTOT=chi[2]+chi[7];
							chiTOT_X=chix[2]+chix[7];
							chiTOT_Y=chiy[2]+chiy[7];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;
								BestTrackCand_internal[2]=i;
								BestTrackCand_internal[7]=j;
								}
							} // End of Detector_internal selection
						} // End og loop over j
					} // End og loop over i
				} // End of 1+1/4 Detector_internal selection

			} // End of elastics trigger selection

		Chi2[0] = minChi;
		Chi2x[0] = minChi_x;
		Chi2y[0] = minChi_y;

		minChi=1e9;
		minChi_x=1e9;
		minChi_y=1e9;

		//~ if (LVL1TrigSig[54])
		//if (LVL1TrigSig[113])
			{
			for (int i=0;i<numtrack_internal;i++)
				{
				for (Int_t dn=0; dn< 8; dn++)
					{
					if(dn==1 || dn==3 || dn==4 || dn==6) //sigma_y calculations
						{
						for (Int_t dm=0; dm< 8; dm++)
							{
							if((dm==1 || dm==3 || dm==4 || dm==6) && dm!=dn)
								{
								sig2y[i][dn][dm]=sigres2[dn]+sigres2[dm]*TMath::Power(RLeffy[dn][dm],2)+TMath::Power(y_A[i][dm]*sigRLeffy[dn][dm],2)+sigdiv2y[dn][dm];
								sig2x[i][dn][dm]=sigres2[dn]+sigres2[dm]*TMath::Power(RLeffx[dn][dm],2)+TMath::Power(x_A[i][dm]*sigRLeffx[dn][dm],2)+sigdiv2x[dn][dm];
								}
							}
						}
					}
				}

			if (Detector_internal[0][1]==1 && Detector_internal[0][3]==1 && Detector_internal[0][4]==1 && Detector_internal[0][6]==1)	// 4/4 elastic
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						for (int k=0;k<numtrack_internal;k++)
							{
							for (int l=0;l<numtrack_internal;l++)
								{
								if (Detector_internal[i][1]==1 && Detector_internal[j][3]==1 && Detector_internal[k][4]==1 && Detector_internal[l][6]==1)
									{
									chiy[4]=TMath::Power(-(y_A[i][1]*RLeffy[4][1])-(y_A[k][4]),2)/sig2y[i][4][1]+
											TMath::Power(-(y_A[j][3]*RLeffy[4][3])-(y_A[k][4]),2)/sig2y[j][4][3]+
											TMath::Power((y_A[l][6]*RLeffy[4][6])-(y_A[k][4]),2)/sig2y[l][4][6];

									chiy[6]=TMath::Power(-(y_A[i][1]*RLeffy[6][1])-(y_A[l][6]),2)/sig2y[i][6][1]+
											TMath::Power(-(y_A[j][3]*RLeffy[6][3])-(y_A[l][6]),2)/sig2y[j][6][3]+
											TMath::Power((y_A[k][4]*RLeffy[6][4])-(y_A[l][6]),2)/sig2y[k][6][4];

									chiy[3]=TMath::Power((y_A[i][1]*RLeffy[3][1])-(y_A[j][3]),2)/sig2y[i][3][1]+
											TMath::Power(-(y_A[k][4]*RLeffy[3][4])-(y_A[j][3]),2)/sig2y[k][3][4]+
											TMath::Power(-(y_A[l][6]*RLeffy[3][6])-(y_A[j][3]),2)/sig2y[l][3][6];

									chiy[1]=TMath::Power((y_A[j][3]*RLeffy[1][3])-(y_A[i][1]),2)/sig2y[j][1][3]+
											TMath::Power(-(y_A[k][4]*RLeffy[1][4])-(y_A[i][1]),2)/sig2y[k][1][4]+
											TMath::Power(-(y_A[l][6]*RLeffy[1][6])-(y_A[i][1]),2)/sig2y[l][1][6];

									chix[4]=TMath::Power(-(x_A[i][1]*RLeffx[4][1])-(x_A[k][4]),2)/sig2x[i][4][1]+
											TMath::Power(-(x_A[j][3]*RLeffx[4][3])-(x_A[k][4]),2)/sig2x[j][4][3]+
											TMath::Power((x_A[l][6]*RLeffx[4][6])-(x_A[k][4]),2)/sig2x[l][4][6];

									chix[6]=TMath::Power(-(x_A[i][1]*RLeffx[6][1])-(x_A[l][6]),2)/sig2x[i][6][1]+
											TMath::Power(-(x_A[j][3]*RLeffx[6][3])-(x_A[l][6]),2)/sig2x[j][6][3]+
											TMath::Power((x_A[k][4]*RLeffx[6][4])-(x_A[l][6]),2)/sig2x[k][6][4];

									chix[3]=TMath::Power((x_A[i][1]*RLeffx[3][1])-(x_A[j][3]),2)/sig2x[i][3][1]+
											TMath::Power(-(x_A[k][4]*RLeffx[3][4])-(x_A[j][3]),2)/sig2x[k][3][4]+
											TMath::Power(-(x_A[l][6]*RLeffx[3][6])-(x_A[j][3]),2)/sig2x[l][3][6];

									chix[1]=TMath::Power((x_A[j][3]*RLeffx[1][3])-(x_A[i][1]),2)/sig2x[j][1][3]+
											TMath::Power(-(x_A[k][4]*RLeffx[1][4])-(x_A[i][1]),2)/sig2x[k][1][4]+
											TMath::Power(-(x_A[l][6]*RLeffx[1][6])-(x_A[i][1]),2)/sig2x[l][1][6];

									chi[1]=chix[1]+chiy[1];
									chi[3]=chix[3]+chiy[3];
									chi[4]=chix[4]+chiy[4];
									chi[6]=chix[6]+chiy[6];

									chiTOT=chi[1]+chi[3]+chi[4]+chi[6];
									chiTOT_X=chix[1]+chix[3]+chix[4]+chix[6];
									chiTOT_Y=chiy[1]+chiy[3]+chiy[4]+chiy[6];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
										{
										minChi=chiTOT;
										minChi_x=chiTOT_X;
										minChi_y=chiTOT_Y;
										BestTrackCand_internal[1]=i;
										BestTrackCand_internal[3]=j;
										BestTrackCand_internal[4]=k;
										BestTrackCand_internal[6]=l;
										cnt++;
										}
									} //End of 4 Detector_internal selection
								} // End of loop over l
							} // End of loop over k
						} // End of loop over j
					} // End of loop over i
				} // End of 4/4 Detector_internal selection

			if (!(Detector_internal[0][1]==1) && Detector_internal[0][3]==1 && Detector_internal[0][4]==1 && Detector_internal[0][6]==1)	// 3/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						for (int k=0;k<numtrack_internal;k++)
							{
							if (Detector_internal[i][3]==1 && Detector_internal[j][4]==1 && Detector_internal[k][6]==1)
								{
								chiy[4]=TMath::Power(-(y_A[i][3]*RLeffy[4][3])-(y_A[j][4]),2)/sig2y[i][4][3]+
										TMath::Power((y_A[k][6]*RLeffy[4][6])-(y_A[j][4]),2)/sig2y[k][4][6];
								chiy[6]=TMath::Power(-(y_A[i][3]*RLeffy[6][3])-(y_A[k][6]),2)/sig2y[i][6][3]+
										TMath::Power((y_A[j][4]*RLeffy[6][4])-(y_A[k][6]),2)/sig2y[j][6][4];
								chiy[3]=TMath::Power(-(y_A[j][4]*RLeffy[3][4])-(y_A[i][3]),2)/sig2y[j][3][4]+
										TMath::Power(-(y_A[k][6]*RLeffy[3][6])-(y_A[i][3]),2)/sig2y[k][3][6];
								chix[4]=TMath::Power(-(x_A[i][3]*RLeffx[4][3])-(x_A[j][4]),2)/sig2x[i][4][3]+
										TMath::Power((x_A[k][6]*RLeffx[4][6])-(x_A[j][4]),2)/sig2x[k][4][6];
								chix[6]=TMath::Power(-(x_A[i][3]*RLeffx[6][3])-(x_A[k][6]),2)/sig2x[i][6][3]+
										TMath::Power((x_A[j][4]*RLeffx[6][4])-(x_A[k][6]),2)/sig2x[j][6][4];
								chix[3]=TMath::Power(-(x_A[j][4]*RLeffx[3][4])-(x_A[i][3]),2)/sig2x[j][3][4]+
										TMath::Power(-(x_A[k][6]*RLeffx[3][6])-(x_A[i][3]),2)/sig2x[k][3][6];

								chi[3]=chix[3]+chiy[3];
								chi[4]=chix[4]+chiy[4];
								chi[6]=chix[6]+chiy[6];

								chiTOT=chi[3]+chi[4]+chi[6];
								chiTOT_X=chix[3]+chix[4]+chix[6];
								chiTOT_Y=chiy[3]+chiy[4]+chiy[6];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
									{
									minChi=chiTOT;
									minChi_x=chiTOT_X;
									minChi_y=chiTOT_Y;
									BestTrackCand_internal[3]=i;
									BestTrackCand_internal[4]=j;
									BestTrackCand_internal[6]=k;
									}
								} //End of 4 Detector_internal selection
							} // End of loop over k
						} // End of loop over j
					} // End of loop over i
				} // End of 3/4 Detector_internal selection

			if (Detector_internal[0][1]==1 && !(Detector_internal[0][3]==1) && Detector_internal[0][4]==1 && Detector_internal[0][6]==1)	// 3/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						for (int k=0;k<numtrack_internal;k++)
							{
							if (Detector_internal[i][1]==1 && Detector_internal[j][4]==1 && Detector_internal[k][6]==1)
								{
								chiy[4]=TMath::Power(-(y_A[i][1]*RLeffy[4][1])-(y_A[j][4]),2)/sig2y[i][4][1]+
										TMath::Power((y_A[k][6]*RLeffy[4][6])-(y_A[j][4]),2)/sig2y[k][4][6];
								chiy[6]=TMath::Power(-(y_A[i][1]*RLeffy[6][1])-(y_A[k][6]),2)/sig2y[i][6][1]+
										TMath::Power((y_A[j][4]*RLeffy[6][4])-(y_A[k][6]),2)/sig2y[j][6][4];
								chiy[1]=TMath::Power(-(y_A[j][4]*RLeffy[1][4])-(y_A[i][1]),2)/sig2y[j][1][4]+
										TMath::Power(-(y_A[k][6]*RLeffy[1][6])-(y_A[i][1]),2)/sig2y[k][1][6];
								chix[4]=TMath::Power(-(x_A[i][1]*RLeffx[4][1])-(x_A[j][4]),2)/sig2x[i][4][1]+
										TMath::Power((x_A[k][6]*RLeffx[4][6])-(x_A[j][4]),2)/sig2x[k][4][6];
								chix[6]=TMath::Power(-(x_A[i][1]*RLeffx[6][1])-(x_A[k][6]),2)/sig2x[i][6][1]+
										TMath::Power((x_A[j][4]*RLeffx[6][4])-(x_A[k][6]),2)/sig2x[j][6][4];
								chix[1]=TMath::Power(-(x_A[j][4]*RLeffx[1][4])-(x_A[i][1]),2)/sig2x[j][1][4]+
										TMath::Power(-(x_A[k][6]*RLeffx[1][6])-(x_A[i][1]),2)/sig2x[k][1][6];

								chi[1]=chix[1]+chiy[1];
								chi[4]=chix[4]+chiy[4];
								chi[6]=chix[6]+chiy[6];

								chiTOT=chi[1]+chi[4]+chi[6];
								chiTOT_X=chix[1]+chix[4]+chix[6];
								chiTOT_Y=chiy[1]+chiy[4]+chiy[6];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
									{
									minChi=chiTOT;
									minChi_x=chiTOT_X;
									minChi_y=chiTOT_Y;
									BestTrackCand_internal[1]=i;
									BestTrackCand_internal[4]=j;
									BestTrackCand_internal[6]=k;
									}
								} //End of 4 Detector_internal selection
							} // End of loop over k
						} // End of loop over j
					} // End of loop over i
				} // End of 3/4 Detector_internal selection

			if (Detector_internal[0][1]==1 && Detector_internal[0][3]==1 && !(Detector_internal[0][4]==1) && Detector_internal[0][6]==1)	// 3/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						for (int k=0;k<numtrack_internal;k++)
							{
							if (Detector_internal[i][1]==1 && Detector_internal[j][3]==1 && Detector_internal[k][6]==1)
								{
								chiy[6]=TMath::Power(-(y_A[i][1]*RLeffy[6][1])-(y_A[k][6]),2)/sig2y[i][6][1]+
										TMath::Power(-(y_A[j][3]*RLeffy[6][3])-(y_A[k][6]),2)/sig2y[j][6][3];
								chiy[3]=TMath::Power((y_A[i][1]*RLeffy[3][1])-(y_A[j][3]),2)/sig2y[i][3][1]+
										TMath::Power(-(y_A[k][6]*RLeffy[3][6])-(y_A[j][3]),2)/sig2y[k][3][6];
								chiy[1]=TMath::Power((y_A[j][3]*RLeffy[1][3])-(y_A[i][1]),2)/sig2y[j][1][3]+
										TMath::Power(-(y_A[k][6]*RLeffy[1][6])-(y_A[i][1]),2)/sig2y[k][1][6];
								chix[6]=TMath::Power(-(x_A[i][1]*RLeffx[6][1])-(x_A[k][6]),2)/sig2x[i][6][1]+
										TMath::Power(-(x_A[j][3]*RLeffx[6][3])-(x_A[k][6]),2)/sig2x[j][6][3];
								chix[3]=TMath::Power((x_A[i][1]*RLeffx[3][1])-(x_A[j][3]),2)/sig2x[i][3][1]+
										TMath::Power(-(x_A[k][6]*RLeffx[3][6])-(x_A[j][3]),2)/sig2x[k][3][6];
								chix[1]=TMath::Power((x_A[j][3]*RLeffx[1][3])-(x_A[i][1]),2)/sig2x[j][1][3]+
										TMath::Power(-(x_A[k][6]*RLeffx[1][6])-(x_A[i][1]),2)/sig2x[k][1][6];

								chi[1]=chix[1]+chiy[1];
								chi[3]=chix[3]+chiy[3];
								chi[6]=chix[6]+chiy[6];

								chiTOT=chi[1]+chi[3]+chi[6];
								chiTOT_X=chix[1]+chix[3]+chix[6];
								chiTOT_Y=chiy[1]+chiy[3]+chiy[6];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
									{
									minChi=chiTOT;
									minChi_x=chiTOT_X;
									minChi_y=chiTOT_Y;
									BestTrackCand_internal[1]=i;
									BestTrackCand_internal[3]=j;
									BestTrackCand_internal[6]=k;
									}
								} //End of 4 Detector_internal selection
							} // End of loop over k
						} // End of loop over j
					} // End of loop over i
				} // End of 3/4 Detector_internal selection

			if (Detector_internal[0][1]==1 && Detector_internal[0][3]==1 && Detector_internal[0][4]==1 && !(Detector_internal[0][6]==1))	// 3/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						for (int k=0;k<numtrack_internal;k++)
							{
							if (Detector_internal[i][1]==1 && Detector_internal[j][3]==1 && Detector_internal[k][4]==1)
								{
								chiy[4]=TMath::Power(-(y_A[i][1]*RLeffy[4][1])-(y_A[k][4]),2)/sig2y[i][4][1]+
										TMath::Power(-(y_A[j][3]*RLeffy[4][3])-(y_A[k][4]),2)/sig2y[j][4][3];
								chiy[3]=TMath::Power((y_A[i][1]*RLeffy[3][1])-(y_A[j][3]),2)/sig2y[i][3][1]+
										TMath::Power(-(y_A[k][4]*RLeffy[3][4])-(y_A[j][3]),2)/sig2y[k][3][4];
								chiy[1]=TMath::Power((y_A[j][3]*RLeffy[1][3])-(y_A[i][1]),2)/sig2y[j][1][3]+
										TMath::Power(-(y_A[k][4]*RLeffy[1][4])-(y_A[i][1]),2)/sig2y[k][1][4];
								chix[4]=TMath::Power(-(x_A[i][1]*RLeffx[4][1])-(x_A[k][4]),2)/sig2x[i][4][1]+
										TMath::Power(-(x_A[j][3]*RLeffx[4][3])-(x_A[k][4]),2)/sig2x[j][4][3];
								chix[3]=TMath::Power((x_A[i][1]*RLeffx[3][1])-(x_A[j][3]),2)/sig2x[i][3][1]+
										TMath::Power(-(x_A[k][4]*RLeffx[3][4])-(x_A[j][3]),2)/sig2x[k][3][4];
								chix[1]=TMath::Power((x_A[j][3]*RLeffx[1][3])-(x_A[i][1]),2)/sig2x[j][1][3]+
										TMath::Power(-(x_A[k][4]*RLeffx[1][4])-(x_A[i][1]),2)/sig2x[k][1][4];

								chi[1]=chix[1]+chiy[1];
								chi[3]=chix[3]+chiy[3];
								chi[4]=chix[4]+chiy[4];

								chiTOT=chi[1]+chi[3]+chi[4];
								chiTOT_X=chix[1]+chix[3]+chix[4];
								chiTOT_Y=chiy[1]+chiy[3]+chiy[4];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
									{
									minChi=chiTOT;
									minChi_x=chiTOT_X;
									minChi_y=chiTOT_Y;
									BestTrackCand_internal[1]=i;
									BestTrackCand_internal[3]=j;
									BestTrackCand_internal[4]=k;
									}
								} //End of 4 Detector_internal selection
							} // End of loop over k
						} // End of loop over j
					} // End of loop over i
				} // End of 3/4 Detector_internal selection

			if (Detector_internal[0][1]==1 && Detector_internal[0][3]==1 && !(Detector_internal[0][4]==1) && !(Detector_internal[0][6]==1))	// 2/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						if (Detector_internal[i][1]==1 && Detector_internal[j][3]==1)
							{
							chiy[3]=TMath::Power((y_A[i][1]*RLeffy[3][1])-(y_A[j][3]),2)/sig2y[i][3][1];
							chiy[1]=TMath::Power((y_A[j][3]*RLeffy[1][3])-(y_A[i][1]),2)/sig2y[j][1][3];
							chix[3]=TMath::Power((x_A[i][1]*RLeffx[3][1])-(x_A[j][3]),2)/sig2x[i][3][1];
							chix[1]=TMath::Power((x_A[j][3]*RLeffx[1][3])-(x_A[i][1]),2)/sig2x[j][1][3];

							chi[1]=chix[1]+chiy[1];
							chi[3]=chix[3]+chiy[3];

							chiTOT=chi[1]+chi[3];
							chiTOT_X=chix[1]+chix[3];
							chiTOT_Y=chiy[1]+chiy[3];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;
								BestTrackCand_internal[1]=i;
								BestTrackCand_internal[3]=j;
								}
							} //End of Detector_internalselection of the reco tracks
						} // End of loop over j
					} // End of loop over i
				} // End of 2/4 Detector_internal selection

			if (!(Detector_internal[0][1]==1) && !(Detector_internal[0][3]==1) && Detector_internal[0][4]==1 && Detector_internal[0][6]==1)	// 2/4
				{
				for (int k=0;k<numtrack_internal;k++)
					{
					for (int l=0;l<numtrack_internal;l++)
						{
						if (Detector_internal[k][4]==1 && Detector_internal[l][6]==1)
							{
							chiy[4]=TMath::Power((y_A[l][6]*RLeffy[4][6])-(y_A[k][4]),2)/sig2y[l][4][6];
							chiy[6]=TMath::Power((y_A[k][4]*RLeffy[6][4])-(y_A[l][6]),2)/sig2y[k][6][4];
							chix[4]=TMath::Power((x_A[l][6]*RLeffx[4][6])-(x_A[k][4]),2)/sig2x[l][4][6];
							chix[6]=TMath::Power((x_A[k][4]*RLeffx[6][4])-(x_A[l][6]),2)/sig2x[k][6][4];

							chi[4]=chix[4]+chiy[4];
							chi[6]=chix[6]+chiy[6];

							chiTOT=chi[4]+chi[6];
							chiTOT_X=chix[4]+chix[6];
							chiTOT_Y=chiy[4]+chiy[6];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;
								BestTrackCand_internal[4]=k;
								BestTrackCand_internal[6]=l;
								}
							} //End of Detector_internal check on the two reco tracks
						} // End of loop over l
					} // End of loop over k
				} // End of 2/4 Detector_internal selection

			if (Detector_internal[0][1]==1 && !(Detector_internal[0][3]==1) && Detector_internal[0][4]==1 && !(Detector_internal[0][6]==1))	// 1+1/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						if (Detector_internal[i][1]==1 && Detector_internal[j][4]==1)
							{
							chiy[4]=TMath::Power(-(y_A[i][1]*RLeffy[4][1])-(y_A[j][4]),2)/sig2y[i][4][1];
							chiy[1]=TMath::Power(-(y_A[j][4]*RLeffy[1][4])-(y_A[i][1]),2)/sig2y[j][1][4];
							chix[4]=TMath::Power(-(x_A[i][1]*RLeffx[4][1])-(x_A[j][4]),2)/sig2x[i][4][1];
							chix[1]=TMath::Power(-(x_A[j][4]*RLeffx[1][4])-(x_A[i][1]),2)/sig2x[j][1][4];

							chi[1]=chix[1]+chiy[1];
							chi[4]=chix[4]+chiy[4];

							chiTOT=chi[1]+chi[4];
							chiTOT_X=chix[1]+chix[4];
							chiTOT_Y=chiy[1]+chiy[4];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;
								BestTrackCand_internal[1]=i;
								BestTrackCand_internal[4]=j;
								}
							} //End of Detector_internalselection of the reco tracks
						} // End of loop over j
					} // End of loop over i
				} // End of 1+1/4 Detector_internal selection

			if (Detector_internal[0][1]==1 && !(Detector_internal[0][3]==1) && !(Detector_internal[0][4]==1) && Detector_internal[0][6]==1)	// 1+1/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						if (Detector_internal[i][1]==1 && Detector_internal[j][6]==1)
							{
							chiy[6]=TMath::Power(-(y_A[i][1]*RLeffy[6][1])-(y_A[j][6]),2)/sig2y[i][6][1];
							chiy[1]=TMath::Power(-(y_A[j][6]*RLeffy[1][6])-(y_A[i][1]),2)/sig2y[j][1][6];
							chix[6]=TMath::Power(-(x_A[i][1]*RLeffx[6][1])-(x_A[j][6]),2)/sig2x[i][6][1];
							chix[1]=TMath::Power(-(x_A[j][6]*RLeffx[1][6])-(x_A[i][1]),2)/sig2x[j][1][6];

							chi[1]=chix[1]+chiy[1];
							chi[6]=chix[6]+chiy[6];

							chiTOT=chi[1]+chi[6];
							chiTOT_X=chix[1]+chix[6];
							chiTOT_Y=chiy[1]+chiy[6];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;
								BestTrackCand_internal[1]=i;
								BestTrackCand_internal[6]=j;
								}
							} //End of Detector_internalselection of the reco tracks
						} // End of loop over j
					} // End of loop over i
				} // End of 1+1/4 Detector_internal selection

			if (!(Detector_internal[0][1]==1) && Detector_internal[0][3]==1 && Detector_internal[0][4]==1 && !(Detector_internal[0][6]==1))	// 1+1/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						if (Detector_internal[i][3]==1 && Detector_internal[j][4]==1)
							{
							chiy[4]=TMath::Power(-(y_A[i][3]*RLeffy[4][3])-(y_A[j][4]),2)/sig2y[i][4][3];
							chiy[3]=TMath::Power(-(y_A[j][4]*RLeffy[3][4])-(y_A[i][3]),2)/sig2y[j][3][4];
							chix[4]=TMath::Power(-(x_A[i][3]*RLeffx[4][3])-(x_A[j][4]),2)/sig2x[i][4][3];
							chix[3]=TMath::Power(-(x_A[j][4]*RLeffx[3][4])-(x_A[i][3]),2)/sig2x[j][3][4];

							chi[3]=chix[3]+chiy[3];
							chi[4]=chix[4]+chiy[4];

							chiTOT=chi[3]+chi[4];
							chiTOT_X=chix[3]+chix[4];
							chiTOT_Y=chiy[3]+chiy[4];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;
								BestTrackCand_internal[3]=i;
								BestTrackCand_internal[4]=j;
								}
							} //End of Detector_internalselection of the reco tracks
						} // End of loop over j
					} // End of loop over i
				} // End of 1+1/4 Detector_internal selection

			if (!(Detector_internal[0][1]==1) && Detector_internal[0][3]==1 && !(Detector_internal[0][4]==1) && Detector_internal[0][6]==1)	// 1+1/4
				{
				for (int i=0;i<numtrack_internal;i++)
					{
					for (int j=0;j<numtrack_internal;j++)
						{
						if (Detector_internal[i][3]==1 && Detector_internal[j][6]==1)
							{
							chiy[6]=TMath::Power(-(y_A[i][3]*RLeffy[6][3])-(y_A[j][6]),2)/sig2y[i][6][3];
							chiy[3]=TMath::Power(-(y_A[j][6]*RLeffy[3][6])-(y_A[i][3]),2)/sig2y[j][3][6];
							chix[6]=TMath::Power(-(x_A[i][3]*RLeffx[6][3])-(x_A[j][6]),2)/sig2x[i][6][3];
							chix[3]=TMath::Power(-(x_A[j][6]*RLeffx[3][6])-(x_A[i][3]),2)/sig2x[j][3][6];

							chi[3]=chix[3]+chiy[3];
							chi[6]=chix[6]+chiy[6];

							chiTOT=chi[3]+chi[6];
							chiTOT_X=chix[3]+chix[6];
							chiTOT_Y=chiy[3]+chiy[6];

									if ((runmode_internal == 0 && chiTOT<minChi) ||
									 (runmode_internal == 1 && chiTOT_X<minChi) ||
									 (runmode_internal == 2 && chiTOT_Y<minChi))
								{
								minChi=chiTOT;
								minChi_x=chiTOT_X;
								minChi_y=chiTOT_Y;
								BestTrackCand_internal[3]=i;
								BestTrackCand_internal[6]=j;
								}
							} //End of Detector_internalselection of the reco tracks
						} // End of loop over j
					} // End of loop over i
				} // End of 1+1/4 Detector_internal selection
			} // End of elastic trigger request

		Chi2[1] = minChi;
		Chi2x[1] = minChi_x;
		Chi2y[1] = minChi_y;



}

