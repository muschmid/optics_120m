#include "Riostream.h"
using namespace std; 
#include <math.h>

TH1D* ff;
void FormFactorInit()
{
  ifstream a1file;
  cout << "FormFactorInit called " << "\n";
  a1file.open("/home/stenzel/madx/Elastic13TeV/optics_25km/Include/FormFactor/DoubleDipole.dat");
//  a1file.open("/home/stenzel/madx/run2/athena/Code/FormFactor/aux/fits/MainzOnly/FriedrichWalcher.dat");
//  a1file.open("/home/stenzel/madx/run2/athena/Code/FormFactor/aux/fits/MainzOnly/InversePolynomial.dat");
//  a1file.open("/home/stenzel/madx/run2/athena/Code/FormFactor/aux/fits/MainzOnly/Spline.dat");
//  a1file.open("/home/stenzel/madx/run2/athena/Code/FormFactor/aux/fits/MainzOnly/Polynomial.dat");
//  a1file.open("/home/stenzel/madx/run2/athena/Code/FormFactor/aux/fits/MainzOnly/PolynomialMultipliedWithStandardDipole.dat");
//  a1file.open("/home/stenzel/madx/run2/athena/Code/FormFactor/aux/fits/MainzOnly/SplineMultipliedWithStandardDipole");
  
  Int_t nf=999;
  Double_t tf[nf+1],FF[nf+1];
  Double_t du;
   for (Int_t ii = 0; ii <= nf; ii++) {
     a1file >> tf[ii] >> FF[ii] >> du >> du >> du >> du >> du >> du >> du 
     >> du >> du >> du >> du >> du >> du >> du >> du >> du >> du;
//     cout << ii << " " << tf[ii] << "\t" << FF[ii] << "\n";
   }  
  
  ff   = new TH1D("ff",";-t[GeV^{2}];FF(t)",nf,tf);
  
   for (Int_t ii = 1; ii <= nf; ii++) {
     ff->SetBinContent(ii,FF[ii]);
   }  
  a1file.close();  
 }
 
Double_t FormFactor(Double_t x)
{
  Int_t nbin = ff->FindBin(x);
  Double_t x1 = ff->GetBinLowEdge(nbin);
  Double_t x2 = ff->GetBinLowEdge(nbin+1);
  Double_t a = (ff->GetBinContent(nbin+1)-ff->GetBinContent(nbin))/(x2-x1);
  Double_t b = ff->GetBinContent(nbin)-a*x1;
  Double_t xff = a*x+b;
//  Double_t xff = ff->GetBinContent(nbin);  
  return xff;
 }
 
