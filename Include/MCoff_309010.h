     // MC offsets are determined from a comparison of Data and MC (dtmc_offsets.C) 
     Double_t x1_offset=-0.000611215;
     Double_t x2_offset=-0.00301433;
     Double_t x3_offset=-0.00147494;
     Double_t x4_offset=-0.000331561;
     Double_t x5_offset=-0.00367092;
     Double_t x6_offset=0.00203437;
     Double_t x7_offset=0.00197767;
     Double_t x8_offset=-0.00265423;
     Double_t y1_offset=-0.0271979;
     Double_t y2_offset=0.00936374;
     Double_t y3_offset=-0.0288516;
     Double_t y4_offset=0.0135365;
     Double_t y5_offset=-0.0168298;
     Double_t y6_offset=0.0376499;
     Double_t y7_offset=-0.00761799;
     Double_t y8_offset=0.0290933;
