    Bool_t DPEy=false; // reduced normalization region yL vs yR
    Bool_t Halo=false;
    Bool_t DPE_txt=false;
Bool_t sel_xtx_diff(Double_t x, Double_t y){
// cuts on x vs theta_x
    Double_t xcut =1.0, ycut=50.,sig=5.0; 
    Double_t sx=0.196,sy=71.1,theta=-2.79E-3; // values for 13 TeV 2.5km optics
    Double_t xp=x*cos(theta)+y*sin(theta);
    Double_t yp=-x*sin(theta)+y*cos(theta);
    Double_t tx=sig*sx, ty=sig*sy; // standard cut at 3.5
    Bool_t tmp=false;
    if(x>xcut && y>ycut ) tmp=true; 
    if( ((xp*xp/tx/tx + yp*yp/ty/ty) < 1.) ) tmp=false;
return(tmp);
}

Bool_t sel_xtx_halo(Double_t x, Double_t y){
// cuts on x vs theta_x
    Double_t xcut =1.0, ycut=50.,sig=5.0;
    Double_t y1=50,y2=-50;
    if(Halo) sig=3.5,xcut=0.0,ycut=20.;
    Double_t sx=0.196,sy=71.1,theta=-2.79E-3; // values for 13 TeV 2.5km optics
    Double_t xp=x*cos(theta)+y*sin(theta);
    Double_t yp=-x*sin(theta)+y*cos(theta);
    Double_t tx=sig*sx, ty=sig*sy; // standard anti-elastic cut at 5 
    Bool_t tmp=false;
    if (!Halo) if(x<xcut || y<ycut ) tmp=true;
    if (Halo) if(y<y1 && y>y2) tmp=true;
    if( ((xp*xp/tx/tx + yp*yp/ty/ty) < 1.) ) tmp=false;
return(tmp);
}

Bool_t sel_xtx_ellipse(Double_t x, Double_t y){
// cuts on x vs theta_x
    Double_t sx=0.196,sy=71.1,theta=-2.79E-3,sig=5.; // values for 13 TeV 2.5km optics
    Double_t xp=x*cos(theta)+y*sin(theta);
    Double_t yp=-x*sin(theta)+y*cos(theta);
    Double_t tx=sig*sx, ty=sig*sy; // standard anti-elastic cut at 5 
    Double_t zx=1.7,zy=50.,zheta=0.1047; // zjeta=6° in rad
    Double_t zxp=x*cos(theta)+y*sin(theta);
    Double_t zyp=-x*sin(theta)+y*cos(theta);
    Bool_t tmp=false;
    if( (zxp*zxp/zx/zx + zyp*zyp/zy/zy) < 1.) tmp=true;
    if( ((xp*xp/tx/tx + yp*yp/ty/ty) < 1.) ) tmp=false;
return(tmp);
}

Bool_t sel_xlro_diff(Double_t x, Double_t y){
// cuts on xL vs xR
    Double_t sx=0.24,sy=1.12,theta=0.783; // expect x1 and x2 in mm
    Double_t xcut =-3.0, ycut=-3.0,sig=5.0; 
    Double_t xp=x*cos(theta)+y*sin(theta);
    Double_t yp=-x*sin(theta)+y*cos(theta);
    Double_t tx=sig*sx, ty=sig*sy; // standard cut at 3.5
    Bool_t tmp=false;
    if(x>xcut && y>ycut ) tmp=true; 
    if( ((xp*xp/tx/tx + yp*yp/ty/ty) < 1.) ) tmp=false;
return(tmp);
}

Bool_t sel_xlro_halo(Double_t x, Double_t y){
// cuts on xL vs xR
    Double_t sx=0.24,sy=1.12,theta=0.783; // expect x1 and x2 in mm
    Double_t xcut =-3.0, ycut=-3.0,sig=5.0; 
    Double_t xp=x*cos(theta)+y*sin(theta);
    Double_t yp=-x*sin(theta)+y*cos(theta);
    Double_t tx=sig*sx, ty=sig*sy; // standard cut at 3.5
    Bool_t tmp=false;
    if(x<xcut || y<ycut ) tmp=true; 
    if( ((xp*xp/tx/tx + yp*yp/ty/ty) < 1.) ) tmp=false;
return(tmp);
}

Bool_t sel_xlri_diff(Double_t x, Double_t y){
// cuts on xL vs xR
    Double_t sx=0.26,sy=0.29,theta=0.761; // expect x1 and x2 in mm
    Double_t xcut =-1.2, ycut=-1.2,sig=5.0; 
    Double_t xp=x*cos(theta)+y*sin(theta);
    Double_t yp=-x*sin(theta)+y*cos(theta);
    Double_t tx=sig*sx, ty=sig*sy; // standard cut at 3.5
    Bool_t tmp=false;
    if(x>xcut && y>ycut ) tmp=true; 
    if( ((xp*xp/tx/tx + yp*yp/ty/ty) < 1.) ) tmp=false;
return(tmp);
}

Bool_t sel_xlri_halo(Double_t x, Double_t y){
// cuts on xL vs xR
    Double_t sx=0.26,sy=0.29,theta=0.761; // expect x1 and x2 in mm
    Double_t xcut =-1.2, ycut=-1.2,sig=5.0; 
    Double_t y1=5.,y2=-5.;
    if(Halo) xcut=-0.6,ycut=-0.6,sig=3.5;
    Double_t xp=x*cos(theta)+y*sin(theta);
    Double_t yp=-x*sin(theta)+y*cos(theta);
    Double_t tx=sig*sx, ty=sig*sy; // standard cut at 3.5
    Bool_t tmp=false;
    if(!Halo) if(x<xcut || y<ycut ) tmp=true; 
    if(Halo) if(y<y1 && y>y2) tmp=true;
    if( ((xp*xp/tx/tx + yp*yp/ty/ty) < 1.) ) tmp=false;
return(tmp);
}

Bool_t sel_DPE(Double_t x, Double_t y){
// cuts on yL vs yR to select DPE in the normalization area
    Double_t sl=-1.,olo=-1.5,oup=1.5; // diagonal
    Double_t x1,x2,x3,x4;
    x1=20.,x2=18.5,x3=19.,x4=5.;
    Bool_t tmp=false;
    if(DPEy) x1=19.5,x2=18.,x3=18.5,x4=5.5;    
    if(y>0){//upper, arm 1
      if(-x1 < x && x < -x4 && x4 < y && y < x2){
          if((y < (olo+sl*x)) || (y > (oup+sl*x))) tmp=true;
      }
    }
    if(y<0){//lower, arm 2
      if(x4 < x && x < x1 && -x3 < y && y < -x4){
          if((y < (olo+sl*x)) || (y > (oup+sl*x))) tmp=true;
      }
    }    
return(tmp);
}

Bool_t sel_DPE_xtx(Double_t x, Double_t y){
// cuts on x vs theata_x to select DPE in the normalization area
    Double_t sl1=50.,ol1=300.,sl2=75.,ol2=-80.,xcut=1.4;
    Double_t sx=0.196,sy=71.1,theta=-2.79E-3,sig=5.0; // values for 13 TeV 2.5km optics
    Double_t xp=x*cos(theta)+y*sin(theta);
    Double_t yp=-x*sin(theta)+y*cos(theta);
    Double_t tx=sig*sx, ty=sig*sy; // standard cut at 3.5
    if(DPE_txt) xcut=5.;
    Bool_t tmp=false;
        if((y < (ol1+sl1*x)) && (y > (ol2+sl2*x)) && (x>xcut)) tmp=true;
        if( ((xp*xp/tx/tx + yp*yp/ty/ty) < 1.) ) tmp=false;
        if(y<80.) tmp=false;
return(tmp);
}
Bool_t sel_DPE_yty(Double_t x, Double_t y){
// cuts on y vs theata_y to select DPE in the corresponding normalization area
   Double_t sl=-5.8,olo=-20.0,oup=20.0; // y in mm, theta_y in microrad
   Double_t dl=-8.0,dlo=-30.0,dup=30.0;
   Double_t ycut1=-20.0,ycut2=-3.0,ycut3=3.0,ycut4=20.; 
   Bool_t tmp=false;
   Int_t ICase=0;
   if(x<0.0) ICase=1;
   if(x>0.0) ICase=2;
   if(ICase==1){
    if((y > (oup+sl*x)) && (y < (dup+dl*x))) tmp=true;
    if(x<ycut1 || x>ycut2) tmp=false;
   }
   if(ICase==2){
    if((y < (olo+sl*x)) && (y > (dlo+dl*x))) tmp=true;
    if(x<ycut3 || x>ycut4) tmp=false;
   }    
return(tmp);
}
