/**
 *  Macro description:
 *
 *	Selection of elastic events dpending on reconstruction ID 
 *  Input:
 * 
 * 		Int_t           ID [0-23];
 *		Double_t         x_B[8] selected tracks x    
 *		Double_t         y_B[8] selected tracks y
 * 
 *  Output
 *	    Bool_t           selastic true/false
 *
 *  @date    2019-07-16
 *  @author  Hasko Stenzel
 *
 **/

#include "TMath.h"
#include "TMathBase.h"

Double_t a1[4]={yetr[0]+33.,yetr[2]+33.,yetr[4]+33.,yetr[6]+33.};
Double_t a2[4]={yetr[0]-16.,yetr[2]-16.,yetr[4]-16.,yetr[6]-16.,};
Double_t a3[4]={yetr[1]-33.,yetr[3]-33.,yetr[5]-33.,yetr[7]-33.};
Double_t a4[4]={yetr[1]+16.,yetr[3]+16.,yetr[5]+16.,yetr[7]+16.,};

Bool_t selastic(Int_t ID , Double_t x_B[8] , Double_t y_B[8]){

    Bool_t result=false;
    // select requested cuts 
    Bool_t xAC=true;
    Bool_t xTX=true;
    Bool_t yAC=true;
    Bool_t yTY=true;
    Bool_t yBeamScreen=true;
    Bool_t yDetectorEdge=true;
    Bool_t xCut=true;
    
    Bool_t x_accept=false; 
    Bool_t y_accept=false; 
    Bool_t xtx_accept=false; 
    Bool_t yty_accept=false; 
    Bool_t ybs_accept=false; 
    Bool_t yed_accept=false;
    Bool_t xcut_accept=false;

    if(!xAC) x_accept=true;
    if(!xTX) xtx_accept=true;
    if(!yAC) y_accept=true;
    if(!yTY) yty_accept=true;
    if(!yBeamScreen) ybs_accept=true;
    if(!yDetectorEdge) yed_accept=true;
    if(!xCut) xcut_accept=true;
    
    if(ID==0){
        // ALFA 1 3 6 8
       	if(axo(x_B[0],x_B[7]) && axi(x_B[2],x_B[5])) x_accept=true;
        if(ayy(y_B[0],y_B[7]) && ayy(y_B[2],y_B[5])) y_accept=true;
       	if(axx(x_B[2],1.E6*(x_B[0]-x_B[2])/dz2) && axx(x_B[5],1.E6*(x_B[7]-x_B[5])/dz1)) xtx_accept=true;	
        if(ayty(y_B[2],1.E6*(y_B[0]-y_B[2])/dz2) && ayty(y_B[5],1.E6*(y_B[7]-y_B[5])/dz1)) yty_accept=true;        
       	if(y_B[0]<ybs[0] && y_B[2]< ybs[2] && y_B[5]> ybs[5] && y_B[7]>ybs[7]) ybs_accept=true;	
        if(y_B[0]>ye[0] && y_B[2]>ye[2] && y_B[5]<ye[5] && y_B[7]<ye[7]) yed_accept=true;
        if( 
            (y_B[0]<(a1[0]-x_B[0])) && (y_B[0]>(a2[0]+x_B[0])) &&
            (y_B[2]<(a1[1]-x_B[2])) && (y_B[2]>(a2[1]+x_B[2])) && 
            (y_B[5]>(a3[2]+x_B[5])) && (y_B[5]<(a4[2]-x_B[5])) &&   
            (y_B[7]>(a3[3]+x_B[7])) && (y_B[7]<(a4[3]-x_B[7]))            
        ) xcut_accept=true;
        // total selection condition
	    if(x_accept && y_accept && xtx_accept && yty_accept && ybs_accept && yed_accept && xcut_accept) result=true;        
    }
    
    if(ID==1){
        // ALFA 2 4 5 7
       	if(axo(x_B[1],x_B[6]) && axi(x_B[3],x_B[4])) x_accept=true;
        if(ayy(y_B[1],y_B[6]) && ayy(y_B[3],y_B[4])) y_accept=true;
       	if(axx(x_B[3],1.E6*(x_B[1]-x_B[3])/dz2) && axx(x_B[4],1.E6*(x_B[6]-x_B[4])/dz1)) xtx_accept=true;	
        if(ayty(y_B[3],1.E6*(y_B[1]-y_B[3])/dz2) && ayty(y_B[4],1.E6*(y_B[6]-y_B[4])/dz1)) yty_accept=true;        
       	if(y_B[1]>ybs[1] && y_B[3]> ybs[3] && y_B[4]< ybs[4] && y_B[6]<ybs[6]) ybs_accept=true;	
        if(y_B[1]<ye[1] && y_B[3]<ye[3] && y_B[4]>ye[4] && y_B[6]>ye[6]) yed_accept=true;	
        if( 
            (y_B[4]<(a1[2]-x_B[4])) && (y_B[4]>(a2[2]+x_B[4])) &&
            (y_B[6]<(a1[3]-x_B[6])) && (y_B[6]>(a2[3]+x_B[6])) &&
            (y_B[3]>(a3[1]+x_B[3])) && (y_B[3]<(a4[1]-x_B[3])) &&   
            (y_B[1]>(a3[0]+x_B[1])) && (y_B[1]<(a4[0]-x_B[1]))            
        ) xcut_accept=true;
        // total selection condition
	    if(x_accept && y_accept && xtx_accept && yty_accept && ybs_accept && yed_accept && xcut_accept) result=true;        
    }
    
    if(ID==2){
        // ALFA 3 6 8
       	if(axi(x_B[2],x_B[5])) x_accept=true;
        if(ayy(y_B[2],y_B[5])) y_accept=true;
       	if(axx(x_B[5],1.E6*(x_B[7]-x_B[5])/dz1)) xtx_accept=true;	
        if(ayty(y_B[5],1.E6*(y_B[7]-y_B[5])/dz1)) yty_accept=true;        
       	if(y_B[2]< ybs[2] && y_B[5]> ybs[5] && y_B[7]>ybs[7]) ybs_accept=true;	
        if(y_B[2]>ye[2] && y_B[5]<ye[5] && y_B[7]<ye[7]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==3){
        // ALFA 4 5 7
       	if(axi(x_B[3],x_B[4])) x_accept=true;
        if(ayy(y_B[3],y_B[4])) y_accept=true;
       	if(axx(x_B[4],1.E6*(x_B[6]-x_B[4])/dz1)) xtx_accept=true;	
        if(ayty(y_B[4],1.E6*(y_B[6]-y_B[4])/dz1)) yty_accept=true;        
       	if(y_B[3]> ybs[3] && y_B[4]< ybs[4] && y_B[6]<ybs[6]) ybs_accept=true;	
        if(y_B[3]<ye[3] && y_B[4]>ye[4] && y_B[6]>ye[6]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==4){
        // ALFA 1 3 6 
       	if(axi(x_B[2],x_B[5])) x_accept=true;
        if(ayy(y_B[2],y_B[5])) y_accept=true;
       	if(axx(x_B[2],1.E6*(x_B[0]-x_B[2])/dz2)) xtx_accept=true;	
        if(ayty(y_B[2],1.E6*(y_B[0]-y_B[2])/dz2)) yty_accept=true;        
       	if(y_B[0]<ybs[0] && y_B[2]< ybs[2] && y_B[5]> ybs[5]) ybs_accept=true;	
        if(y_B[0]>ye[0] && y_B[2]>ye[2] && y_B[5]<ye[5]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==5){
        // ALFA 2 4 5
       	if(axi(x_B[3],x_B[4])) x_accept=true;
        if(ayy(y_B[3],y_B[4])) y_accept=true;
       	if(axx(x_B[3],1.E6*(x_B[1]-x_B[3])/dz2)) xtx_accept=true;	
        if(ayty(y_B[3],1.E6*(y_B[1]-y_B[3])/dz2)) yty_accept=true;        
       	if(y_B[1]>ybs[1] && y_B[3]> ybs[3] && y_B[4]<ybs[4]) ybs_accept=true;	
        if(y_B[1]<ye[1] && y_B[3]<ye[3] && y_B[4]>ye[4]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==6){
        // ALFA 1 3 8
       	if(axo(x_B[0],x_B[7])) x_accept=true;
        if(ayy(y_B[0],y_B[7])) y_accept=true;
       	if(axx(x_B[2],1.E6*(x_B[0]-x_B[2])/dz2)) xtx_accept=true;	
        if(ayty(y_B[2],1.E6*(y_B[0]-y_B[2])/dz2)) yty_accept=true;        
       	if(y_B[0]<ybs[0] && y_B[2]< ybs[2] && y_B[7]>ybs[7]) ybs_accept=true;	
        if(y_B[0]>ye[0] && y_B[2]>ye[2] && y_B[7]<ye[7]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==7){
        // ALFA 2 4  7
       	if(axo(x_B[1],x_B[6])) x_accept=true;
        if(ayy(y_B[1],y_B[6])) y_accept=true;
       	if(axx(x_B[3],1.E6*(x_B[1]-x_B[3])/dz2)) xtx_accept=true;	
        if(ayty(y_B[3],1.E6*(y_B[1]-y_B[3])/dz2)) yty_accept=true;        
       	if(y_B[1]>ybs[1] && y_B[3]> ybs[3] && y_B[6]<ybs[6]) ybs_accept=true;	
        if(y_B[1]<ye[1] && y_B[3]<ye[3] && y_B[6]>ye[6]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==8){
        // ALFA 1 6 8
       	if(axo(x_B[0],x_B[7])) x_accept=true;
        if(ayy(y_B[0],y_B[7])) y_accept=true;
       	if(axx(x_B[5],1.E6*(x_B[7]-x_B[5])/dz1)) xtx_accept=true;	
        if(ayty(y_B[5],1.E6*(y_B[7]-y_B[5])/dz1)) yty_accept=true;        
       	if(y_B[0]<ybs[0] && y_B[5]> ybs[5] && y_B[7]>ybs[7]) ybs_accept=true;	
        if(y_B[0]>ye[0] && y_B[5]<ye[5] && y_B[7]<ye[7]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==9){
        // ALFA 2 5 7
       	if(axo(x_B[1],x_B[6])) x_accept=true;
        if(ayy(y_B[1],y_B[6])) y_accept=true;
       	if(axx(x_B[4],1.E6*(x_B[6]-x_B[4])/dz1)) xtx_accept=true;	
        if(ayty(y_B[4],1.E6*(y_B[6]-y_B[4])/dz1)) yty_accept=true;        
       	if(y_B[1]>ybs[1] && y_B[4]< ybs[4] && y_B[6]<ybs[6]) ybs_accept=true;	
        if(y_B[1]<ye[1] && y_B[4]>ye[4] && y_B[6]>ye[6]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==10){
        // ALFA 1 3
       	if(axx(x_B[2],1.E6*(x_B[0]-x_B[2])/dz2)) xtx_accept=true;	
        if(ayty(y_B[2],1.E6*(y_B[0]-y_B[2])/dz2)) yty_accept=true;        
       	if(y_B[0]<ybs[0] && y_B[2]< ybs[2]) ybs_accept=true;	
        if(y_B[0]>ye[0] && y_B[2]>ye[2]) yed_accept=true;	
        // total selection condition
	    if(xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==11){
        // ALFA 6 8
       	if(axx(x_B[5],1.E6*(x_B[7]-x_B[5])/dz1)) xtx_accept=true;	
        if(ayty(y_B[5],1.E6*(y_B[7]-y_B[5])/dz1)) yty_accept=true;        
       	if(y_B[5]> ybs[5] && y_B[7]>ybs[7]) ybs_accept=true;	
        if(y_B[5]<ye[5] && y_B[7]<ye[7]) yed_accept=true;	
        // total selection condition
	    if(xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==12){
        // ALFA 2 4
       	if(axx(x_B[3],1.E6*(x_B[1]-x_B[3])/dz2)) xtx_accept=true;	
        if(ayty(y_B[3],1.E6*(y_B[1]-y_B[3])/dz2)) yty_accept=true;        
       	if(y_B[1]>ybs[1] && y_B[3]> ybs[3]) ybs_accept=true;	
        if(y_B[1]<ye[1] && y_B[3]<ye[3]) yed_accept=true;	
        // total selection condition
	    if(xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==13){
        // ALFA 5 7
       	if(axx(x_B[4],1.E6*(x_B[6]-x_B[4])/dz1)) xtx_accept=true;	
        if(ayty(y_B[4],1.E6*(y_B[6]-y_B[4])/dz1)) yty_accept=true;        
       	if(y_B[4]< ybs[4] && y_B[6]<ybs[6]) ybs_accept=true;	
        if(y_B[4]>ye[4] && y_B[6]>ye[6]) yed_accept=true;	
        // total selection condition
	    if(xtx_accept && yty_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==14){
        // ALFA 3 6 
       	if(axi(x_B[2],x_B[5])) x_accept=true;
        if(ayy(y_B[2],y_B[5])) y_accept=true;
       	if(y_B[2]< ybs[2] && y_B[5]> ybs[5]) ybs_accept=true;	
        if(y_B[2]>ye[2] && y_B[5]<ye[5]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==15){
        // ALFA 4 5 
       	if(axi(x_B[3],x_B[4])) x_accept=true;
        if(ayy(y_B[3],y_B[4])) y_accept=true;
       	if(y_B[3]> ybs[3] && y_B[4]< ybs[4]) ybs_accept=true;	
        if(y_B[3]<ye[3] && y_B[4]>ye[4]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==31){
        // ALFA 1 8 
       	if(axo(x_B[0],x_B[7])) x_accept=true;
        if(ayy(y_B[0],y_B[7])) y_accept=true;
       	if(y_B[0]< ybs[0] && y_B[7]> ybs[7]) ybs_accept=true;	
        if(y_B[0]>ye[0] && y_B[7]<ye[7]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && ybs_accept && yed_accept) result=true;        
    }    
    if(ID==32){
        // ALFA 1 6 
        // non-exact left-right matching mixing inner-outer
       	if(axo(x_B[0],x_B[5])) x_accept=true;
        if(ayy(y_B[0],y_B[5])) y_accept=true;
       	if(y_B[0]< ybs[0] && y_B[5]> ybs[5]) ybs_accept=true;	
        if(y_B[0]>ye[0] && y_B[5]<ye[5]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && ybs_accept && yed_accept) result=true;        
    }
    if(ID==33){
        // ALFA 3 8 
        // non-exact left-right matching mixing inner-outer
       	if(axo(x_B[2],x_B[7])) x_accept=true;
        if(ayy(y_B[2],y_B[7])) y_accept=true;
       	if(y_B[2]< ybs[2] && y_B[7]> ybs[7]) ybs_accept=true;	
        if(y_B[2]>ye[2] && y_B[7]<ye[7]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && ybs_accept && yed_accept) result=true;        
    }
    if(ID==34){
        // ALFA 2 7 
       	if(axo(x_B[1],x_B[6])) x_accept=true;
        if(ayy(y_B[1],y_B[6])) y_accept=true;
       	if(y_B[1]> ybs[1] && y_B[6]< ybs[6]) ybs_accept=true;	
        if(y_B[1]<ye[1] && y_B[6]>ye[6]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && ybs_accept && yed_accept) result=true;        
    }    
    if(ID==35){
        // ALFA 2 5 
       	if(axo(x_B[1],x_B[4])) x_accept=true;
        if(ayy(y_B[1],y_B[4])) y_accept=true;
       	if(y_B[1]> ybs[1] && y_B[4]< ybs[4]) ybs_accept=true;	
        if(y_B[1]<ye[1] && y_B[4]>ye[4]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && ybs_accept && yed_accept) result=true;        
    }    
    if(ID==36){
        // ALFA 4 7 
       	if(axo(x_B[3],x_B[6])) x_accept=true;
        if(ayy(y_B[3],y_B[6])) y_accept=true;
       	if(y_B[3]> ybs[3] && y_B[6]< ybs[6]) ybs_accept=true;	
        if(y_B[3]<ye[3] && y_B[6]>ye[6]) yed_accept=true;	
        // total selection condition
	    if(x_accept && y_accept && ybs_accept && yed_accept) result=true;        
    }    
    
    if(ID==18){
        // ALFA 3  
       	if(y_B[2] < ybs[2]) ybs_accept=true;	
        if(y_B[2] > ye[2]) yed_accept=true;	
        // total selection condition
	    if(ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==19){
        // ALFA 6  
       	if(y_B[5] > ybs[5]) ybs_accept=true;	
        if(y_B[5] < ye[5]) yed_accept=true;	
        // total selection condition
	    if(ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==20){
        // ALFA 4  
       	if(y_B[3] > ybs[3]) ybs_accept=true;	
        if(y_B[3] < ye[3]) yed_accept=true;	
        // total selection condition
	    if(ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==21){
        // ALFA 5  
       	if(y_B[4] < ybs[4]) ybs_accept=true;	
        if(y_B[4] > ye[4]) yed_accept=true;	
        // total selection condition
	    if(ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==41){
        // ALFA 1  
       	if(y_B[0] < ybs[0]) ybs_accept=true;	
        if(y_B[0] > ye[0]) yed_accept=true;	
        // total selection condition
	    if(ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==42){
        // ALFA 8  
       	if(y_B[7] > ybs[7]) ybs_accept=true;	
        if(y_B[7] < ye[7]) yed_accept=true;	
        // total selection condition
	    if(ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==43){
        // ALFA 2  
       	if(y_B[1] > ybs[1]) ybs_accept=true;	
        if(y_B[1] < ye[1]) yed_accept=true;	
        // total selection condition
	    if(ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==44){
        // ALFA 7  
       	if(y_B[6] < ybs[6]) ybs_accept=true;	
        if(y_B[6] > ye[6]) yed_accept=true;	
        // total selection condition
	    if(ybs_accept && yed_accept) result=true;        
    }
    
    if(ID==101){
        // only x selection arm 1
        if( 
            (y_B[0]<(a1[0]-x_B[0])) && (y_B[0]>(a2[0]+x_B[0])) &&
            (y_B[2]<(a1[1]-x_B[2])) && (y_B[2]>(a2[1]+x_B[2])) && 
            (y_B[5]>(a3[2]+x_B[5])) && (y_B[5]<(a4[2]-x_B[5])) &&   
            (y_B[7]>(a3[3]+x_B[7])) && (y_B[7]<(a4[3]-x_B[7]))            
        ) result=true;
        // total selection condition
    }
    
    if(ID==102){
        // only x selection arm 2
        if( 
            (y_B[4]<(a1[2]-x_B[4])) && (y_B[4]>(a2[2]+x_B[4])) &&
            (y_B[6]<(a1[3]-x_B[6])) && (y_B[6]>(a2[3]+x_B[6])) &&
            (y_B[3]>(a3[1]+x_B[3])) && (y_B[3]<(a4[1]-x_B[3])) &&   
            (y_B[1]>(a3[0]+x_B[1])) && (y_B[1]<(a4[0]-x_B[1]))            
        ) result=true;
        // total selection condition
    }
    
    return(result);
}




