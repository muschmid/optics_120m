     // MC offsets are determined from a comparison of Data and MC (dtmc_offsets.C) 
     Double_t x1_offset=-0.00547197;
     Double_t x2_offset=0.00262582;
     Double_t x3_offset=-0.000145589;
     Double_t x4_offset=-8.94859e-05;
     Double_t x5_offset=0.00124848;
     Double_t x6_offset=0.00113237;
     Double_t x7_offset=0.000232594;
     Double_t x8_offset=-0.000570981;
     Double_t y1_offset=0.0292892+0.0147159;
     Double_t y2_offset=-0.0266631-0.0161876;
     Double_t y3_offset=0.0312065+0.0154571;
     Double_t y4_offset=-0.0228513-0.0170071;
     Double_t y5_offset=0.018322+0.0169935;
     Double_t y6_offset=-0.0176272-0.0154133;
     Double_t y7_offset=0.0184286+0.0161802;
     Double_t y8_offset=-0.0163821-0.0146789;
