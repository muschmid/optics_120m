     // MC offsets are determined from a comparison of Data and MC (dtmc_offsets.C) 
     Double_t x1_offset=-0.00226794;
     Double_t x2_offset=0.000119118;
     Double_t x3_offset=0.00048319;
     Double_t x4_offset=0.00305453;
     Double_t x5_offset=-0.00343893;
     Double_t x6_offset=0.00397832;
     Double_t x7_offset=-0.00302065;
     Double_t x8_offset=-0.00148863;
     Double_t y1_offset=-0.0261283;
     Double_t y2_offset=0.0183575;
     Double_t y3_offset=-0.0254044;
     Double_t y4_offset=0.0219909;
     Double_t y5_offset=-0.0251365;
     Double_t y6_offset=0.0372356;
     Double_t y7_offset=-0.0172162;
     Double_t y8_offset=0.0280774;
