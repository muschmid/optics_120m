     // MC offsets are determined from a comparison of Data and MC (dtmc_offsets.C) 
     Double_t x1_offset=-0.00771395;
     Double_t x2_offset=0.0088138;
     Double_t x3_offset=0.00344178;
     Double_t x4_offset=-0.00238327;
     Double_t x5_offset=0.000385632;
     Double_t x6_offset=0.00103827;
     Double_t x7_offset=-0.000413934;
     Double_t x8_offset=-0.0008144;
     Double_t y1_offset=-0.0137503;
     Double_t y2_offset=0.0124722;
     Double_t y3_offset=-0.0132432;
     Double_t y4_offset=0.016683;
     Double_t y5_offset=-0.0188738;
     Double_t y6_offset=0.0198041;
     Double_t y7_offset=-0.00946818;
     Double_t y8_offset=0.00986763;
