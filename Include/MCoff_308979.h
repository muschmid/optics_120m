     // MC offsets are determined from a comparison of Data and MC (dtmc_offsets.C) 
     Double_t x1_offset=-0.00276102;
     Double_t x2_offset=0.00730937;
     Double_t x3_offset=0.000649235;
     Double_t x4_offset=-0.00246314;
     Double_t x5_offset=0.00190753;
     Double_t x6_offset=7.16326e-05;
     Double_t x7_offset=0.00534498;
     Double_t x8_offset=-0.00150254;
     Double_t y1_offset=-0.0299262;
     Double_t y2_offset=0.0339474;
     Double_t y3_offset=-0.0315139;
     Double_t y4_offset=0.0391058;
     Double_t y5_offset=-0.0431334;
     Double_t y6_offset=0.040355;
     Double_t y7_offset=-0.0322254;
     Double_t y8_offset=0.0308511;
