     // MC offsets are determined from a comparison of Data and MC (dtmc_offsets.C) 
     Double_t x1_offset=-0.00187239;
     Double_t x2_offset=-0.00261815;
     Double_t x3_offset=-0.000609525;
     Double_t x4_offset=0.00035554;
     Double_t x5_offset=-0.00469716;
     Double_t x6_offset=0.00340636;
     Double_t x7_offset=-0.00147878;
     Double_t x8_offset=-0.00376735;
     Double_t y1_offset=-0.0309771;
     Double_t y2_offset=0.0271361;
     Double_t y3_offset=-0.0319355;
     Double_t y4_offset=0.0331302;
     Double_t y5_offset=-0.0330449;
     Double_t y6_offset=0.0387527;
     Double_t y7_offset=-0.0229076;
     Double_t y8_offset=0.0297702;
