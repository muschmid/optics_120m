     // MC offsets are determined from a comparison of Data and MC (dtmc_offsets.C) 
     Double_t x1_offset=0.000506182;
     Double_t x2_offset=0.00194683;
     Double_t x3_offset=-0.00191664;
     Double_t x4_offset=-0.000949522;
     Double_t x5_offset=-0.00199532;
     Double_t x6_offset=0.0022052;
     Double_t x7_offset=-7.76725e-05;
     Double_t x8_offset=-0.00060273;
     Double_t y1_offset=-0.0256605;
     Double_t y2_offset=0.0194641;
     Double_t y3_offset=-0.0272776;
     Double_t y4_offset=0.0246877;
     Double_t y5_offset=-0.027897;
     Double_t y6_offset=0.0340447;
     Double_t y7_offset=-0.0187745;
     Double_t y8_offset=0.0250153;
