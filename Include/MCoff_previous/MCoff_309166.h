     // MC offsets are determined from a comparison of Data and MC (dtmc_offsets.C) 
     Double_t x1_offset=0.00262727;
     Double_t x2_offset=0.000944513;
     Double_t x3_offset=-0.00252622;
     Double_t x4_offset=-0.000723587;
     Double_t x5_offset=-0.00310928;
     Double_t x6_offset=0.00226226;
     Double_t x7_offset=0.00220216;
     Double_t x8_offset=-0.0047368;
     Double_t y1_offset=-0.0163507;
     Double_t y2_offset=0.0242219;
     Double_t y3_offset=-0.0168652;
     Double_t y4_offset=0.0295165;
     Double_t y5_offset=-0.0302807;
     Double_t y6_offset=0.0244296;
     Double_t y7_offset=-0.020413;
     Double_t y8_offset=0.0163173;
