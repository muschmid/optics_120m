#include <math.h>
Double_t MyGauss2 (Double_t *x, Double_t *par)
{
  Double_t cts  =par[0];
  Double_t X0   =par[1];
  Double_t Y0   =par[2];
  Double_t sigX =par[3];
  Double_t sigY =par[4];
  Double_t theta=par[5];

  Double_t a=(pow(cos(theta),2)/(2.*sigX*sigX))+(pow(sin(theta),2)/(2*sigY*sigY));
  Double_t b=-1.*(sin(2.*theta)/(4.*sigX*sigX))+(sin(2.*theta)/(4.*sigY*sigY));
  Double_t c=(pow(sin(theta),2)/(2.*sigX*sigX))+(pow(cos(theta),2)/(2*sigY*sigY));
  
  Double_t value = cts*exp(-1.*(a*(x[0]-X0)*(x[0]-X0)+2.*b*(x[0]-X0)*(x[1]-Y0)+c*(x[1]-Y0)*(x[1]-Y0)));
//  cout << " x = " << x[0] << " : " << x[1] << " FCN = " << value << "\n";
  return value;
}
