// params for BSW
//    Double_t pc1=56.31,pt1=-0.32,pb1=16.67,pc2=30.44,pt2=-0.47,pb2=10.70;
//    Double_t sPar[7]={56.31,-0.32,16.67,30.44,-0.47,10.70,1.0}; // intitial estimate based on 2.5 km data
//    Double_t sPar[7]={49.20,-0.41,12.95,10.05,-0.54,7.51,1.0}; // first estimate 90m data
    Double_t sPar[7]={50.71,-0.3935,13.61,14.6,-0.515,8.27,1.0}; // BSW second estimate 90m data
    Double_t hExp[2]={4.7,1.52};
// params for West-Yenni
    Double_t wyPar[5]={105.0,21.14,0.097,-6.7,17.4}; // BCD guess for 13.6 TeV
//    Double_t wyPar[5]={105.,21.18,0.078,-7.63,20.5}; // BCD rho syst
//    Double_t wyPar[5]={105.,21.11,0.09,-5.2,0.0}; // BC
//    Double_t wyPar[5]={105.,20.9,0.09,-5.2,0.0}; // B
 // IForm to select low-t for, i.e. WY Bt (=0) or WY Bt + Ct2 (=1) or WY Bt + Ct2 + Dt3 (=2)
    Int_t IForm=2;
    Bool_t notyet=false;
    Bool_t BHist=true;
    Bool_t switchoff=false; // if true take low-t prediction (West+Yenni) throughout
    Double_t tmatch=0.2; //WY-BSW matching
    Double_t texp=1.2; // BSW-EXP matching
    Double_t vlow=5.E-5;
    Double_t vhigh=3.0;
//    Double_t vlow=0.01;
//    Double_t vhigh=5.0;
    Long_t nEvent=10000000;
    Int_t nMad=250; // number of MADX files , might be modified later on 
    Int_t nSub=20000; // number of events per MADX-file
    Double_t p0=6800.;
    Int_t Iseed=4711; // initial random seed

Double_t WY(Double_t *x,Double_t *par)
// West-Yenni differential elastic cross section
{
    Bool_t Bourrely=false;
    Bool_t xL_fit=false;
  Double_t t=x[0];
  Double_t st=par[0];  //sigma_tot
  Double_t bs=par[1];  // B-slope 
  Double_t rho=par[2]; // rho  
  Double_t cs=par[3];  // C-term
  Double_t ds=par[4];  // D-term
  Double_t xL=par[5];
  Double_t lamb=0.71; // electric form factor parameter 
  Double_t phaseConst=0.577; // CoulombPhaseConstant (Gamma_E) 
  Double_t AEM=1./137.036; // electromagnetic finestrucure constant 
  Double_t conv=0.389379; // (hbar c)**2 conversion to mbarn
  Double_t fitval =0.0;
  Double_t ppi=TMath::Pi();
  if(!xL_fit) xL=1.0;
//  Double_t phase=TMath::Log(2./(bs*t))-phaseConst; // West and Yenni phase
//  Double_t phase = KFK(t,bs); // KFK phase model
   Double_t phase = Cahn(t,bs); // Cahn phase model
//  Double_t Anuc=ppi/2.-atan(rho)-800.*pow(t,2.311)*exp(-t*8.161); // periphal phase
//  Double_t Anuc=ppi/2.-atan(rho)*(1.-t/0.1); // simple phase
//  Double_t Anuc=ppi/2.-atan(rho)*(1.-t/0.16)/(1.-t/0.42); // Durand-Ha
//  Double_t Anuc=ppi/2.-atan(rho)+atan((t-0.5)/0.1)-atan(-0.5/0.1); // standard
//  Double_t Anuc=ppi/2.-atan(rho/(1.+t/0.53)); // Bailly
  Double_t Anuc=ppi/2.-atan(rho); // constant phase
   if(!notyet){
     FormFactorInit();
     notyet=true;
   }
  Double_t Gt = pow(1.+t/lamb,-2);// electric form factor standard Dipole
//   Double_t Gt = FormFactor(t);// electric form factor A1 fit Double Dipole
//   cout << "WY: t = " << t << " Standard Dipole = " << pow(1.+t/lamb,-2) << " double Dipole = " << Gt << "\n";
  // Bourrely and Wray
   Double_t mu=2.79282; // magnetic moment of proton
   Double_t mp=0.938272; // mass of proton 
   Double_t F1=(t*mu+4.*mp*mp)/(4.*mp*mp+t)/pow((1.+t/lamb),2); // F1 form factor
   Double_t F2=4.*mp*mp/(4.*mp*mp+t)/pow((1.+t/lamb),2); // F2 form factor
   Double_t om=6800.; // omega=1/2*sqrt(s)
   Double_t s=13600.*13600.;
   // auxillary terms for the Coulomb cross section Eq.8
   
   Double_t Front=4.*ppi/(s*(s-4.*mp*mp))*AEM*AEM/t/t*conv;
   Double_t t1=pow(F1+(mu-1.)*F2,4)*(pow(s-2.*mp*mp,2)-s*t+0.5*t*t);
   Double_t t2=0.25*pow(4.*(mu-1.)*F1*F2+(4.*mp*mp-t)/(2.*mp*mp)*(mu-1.)*(mu-1.)*F2*F2,2)*pow(s-2.*mp*mp-0.5*t,2);
   Double_t t3=-pow(F1+(mu-1.)*F2,2)*(4.*(mu-1.)*F1*F2+(4.*mp*mp-t)/(2.*mp*mp)*(mu-1.)*(mu-1.)*F2*F2)*(pow(s-2.*mp*mp,2)-t*(s-mp*mp));

   Double_t SCoul=Front*(t1+t2+t3);

// auxillary terms for the CNI
   Double_t t4=F1*F1*(2.*om*om-mp*mp-(3.*om+mp)/(4.*(om+mp))*t+t*t/(16.*pow(om+mp,2)));
   Double_t t5=-(mu-1.)*F1*F2/(2.*mp*(om+mp))*t*(2.*om*om+mp*om-mp*mp-0.5*t);
   Double_t t6=(mu-1.)*(mu-1.)*F2*F2/(8.*mp*mp*(om+mp))*t*t*(om-t/(8.*(om+mp)));
   
   Double_t Ac=-AEM/t/om*(t4+t5+t6);
   Double_t SInter=st*TMath::Exp(-bs*t/2.-cs*t*t/2.-ds*t*t*t/2.)*(TMath::Sin(AEM*phase)+rho*TMath::Cos(AEM*phase))*Ac/om/2.;
  
  Double_t rC=-2.*sqrt(ppi*conv)*AEM*TMath::Power(Gt,2)/t;
  Double_t rN=0.0;
  if(IForm==0) rN=sqrt(1.+rho*rho)*st/(4.*sqrt(ppi*conv))*exp(-bs*t/2.);
  if(IForm==1) rN=sqrt(1.+rho*rho)*st/(4.*sqrt(ppi*conv))*exp(-bs*t/2.-cs*t*t/2.);
  if(IForm==2) rN=sqrt(1.+rho*rho)*st/(4.*sqrt(ppi*conv))*exp(-bs*t/2.-cs*t*t/2.-ds*t*t*t/2.);
  fitval=rC*rC+rN*rN+2.*rC*rN*cos(AEM*phase-Anuc);
  if(Bourrely){
      fitval=SCoul+SInter+rN*rN; // full formula
  }
  
/*  
 if(IForm==0){
   Double_t Coulomb=4.*ppi*conv*AEM*AEM*TMath::Power(Gt,4)/(t*t);
   Double_t Interf=-st*AEM*TMath::Exp(-bs*t/2.)*(TMath::Sin(AEM*phase)+rho*TMath::Cos(AEM*phase))*Gt*Gt/t;
//  Double_t Interf=-st*AEM*TMath::Exp(-bs*t/2.)*(AEM*phase+rho)*Gt*Gt/t; // TEVATRON form: same result 
   Double_t Nucl=st*st*(1.+rho*rho)*TMath::Exp(-bs*t)/(16.*ppi*conv);
 
//  fitval=Nucl; // test only nuc
  fitval=Coulomb+Interf+Nucl; // full formula
 }
 
  if(IForm==1){ // traditional Ct2 fit
   Double_t Coulomb=4.*ppi*conv*AEM*AEM*TMath::Power(Gt,4)/(t*t);
            Coulomb=rC*rC;
   Double_t Interf=-st*AEM*TMath::Exp(-bs*t/2.-cs*t*t/2)*(TMath::Sin(AEM*phase)+rho*TMath::Cos(AEM*phase))*Gt*Gt/t;
   Double_t Nucl=st*st*(1.+rho*rho)*TMath::Exp(-bs*t-cs*t*t)/(16.*ppi*conv);
            Nucl=rN*rN;
  //cout << "Interf = " << Interf << " 2.*rC*rN*cos(-AEM*phase) = " << 2.*rC*rN*cos(AEM*phase) << "\n"; 
   Interf=2.*rC*rN*TMath::Cos(AEM*phase-ppi/2+atan(rho));
   fitval=Coulomb+Interf+Nucl; 
  }
 */ 
  return xL*fitval;
}
 
Double_t BSW(Double_t *x,Double_t *par){
    
   Double_t fitval=0.0;
   Double_t t=x[0];
   Double_t C1=par[0];
   Double_t t1=par[1];
   Double_t B1=par[2];
   Double_t C2=par[3];
   Double_t t2=par[4];
   Double_t B2=par[5];
   Double_t Norm=par[6];
  
   fitval = Norm*(C1*C1*(t1+t)*(t1+t)*exp(-B1*t) + C2*C2*(t2+t)*(t2+t)*exp(-B2*t));
   return fitval; 
}

Double_t tExp(Double_t *x,Double_t *par){
    
   Double_t fitval=0.0;
   Double_t t=x[0];
   Double_t B=par[0];
   Double_t A=par[1];
  
   fitval = A*exp(-B*t);
   return fitval; 
}

Double_t tfinal(Double_t *x,Double_t *par){
    
   Double_t fitval=0.0;
   Double_t t=x[0];
   Double_t wypar[5]={par[0],par[1],par[2],par[3],par[4]};
   Double_t bswpar[7]={par[5],par[6],par[7],par[8],par[9],par[10],par[11]};
   Double_t hexpar[2]={par[12],par[13]};
   Double_t wm = WY(x,wypar); 

   if(t<=tmatch) fitval=wm;
   if(t>tmatch && t<texp) fitval=BSW(x,bswpar);
   if(t>texp) fitval=tExp(x,hexpar);
   
   if(switchoff) fitval=wm; // take West+Yenni anyhow
//   cout << "tfinal t = " << t << " dsigma/dt = " << fitval << "\n"; 
   
   return fitval; 
}

Double_t tfinal_xL(Double_t *x,Double_t *par){
    
   Double_t fitval=0.0;
   Double_t t=x[0];
   Double_t wypar[6]={par[0],par[1],par[2],par[3],par[4],par[5]};
   Double_t bswpar[7]={par[6],par[7],par[8],par[9],par[10],par[11],par[12]};
   Double_t hexpar[2]={par[13],par[14]};
   Double_t wm = WY(x,wypar); 

   if(t<=tmatch) fitval=wm;
   if(t>tmatch && t<texp) fitval=BSW(x,bswpar);
   if(t>texp) fitval=tExp(x,hexpar);
   
   if(switchoff) fitval=wm; // take West+Yenni anyhow
//   cout << "tfinal t = " << t << " dsigma/dt = " << fitval << "\n"; 
   
   return fitval; 
}

Double_t WYxl(Double_t *x,Double_t *par){
    
   Double_t fitval=0.0;
   Double_t t=x[0];
   Double_t wypar[4]={par[0],par[1],par[2],par[3]};
   Double_t xLpar=par[4];//Luminosity rescaling factor
   Double_t wm = WY(x,wypar); 
    
//   cout << " WYxl wm = " << wm << " xL= " << xLpar << "\n";
   fitval=wm*xLpar;
   
   return fitval; 
}

Double_t tlog10(Double_t *x,Double_t *par){
    
   Double_t fitval=0.0;
   Double_t t[2];
   t[0]=pow(10.,x[0]);
   Double_t wypar[4]={par[0],par[1],par[2],par[3]};
   Double_t bswpar[7]={par[4],par[5],par[6],par[7],par[8],par[9],par[10]};
   Double_t hexpar[2]={par[11],par[12]};
   Double_t wm = WY(t,wypar); 

   if(t[0]<=tmatch) fitval=wm;
   if(t[0]>tmatch && t[0]<texp) fitval=BSW(t,bswpar);
   if(t[0]>texp) fitval=tExp(t,hexpar);
   
   if(switchoff) fitval=wm; // take West+Yenni anyhow
//   cout << "tlog10 t = " << t[0] << " dsigma/dt = " << fitval << "\n"; 
   fitval=fitval*log(10.)*t[0];
   
   
   return fitval; 
}

