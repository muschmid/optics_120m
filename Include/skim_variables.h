  //Run Header
  
  // Declaration of leaf types
  Int_t           Fill_num;
  Int_t           Run_num;
  Bool_t          Trigger_set[8][6];
  Int_t           Latency[8][3];
  vector<string>* mainDet_gainMode;
  Int_t           Threshold[8][3];
  Int_t           Gain[8][5];
  Float_t         TransformDetRP[8][4][4];
  Float_t         Proton_momentum;

  //Event Header

  UInt_t          Evt_num;
  Int_t           BCId;
  Int_t           TimeStp;
  Int_t           TimeStp_ns;
  Int_t           Scaler[8];
  UInt_t          Lum_block;
  Bool_t          TrigPat[8][16];
  Int_t           QDC_Trig[8][2];
  Bool_t          LVL1TrigSig[256];
  Bool_t          LVL2TrigSig[256];
  Bool_t          HLTrigSig[256];
  Bool_t          FiberHitsMD[8][20][64];
  Bool_t          FiberHitsODPos[8][3][30];
  Bool_t          FiberHitsODNeg[8][3][30];
  Int_t           PotPos;
  Int_t           Temperature;
  Int_t           Volt;
  Int_t           RadDose;
  Int_t           BeamLossMon;
  Int_t           TRate;
  Int_t           BPM;
  Bool_t          DQFlag[25];
  Int_t           MultiMD[8][20];
  Int_t           MultiODPos[8][3];
  Int_t           MultiODNeg[8][3];
 
  //Tracking Data
  const int MAXNUMTRACKS     = 100;

  // Declaration of leaf types
  Int_t           NumTrack;
  Bool_t          RecFlag[MAXNUMTRACKS][8];   //[NumTrack]
  Int_t           Detector[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         x_Det[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         y_Det[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         x_Pot[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         y_Pot[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         x_Stat[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         y_Stat[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         x_LHC[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         y_LHC[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         x_LHC_Corr[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         y_LHC_Corr[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         x_Beam[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         y_Beam[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         OverU[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         OverV[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         OverY[MAXNUMTRACKS][8];   //[NumTrack]
  Int_t           NU[MAXNUMTRACKS][8];   //[NumTrack]
  Int_t           NV[MAXNUMTRACKS][8];   //[NumTrack]
  Int_t           NY[MAXNUMTRACKS][8];   //[NumTrack]
  Int_t           Fib_SelMD[MAXNUMTRACKS][8][20];   //[NumTrack]
  Int_t           Fib_SelOD[MAXNUMTRACKS][8][3];   //[NumTrack]
  Float_t         intercept_Det[MAXNUMTRACKS][8];   //[NumTrack]
  Float_t         slope_Det[MAXNUMTRACKS][8];   //[NumTrack]
  
  // adding skim tracks
  
  Double_t x_A[8],y_A[8];
  Int_t Nu[8],Nv[8];

	// Here are the new Trigger-Variables. I declared them all, but I will only use the elastic 15/18 triggers in the example
	Bool_t                         L1_EM10VH;
	Bool_t                           L1_EM15;
	Bool_t                         L1_EM20VH;
	Bool_t                        L1_EM20VHI;
	Bool_t                      L1_EM3_EMPTY;
	Bool_t                      L1_EM7_EMPTY;
	Bool_t                           L1_MU15;
	Bool_t                      L1_MU4_EMPTY;
	Bool_t                     L1_MU11_EMPTY;
	Bool_t                        L1_2EM10VH;
	Bool_t                           L1_2MU6;
	Bool_t                           L1_3MU4;
	Bool_t                       L1_EM7_MU10;
	Bool_t                      L1_EM15I_MU4;
	Bool_t                    L1_2EM8VH_MU10;
	Bool_t                    L1_EM15VH_MU10;
	Bool_t                          L1_TAU60;
	Bool_t                     L1_TAU8_EMPTY;
	Bool_t                L1_EM15HI_2TAU12IM;
	Bool_t       L1_EM15HI_2TAU12IM_J25_3J12;
	Bool_t            L1_EM15HI_TAU40_2TAU15;
	Bool_t                   L1_MU10_TAU12IM;
	Bool_t          L1_MU10_TAU12IM_J25_2J12;
	Bool_t            L1_MU10_TAU12_J25_2J12;
	Bool_t                     L1_MU10_TAU20;
	Bool_t L1_TAU20IM_2TAU12IM_J25_2J20_3J12;
	Bool_t                L1_TAU20_2J20_XE45;
	Bool_t           L1_EM15HI_2TAU12IM_XE35;
	Bool_t              L1_MU10_TAU12IM_XE35;
	Bool_t          L1_TAU20IM_2TAU12IM_XE35;
	Bool_t              L1_TAU20_2TAU12_XE35;
	Bool_t                        L1_MU6_J40;
	Bool_t                            L1_J12;
	Bool_t                            L1_J75;
	Bool_t                           L1_J100;
	Bool_t                    L1_J30_31ETA49;
	Bool_t                    L1_J75_31ETA49;
	Bool_t                   L1_J100_31ETA49;
	Bool_t                      L1_J12_EMPTY;
	Bool_t                 L1_J12_FIRSTEMPTY;
	Bool_t               L1_J12_UNPAIRED_ISO;
	Bool_t            L1_J12_UNPAIRED_NONISO;
	Bool_t           L1_J12_ABORTGAPNOTCALIB;
	Bool_t                      L1_J30_EMPTY;
	Bool_t                 L1_J30_FIRSTEMPTY;
	Bool_t              L1_J30_31ETA49_EMPTY;
	Bool_t       L1_J30_31ETA49_UNPAIRED_ISO;
	Bool_t    L1_J30_31ETA49_UNPAIRED_NONISO;
	Bool_t               L1_J50_UNPAIRED_ISO;
	Bool_t            L1_J50_UNPAIRED_NONISO;
	Bool_t           L1_J50_ABORTGAPNOTCALIB;
	Bool_t                           L1_3J40;
	Bool_t                           L1_4J15;
	Bool_t                      L1_2J15_XE55;
	Bool_t                       L1_J40_XE50;
	Bool_t                       L1_J75_XE40;
	Bool_t                           L1_XE50;
	Bool_t                           L1_XE80;
	Bool_t                   L1_MBTS_1_EMPTY;
	Bool_t            L1_MBTS_1_UNPAIRED_ISO;
	Bool_t                     L1_RD0_FILLED;
	Bool_t               L1_RD0_UNPAIRED_ISO;
	Bool_t                      L1_RD0_EMPTY;
	Bool_t                      L1_RD1_EMPTY;
	Bool_t                          L1_LUCID;
	Bool_t                    L1_LUCID_EMPTY;
	Bool_t             L1_LUCID_UNPAIRED_ISO;
	Bool_t                L1_BCM_AC_CA_BGRP0;
	Bool_t                         L1_MBTS_1;
	Bool_t                 L1_EM7_FIRSTEMPTY;
	Bool_t           L1_RD0_ABORTGAPNOTCALIB;
	Bool_t                    L1_3J25_0ETA23;
	Bool_t                       L1_MU4_3J20;
	Bool_t                L1_TAU8_FIRSTEMPTY;
	Bool_t              L1_EM20VH_FIRSTEMPTY;
	Bool_t                L1_J100_FIRSTEMPTY;
	Bool_t        L1_J100_31ETA49_FIRSTEMPTY;
	Bool_t                   L1_ALFA_ELAST15;
	Bool_t             L1_ALFA_ELAST15_Calib;
	Bool_t                   L1_ALFA_ELAST18;
	Bool_t             L1_ALFA_ELAST18_Calib;
	Bool_t                    L1_ALFA_SDIFF5;
	Bool_t                    L1_ALFA_SDIFF6;
	Bool_t                    L1_ALFA_SDIFF7;
	Bool_t                    L1_ALFA_SDIFF8;
	Bool_t                L1_MBTS_1_A_ALFA_C;
	Bool_t                L1_MBTS_1_C_ALFA_A;
	Bool_t   L1_MBTS_1_A_ALFA_C_UNPAIRED_ISO;
	Bool_t   L1_MBTS_1_C_ALFA_A_UNPAIRED_ISO;
	Bool_t                L1_MBTS_2_A_ALFA_C;
	Bool_t                L1_MBTS_2_C_ALFA_A;
	Bool_t   L1_MBTS_2_A_ALFA_C_UNPAIRED_ISO;
	Bool_t   L1_MBTS_2_C_ALFA_A_UNPAIRED_ISO;
	Bool_t                 L1_LUCID_A_ALFA_C;
	Bool_t                 L1_LUCID_C_ALFA_A;
	Bool_t    L1_LUCID_A_ALFA_C_UNPAIRED_ISO;
	Bool_t    L1_LUCID_C_ALFA_A_UNPAIRED_ISO;
	Bool_t                   L1_EM3_ALFA_ANY;
	Bool_t      L1_EM3_ALFA_ANY_UNPAIRED_ISO;
	Bool_t                  L1_EM3_ALFA_EINE;
	Bool_t      L1_ALFA_ELASTIC_UNPAIRED_ISO;
	Bool_t L1_ALFA_ANTI_ELASTIC_UNPAIRED_ISO;
	Bool_t                   L1_J12_ALFA_ANY;
	Bool_t      L1_J12_ALFA_ANY_UNPAIRED_ISO;
	Bool_t                   L1_TE3_ALFA_ANY;
	Bool_t      L1_TE3_ALFA_ANY_UNPAIRED_ISO;
	Bool_t                  L1_TE3_ALFA_EINE;
	Bool_t                L1_ALFA_BGT_BGRP10;
	Bool_t                    L1_ALFA_SYST11;
	Bool_t                    L1_ALFA_SYST12;
	Bool_t                    L1_ALFA_SYST17;
	Bool_t                    L1_ALFA_SYST18;
	Bool_t                       L1_ALFA_ANY;
	Bool_t                 L1_ALFA_ANY_EMPTY;
	Bool_t            L1_ALFA_ANY_FIRSTEMPTY;
	Bool_t          L1_ALFA_ANY_UNPAIRED_ISO;
	Bool_t       L1_ALFA_ANY_UNPAIRED_NONISO;
	Bool_t      L1_ALFA_ANY_ABORTGAPNOTCALIB;
	Bool_t                 L1_ALFA_ANY_CALIB;
	Bool_t                   L1_ALFA_B7L1_OD;
	Bool_t                   L1_ALFA_A7L1_OD;
	Bool_t                   L1_ALFA_B7R1_OD;
	Bool_t                   L1_ALFA_A7R1_OD;
	Bool_t                        L1_CALREQ2;
