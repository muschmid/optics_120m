void renormalize(TH1D *h1){
// divide bins by bin width and set proper error
int nbins = h1->GetNbinsX();
  for (int iBin = 1; iBin <= nbins; ++iBin) {
   Double_t t1=h1->GetBinContent(iBin);
   Double_t et=h1->GetBinError(iBin);
   Double_t dt=h1->GetBinWidth(iBin);
   h1->SetBinContent(iBin,t1/dt);
//   cout << "Ibin = " << iBin << " bin width scaling factor = " << 1./dt << "\n";
//   cout << " Renormalize Bin " << iBin << " Covariance error square = " << et*et/dt/dtxy  << "\n";
   h1->SetBinError(iBin,et/dt);   
  }
return;
}

void unnormalize(TH1D *h1){
// revert normalisation, e.g. before unfolding
int nbins = h1->GetNbinsX();
  for (int iBin = 1; iBin <= nbins; ++iBin) {
   Double_t t1=h1->GetBinContent(iBin);
   Double_t et=h1->GetBinError(iBin);
   Double_t dt=h1->GetBinWidth(iBin);
   h1->SetBinContent(iBin,t1*dt);
//   cout << "Ibin = " << iBin << " bin width scaling factor = " << 1./dt << "\n";
//   cout << " Renormalize Bin " << iBin << " Covariance error square = " << et*et/dt/dtxy  << "\n";
   h1->SetBinError(iBin,et*dt);   
  }
return;
}

void renormalize_cov(TH2D *h1){
// calculate error for covariance matrix
int nbinsx = h1->GetNbinsX();
int nbinsy = h1->GetNbinsY();
  for (int iBinx = 1; iBinx <= nbinsx; ++iBinx) {
    for (int iBiny = 1; iBiny <= nbinsy; ++iBiny) {
     Double_t t1=h1->GetBinContent(iBinx,iBiny);
     Double_t dtx = h1->GetXaxis()->GetBinWidth(iBinx);
     Double_t dty = h1->GetYaxis()->GetBinWidth(iBiny);
//     cout << "Covariance  binX = " << iBinx << " binY = " << iBiny << " before = " << t1 << " after = " << t1/dtx/dty << " dt = " << dtx << "\n";
     h1->SetBinContent(iBinx,iBiny,t1/dtx/dty);
    }
  }
return;
}

void Mnormalise(TH2D *h1){
// normalize transition matrix to folding matrix
int nbinsx = h1->GetNbinsX();
int nbinsy = h1->GetNbinsY();
Double_t tmpmat[nf][nf];
Double_t dtmpmat[nf][nf];
  for (int iBinx = 1; iBinx <= nbinsx; ++iBinx) {
   for (int iBiny = 1; iBiny <= nbinsy; ++iBiny) {
     Double_t tmp=0.0;
     for(int ii=1;ii<=nbinsx;ii++){
       tmp=tmp+h1->GetBinContent(ii,iBiny);
     }
     if(tmp>0){
       tmpmat[iBinx-1][iBiny-1]=(h1->GetBinContent(iBinx,iBiny))/tmp;
       dtmpmat[iBinx-1][iBiny-1]=sqrt((h1->GetBinContent(iBinx,iBiny)))/tmp;
     }
     else{
      tmpmat[iBinx-1][iBiny-1]=0.0; 
      dtmpmat[iBinx-1][iBiny-1]=0.0; 
     }
   }
  }
  for (int iBinx = 1; iBinx <= nbinsx; ++iBinx) {
   for (int iBiny = 1; iBiny <= nbinsy; ++iBiny) {
     h1->SetBinContent(iBinx,iBiny,tmpmat[iBinx-1][iBiny-1]);
     h1->SetBinError(iBinx,iBiny,dtmpmat[iBinx-1][iBiny-1]);
//     cout << " Mnormalize " << iBinx << "\t" << iBiny << " Matrix = " << tmpmat[iBinx-1][iBiny-1] << " Error = " << dtmpmat[iBinx-1][iBiny-1] << "\n";
   }
  }
       
return;
}

TH1D* fold(TH2D *h2,TH1D* h1){
  
 int nbinsx = h2->GetNbinsX();
 int nbinsy = h2->GetNbinsY();
  
  TH1D* hf   = new TH1D("hf",";trec;that",nf,tf);
  for (int iBinx = 1; iBinx <= nbinsx; ++iBinx) {
   Double_t tmpsum=0.0,dtmpsum=0.0;
   for (int iBiny = 1; iBiny <= nbinsy; ++iBiny) {
     Double_t tmp1=(h2->GetBinContent(iBinx,iBiny));
     Double_t dtmp1=(h2->GetBinError(iBinx,iBiny));
     Double_t tmp2=h1->GetBinContent(iBiny);
     Double_t dtmp2=h1->GetBinError(iBiny);
     tmpsum=tmpsum+tmp1*tmp2;
     dtmpsum=dtmpsum+tmp2*tmp2*dtmp1*dtmp1+tmp1*tmp1*dtmp2*dtmp2;
   }
//    cout << " fold " << iBinx << " tmpsum = " << tmpsum << " dtmpsum = " << dtmpsum << "\n";
    hf->SetBinContent(iBinx,tmpsum);
    hf->SetBinError(iBinx,sqrt(dtmpsum));
  }
  return(hf);
}

Double_t RangeSum(TH1D *h1, Double_t low, Double_t high){
// returns the sum of bins in given range
int nbins = h1->GetNbinsX();
Double_t result=0.0;
  for (int iBin = 1; iBin <= nbins; ++iBin) {
   Double_t t1=h1->GetBinLowEdge(iBin);
   Double_t t2=h1->GetBinLowEdge(iBin+1);
   if(t1 > low && t2 < high) {
    Double_t t3=h1->GetBinContent(iBin);
    result=result+t3;
   }
  }
return result;
}

Double_t TotSum(TH1D *h1){
// returns the sum of all bins
int nbins = h1->GetNbinsX();
Double_t result=0.0;
  for (int iBin = 1; iBin <= nbins; ++iBin) {
    Double_t t3=h1->GetBinContent(iBin);
    result=result+t3;
  }
return result;
}

Double_t mabs(Double_t t1){
  Double_t result=sqrt(t1*t1);
  return result;
}

Double_t DistL(Int_t n1, Int_t n2, Float_t x[][8],Float_t y[][8]){
// n1=track, n2=detector
Double_t result=0.0;
Double_t xd=0.0,yd=0.0;
Int_t m2=-1, m3=-1, m4=-1;
if(n2==0){
  m2=2;
  m3=5;
  m4=7;
}
if(n2==2){
  m2=0;
  m3=5;
  m4=7;
}
if(n2==5){
  m2=7;
  m3=0;
  m4=2;
}
if(n2==7){
  m2=5;
  m3=0;
  m4=2;
}
if(n2==1){
  m2=3;
  m3=4;
  m4=6;
}
if(n2==3){
  m2=1;
  m3=4;
  m4=6;
}
if(n2==4){
  m2=6;
  m3=1;
  m4=3;
}
if(n2==6){
  m2=4;
  m3=1;
  m4=3;
}
if(m2==-1){
return result;
cout << "DistL problem no match found for multitrack detector " << n2 << "\n";
}

xd=(x[n1][n2]+x[0][m2]+x[0][m3]+x[0][m4])*(x[n1][n2]+x[0][m2]+x[0][m3]+x[0][m4]);
yd=(y[n1][n2]+y[0][m2]+y[0][m3]+y[0][m4])*(y[n1][n2]+y[0][m2]+y[0][m3]+y[0][m4]);
result=xd+yd;
return result;
}









