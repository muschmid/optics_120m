
#include "TGraphAsymmErrors.h"
#include "TMatrixD.h"
#include "TGraphErrors.h"
#include "TProfile.h"
#include "TF1.h"
#include "TEfficiency.h"

 TH1D *totalsyst;

// Simple version, raw data, background, acceptance , cut efficiency
TGraphAsymmErrors* fitprep(TH1D *raw, TH1D *back, TEfficiency *acc,TEfficiency *eff, TH1D *div, Int_t iarm){
raw->Sumw2();

Int_t nbins = raw->GetNbinsX();
Double_t x[nf],y[nf],exl[nf],exh[nf],eyl[nf],eyh[nf];
Bool_t background=false,recef=true, divcor=true;
Double_t effBin,errUp,errLo,centrT;

  if(background) raw->Add(back,-1.0);
  if(divcor) raw->Multiply(div); //no unfolding for now

  for (int iBin = 1; iBin <= nbins; ++iBin) {
   Int_t j=iBin-1;
   
   Double_t t=raw->GetBinContent(iBin);
   Double_t dt=raw->GetBinError(iBin);
   Double_t a=acc->GetEfficiency(iBin);
   Double_t ap=acc->GetEfficiencyErrorUp(iBin);
   Double_t am=acc->GetEfficiencyErrorLow(iBin);
   Double_t e=eff->GetEfficiency(iBin);
   Double_t ep=eff->GetEfficiencyErrorUp(iBin);
   Double_t em=eff->GetEfficiencyErrorLow(iBin);
   Double_t r=eps[iarm],rp=deps[iarm],rm=deps[iarm];
   
   if(recef){
    ep=sqrt(r*r*ep*ep+e*e*rp*rp);
    em=sqrt(r*r*em*em+e*e*rm*rm);
    e=e*r;
   }

    y[iBin-1] = t;
    eyl[iBin-1] = dt;
    eyh[iBin-1] = dt;
    x[iBin-1] = raw->GetBinCenter(iBin);
    exh[iBin-1] = 0.5*(raw->GetBinWidth(iBin));
    exl[iBin-1] = 0.5*(raw->GetBinWidth(iBin));
   
   if(a > 0 && e > 0){
    Double_t d = e*a;
    y[iBin-1] = t/d;
    Double_t dp=sqrt(a*a*ep*ep+e*e*ap*ap); 
    Double_t dm=sqrt(a*a*em*em+e*e*am*am);
    eyh[iBin-1] = sqrt(dt*dt/d/d + t*t*dp*dp/d/d/d/d);
    eyl[iBin-1] = sqrt(dt*dt/d/d + t*t*dm*dm/d/d/d/d);
    
  }
//  cout << iBin << " t = " << tt  << " raw " << t << " Background = " << b << " acceptance = " << a << " efficiency = " << e << " corrected = " << y[iBin-1] <<"\n";
}


TGraphAsymmErrors *gr = new TGraphAsymmErrors(nbins,x,y,exl,exh,eyl,eyh);
return gr;
}
// Unfolded version data, acceptance , cut efficiency reco efficiency
TGraphAsymmErrors* uprep(TH1D *raw, TEfficiency *acc,TEfficiency *eff, Int_t iarm){
raw->Sumw2();

Int_t nbins = raw->GetNbinsX();
Double_t x[nf],y[nf],exl[nf],exh[nf],eyl[nf],eyh[nf];
Double_t effBin,errUp,errLo,centrT;

  for (int iBin = 1; iBin <= nbins; ++iBin) {
   Int_t j=iBin-1;
   
   Double_t t=raw->GetBinContent(iBin);
   Double_t dt=raw->GetBinError(iBin);
   Double_t a=acc->GetEfficiency(iBin);
   Double_t ap=acc->GetEfficiencyErrorUp(iBin);
   Double_t am=acc->GetEfficiencyErrorLow(iBin);
   Double_t e=eff->GetEfficiency(iBin);
   Double_t ep=eff->GetEfficiencyErrorUp(iBin);
   Double_t em=eff->GetEfficiencyErrorLow(iBin);
   Double_t r=eps[iarm],rp=deps[iarm],rm=deps[iarm];
      
    ep=sqrt(r*r*ep*ep+e*e*rp*rp);
    em=sqrt(r*r*em*em+e*e*rm*rm);
    e=e*r;

    y[iBin-1] = t;
    eyl[iBin-1] = dt;
    eyh[iBin-1] = dt;
    x[iBin-1] = raw->GetBinCenter(iBin);
    exh[iBin-1] = 0.5*(raw->GetBinWidth(iBin));
    exl[iBin-1] = 0.5*(raw->GetBinWidth(iBin));
   
   if(a > 0 && e > 0){
    Double_t d = e*a;
    y[iBin-1] = t/d;
    Double_t dp=sqrt(a*a*ep*ep+e*e*ap*ap); 
    Double_t dm=sqrt(a*a*em*em+e*e*am*am);
    eyh[iBin-1] = sqrt(dt*dt/d/d + t*t*dp*dp/d/d/d/d);
    eyl[iBin-1] = sqrt(dt*dt/d/d + t*t*dm*dm/d/d/d/d);
    
  }
//  cout << iBin << " t = " << tt  << " raw " << t << " Background = " << b << " acceptance = " << a << " efficiency = " << e << " corrected = " << y[iBin-1] <<"\n";
}

TGraphAsymmErrors *gr = new TGraphAsymmErrors(nbins,x,y,exl,exh,eyl,eyh);
return gr;
}

TGraphAsymmErrors* comb_arms(TGraphAsymmErrors *gr1, TGraphAsymmErrors *gr2){

//Int_t np = gr1->GetN();

//if(np != nf) {
//  cout << "Arm combination mismatch nf = " << nf << " np = " << np << "\n";
//  return gr1;
//}

Double_t x[nf],y[nf],exl[nf],exh[nf],eyl[nf],eyh[nf];
Double_t x1,x2,y1,y2,eyh1,eyl1,eyh2,eyl2;

Int_t mp=0;

  for (Int_t ii = 0; ii < nf; ii++) {
    gr1->GetPoint(ii,x1,y1);
    gr2->GetPoint(ii,x2,y2);
//    cout << " x1 = " << x1 << " x2 = " << x2 << " y1 = " << y1 << " y2 " << y2 << "\n";
    mp=ii;
     x[mp]=x1;
     exl[mp]=gr1->GetErrorXlow(ii),exh[mp]=gr1->GetErrorXhigh(ii);
     eyl1=gr1->GetErrorYlow(ii),eyh1=gr1->GetErrorYhigh(ii);
     eyl2=gr2->GetErrorYlow(ii),eyh2=gr2->GetErrorYhigh(ii);
     y[mp]=(y1+y2);
     Double_t dyh=sqrt(eyh1*eyh1+eyh2*eyh2);
     Double_t dyl=sqrt(eyl1*eyl1+eyl2*eyl2);
     
     eyl[mp]=dyl;
     eyh[mp]=dyh;
//     eyl[mp]=sqrt(dyl*dyl/xL/xL + (y1+y2)*(y1+y2)*exL*exL/xL/xL/xL/xL);
//     eyh[mp]=sqrt(dyh*dyh/xL/xL + (y1+y2)*(y1+y2)*exL*exL/xL/xL/xL/xL);
     
//     cout << "Comb_arms bin = " << mp << " error = " << max(eyl[mp],eyh[mp]) << "\n";
     
   // that one is a bit buggy!  
   //  eyl[mp]=sqrt(eyl1*eyl1+eyl2*eyl2 + (y1+y2)*(y1+y2)*exL*exL/xL/xL)/xL;
   //  eyh[mp]=sqrt(eyh1*eyh1+eyh2*eyh2 + (y1+y2)*(y1+y2)*exL*exL/xL/xL)/xL;
     // add here systematic error  
//      Double_t error = totalsyst->GetBinContent(ii+1);
//      eyl[mp]=sqrt(eyl[mp]*eyl[mp]+error*error);
//      eyh[mp]=sqrt(eyh[mp]*eyh[mp]+error*error);
 
//  cout << "Diag error arm 1 bin  " << mp << " = " << sqrt(eyh1*eyh1/xL/xL+y1*y1*exL*exL/xL/xL/xL/xL) << "\n";
//  cout << "Diag error arm 2 bin  " << mp << " = " << sqrt(eyh2*eyh2/xL/xL+y2*y2*exL*exL/xL/xL/xL/xL) << "\n";
//   cout << "Diag error  bin = " << mp << " = "  << (eyh[mp]+ eyl[mp])/2. << "\n";
//    cout << ii << " t = " << x[ii] << " combined " << y[ii] << " error up " << eyh[ii] << "\n";
//  cout << ii << " t = " << x[ii] << " error low " << eyl[ii] << " error high " << eyh[ii] << "\n";
       
  }
  
  
 TGraphAsymmErrors *gr = new TGraphAsymmErrors(nf,x,y,exl,exh,eyl,eyh);
// cout << "Points in comb_graf = " << mp << "\n";
//Double_t xmin = gr->GetXaxis()->GetXmin(), xmax = gr->GetXaxis()->GetXmax();
//cout << " Min = " << xmin << " Max = " << xmax << "\n";
 return gr;

}
TGraphAsymmErrors* graf_band(TGraphAsymmErrors *gr){
// actually don't do much, just copy graph, keep the error but reset central values.

Int_t np = gr->GetN();
TGraphAsymmErrors *band = (TGraphAsymmErrors*)gr->Clone("band");
Double_t x,y,eyl,eyh;

  for (Int_t ii = 0; ii < np; ii++) {
    gr->GetPoint(ii,x,y);
    band->SetPoint(ii,x,0.0);
    eyh=gr->GetErrorYhigh(ii);
    eyl=gr->GetErrorYlow(ii);
//    if(y>0.0) cout << " graf_band point " << ii << " value = " << y << " High = " << eyh << " Low = " << eyl << "\n"; 
    if(y>0.0) eyh=eyh/y,eyl=eyl/y;
//    if(y==0.0) cout << " graf_band problem: zero graf central value bin = " << ii <<"\n";
    band->SetPointEYhigh(ii,eyh);
    band->SetPointEYlow(ii,eyl);
  }
 return band;
}

TGraphAsymmErrors* comb_L(TGraphAsymmErrors *gr1){
// actually doesn't combine anything, just normalizes to the luminosity

Int_t np = gr1->GetN();

//if(np != nf) {
//  cout << "Arm combination mismatch nf = " << nf << " np = " << np << "\n";
//  return gr1;
//}

Double_t x[nf],y[nf],exl[nf],exh[nf],eyl[nf],eyh[nf];
Double_t x1,x2,y1,y2,eyh1,eyl1,eyh2,eyl2;

Int_t mp=0;

  for (Int_t ii = 0; ii < np; ii++) {
    gr1->GetPoint(ii,x1,y1);
    if(y1>0){
     x[mp]=x1;
     exl[mp]=gr1->GetErrorXlow(ii),exh[mp]=gr1->GetErrorXhigh(ii);
     eyl1=gr1->GetErrorYlow(ii),eyh1=gr1->GetErrorYhigh(ii);
     y[mp]=y1/xL;
     eyl[mp]=sqrt(eyl1*eyl1 + y1*y1*exL*exL/xL/xL)/xL;
     eyh[mp]=sqrt(eyh1*eyh1 + y1*y1*exL*exL/xL/xL)/xL;
     // add here systematic error 
//      Double_t error = totalsyst->GetBinContent(ii+1);
//      eyl[mp]=sqrt(eyl[mp]*eyl[mp]+error*error);
//      eyh[mp]=sqrt(eyh[mp]*eyh[mp]+error*error);
    mp++;
    }    
//    cout << ii << " t = " << x[ii] << " combined " << y[ii] << " error up " << eyh[ii] << "\n";
//  cout << ii << " t = " << x[ii] << " error low " << eyl[ii] << " error high " << eyh[ii] << "\n";
       
  }
  
 TGraphAsymmErrors *gr = new TGraphAsymmErrors(mp,x,y,exl,exh,eyl,eyh);
//Double_t xmin = gr->GetXaxis()->GetXmin(), xmax = gr->GetXaxis()->GetXmax();
//cout << " Min = " << xmin << " Max = " << xmax << "\n";
 return gr;

}

TGraphAsymmErrors* Divide_Graph(TGraphAsymmErrors *gr1, TGraphAsymmErrors *gr2){


Int_t np = gr1->GetN();


Double_t x[nf],y[nf],exl[nf],exh[nf],eyl[nf],eyh[nf];
Double_t x1,x2,y1,y2,eyh1,eyl1,eyh2,eyl2;

Int_t mp=0;

  for (Int_t ii = 0; ii < np; ii++) {
    gr1->GetPoint(ii,x1,y1);
    gr2->GetPoint(ii,x2,y2);
    if(y1>0 && y2>0){
     x[mp]=x1;
     exl[mp]=gr1->GetErrorXlow(ii),exh[mp]=gr1->GetErrorXhigh(ii);
     eyl1=gr1->GetErrorYlow(ii),eyh1=gr1->GetErrorYhigh(ii);
     eyl2=gr2->GetErrorYlow(ii),eyh2=gr2->GetErrorYhigh(ii);
     y[mp]=y1/y2;
     eyl[mp]=sqrt(eyl1*eyl1/y2/y2 + eyl2*eyl2*y1*y1/y2/y2/y2/y2);
     eyh[mp]=sqrt(eyh1*eyh1/y2/y2 + eyh2*eyh2*y1*y1/y2/y2/y2/y2);     
    mp++;
    }    
//    cout << ii << " t = " << x[ii] << " combined " << y[ii] << " error up " << eyh[ii] << "\n";
//  cout << ii << " t = " << x[ii] << " error low " << eyl[ii] << " error high " << eyh[ii] << "\n";
       
  }
  
 TGraphAsymmErrors *gr = new TGraphAsymmErrors(mp,x,y,exl,exh,eyl,eyh);
// cout << "Points in Divide_graph = " << mp << "\n";
//Double_t xmin = gr->GetXaxis()->GetXmin(), xmax = gr->GetXaxis()->GetXmax();
//cout << " Min = " << xmin << " Max = " << xmax << "\n";
 return gr;

}

void observed_elastic(Double_t *zs, Double_t *dzslow,Double_t *dzshigh,TGraphAsymmErrors *gr){
// returns the observed=measured elastic cross section from the combined final dSigma_el/dt

  Double_t result=0.0; // 
  Double_t xs=0.0,t=0,xel=0.,elow=0.,ehigh=0.0,dxslow=0.0,dxshigh=0.0;
  Int_t np = gr->GetN();
  Int_t npoint=0;

//if(np != nf) {
//  cout << "Graph point mismatch nf = " << nf << " np = " << np << "\n";
//  return result;
//}
  
  for (Int_t ii = 0; ii < np; ii++) {
    gr->GetPoint(ii,t,xel);
    if(xel>0.){
     npoint++;
     Double_t dt=tf[ii+1]-tf[ii];
     elow=gr->GetErrorYlow(ii),ehigh=gr->GetErrorYhigh(ii);
     xs=xs+xel*dt;
//     cout << ii << " t = " << t << " Integrated elastic cross section = " << xs << " xs = " << xel << " error = " << ehigh << "\n";
     dxslow=dxslow+elow*elow*dt*dt;
     dxshigh=dxshigh+ehigh*ehigh*dt*dt; 
    }
  }
  dxslow=sqrt(dxslow),dxshigh=sqrt(dxshigh);
  *zs=xs;
  *dzshigh=dxshigh;
  *dzslow=dxslow;
}

TGraphAsymmErrors* resifit(TGraphAsymmErrors *gr, TF1 *ft){


Int_t np = gr->GetN();

//if(np != nf) {
//  cout << "Arm combination mismatch nf = " << nf << " np = " << np << "\n";
//  return gr;
//}

Double_t x[nf],y[nf],exl[nf],exh[nf],eyl[nf],eyh[nf],fit[nf];
        Double_t low,high,width;

//  cout << " Point in resifit = " << np << "\n";
  for (Int_t ii = 0; ii < np; ii++) {
     gr->GetPoint(ii,x[ii],y[ii]);
     exl[ii]=gr->GetErrorXlow(ii),exh[ii]=gr->GetErrorXhigh(ii);
     eyl[ii]=gr->GetErrorYlow(ii),eyh[ii]=gr->GetErrorYhigh(ii);
    if(y[ii]>0.){
     low=tf[ii];
     high=tf[ii+1];
     width=tf[ii+1]-tf[ii];     
     fit[ii]=ft->Integral(low,high)/width;
     // that's actually crap
//     eyl[ii]=(fit[ii]+2.*y[ii])/y[ii]/y[ii]*eyl[ii];
//     eyh[ii]=(fit[ii]+2.*y[ii])/y[ii]/y[ii]*eyh[ii];
     eyl[ii]=fit[ii]/y[ii]/y[ii]*eyl[ii];
     eyh[ii]=fit[ii]/y[ii]/y[ii]*eyh[ii];
     fit[ii]=(fit[ii]-y[ii])/y[ii];
    }   
    else{
      fit[ii]=0,eyl[ii]=0,eyh[ii]=0;
} 
//    cout << ii << " t = " << x[ii] << " error low " << exl[ii] << " error high " << exh[ii] << "\n";
       
  }
  
 TGraphAsymmErrors *fr = new TGraphAsymmErrors(np,x,fit,exl,exh,eyl,eyh);
//Double_t xmin = fr->GetXaxis()->GetXmin(), xmax = fr->GetXaxis()->GetXmax();
//cout << " Min = " << xmin << " Max = " << xmax << "\n";
 return fr;

}

TGraphAsymmErrors* resihist(TGraphAsymmErrors *gr, TH1D *hr, TF1 *ft){


Int_t np = gr->GetN();

//if(np != nf) {
//  cout << "Arm combination mismatch nf = " << nf << " np = " << np << "\n";
//  return gr;
//}

Double_t x[nf],y[nf],exl[nf],exh[nf],eyl[nf],eyh[nf],fit[nf],ytmp;

//  cout << " Point in resifit = " << np << "\n";
  for (Int_t ii = 0; ii < np; ii++) {
    gr->GetPoint(ii,x[ii],ytmp);
     y[ii]=hr->GetBinContent(ii+1);
//     cout << ii << " before = " << ytmp << " after = " << y[ii] << "\n";
     exl[ii]=gr->GetErrorXlow(ii),exh[ii]=gr->GetErrorXhigh(ii);
     eyl[ii]=gr->GetErrorYlow(ii),eyh[ii]=gr->GetErrorYhigh(ii);
    if(y[ii]>0.){
     fit[ii]=ft->Eval(x[ii],0,0);
     // that's actually crap
//     eyl[ii]=(fit[ii]+2.*y[ii])/y[ii]/y[ii]*eyl[ii];
//     eyh[ii]=(fit[ii]+2.*y[ii])/y[ii]/y[ii]*eyh[ii];
     eyl[ii]=fit[ii]/y[ii]/y[ii]*eyl[ii];
     eyh[ii]=fit[ii]/y[ii]/y[ii]*eyh[ii];
     fit[ii]=(fit[ii]-y[ii])/y[ii];
    }   
    else{
      fit[ii]=0,eyl[ii]=0,eyh[ii]=0;
} 
//    cout << ii << " t = " << x[ii] << " error low " << exl[ii] << " error high " << exh[ii] << "\n";
       
  }
  
 TGraphAsymmErrors *fr = new TGraphAsymmErrors(np,x,fit,exl,exh,eyl,eyh);
//Double_t xmin = fr->GetXaxis()->GetXmin(), xmax = fr->GetXaxis()->GetXmax();
//cout << " Min = " << xmin << " Max = " << xmax << "\n";
 return fr;

}


TGraphErrors *thprep(TH1D *raw, TProfile *tr){
// function taking theory t-spectrum and replacing the t-bin centres by the mean t in each bin (exp. distribution) 

Int_t nbins = raw->GetNbinsX();
Double_t x[nf],y[nf],ex[nf],ey[nf];

  for (int iBin = 1; iBin <= nbins; ++iBin){
   
   Double_t t=raw->GetBinContent(iBin);
   Double_t dt=raw->GetBinError(iBin);
   Double_t ttrue=tr->GetBinContent(iBin);
   
   y[iBin-1]=t;
   ey[iBin-1]=dt;
   x[iBin-1]=ttrue;
   ex[iBin-1]=0.0*(raw->GetBinWidth(iBin));    
  }
TGraphErrors *gr = new TGraphErrors(nbins,x,y,ex,ey);
return gr;
}

// Unfolded version data, acceptance , cut efficiency reco efficiency, t-center correction
TGraphAsymmErrors* tprep(TH1D *raw, TEfficiency *acc,TEfficiency *eff, Int_t iarm, TProfile *prot){
raw->Sumw2();

Int_t nbins = raw->GetNbinsX();
Double_t x[nf]={0},y[nf]={0},exl[nf]={0},exh[nf]={0},eyl[nf]={0},eyh[nf]={0};
Double_t effBin,errUp,errLo,centrT;

//cout << " fitprep : Nbins = " << nbins << "\n";


  for (int iBin = 1; iBin <= nbins; ++iBin) {
   Int_t j=iBin-1;
   
   Double_t t=raw->GetBinContent(iBin);
   Double_t dt=raw->GetBinError(iBin);
   Double_t a=acc->GetEfficiency(iBin);
   Double_t ap=acc->GetEfficiencyErrorUp(iBin);
   Double_t am=acc->GetEfficiencyErrorLow(iBin);
   Double_t e=eff->GetEfficiency(iBin);
   Double_t ep=eff->GetEfficiencyErrorUp(iBin);
   Double_t em=eff->GetEfficiencyErrorLow(iBin);
   Double_t r=eps[iarm],rp=deps[iarm],rm=deps[iarm];
   
   Double_t tc=raw->GetBinCenter(iBin);
   Double_t tb=raw->GetBinWidth(iBin);
   Double_t tl=tc-tb*0.5, th=tc+tb*0.5;
//   cout << " t " << tl << " - " << th << " acceptance = " << a << "\n";   
   
//   cout << iBin << " t = " << t << " dt = " << dt << " a = " << a << " da = " << ap << "\n";
//    cout << " Arm = " << iarm << " tprep bin = " << iBin-1 << " a = " << a << " e = " << e << " r = " << r << "\n";    
    ep=sqrt(r*r*ep*ep+e*e*rp*rp);
    em=sqrt(r*r*em*em+e*e*rm*rm);
    e=e*r;
   
    y[iBin-1] = t;
    eyl[iBin-1] = dt;
    eyh[iBin-1] = dt;
    x[iBin-1] = prot->GetBinContent(iBin);
    exh[iBin-1] = 0.5*(raw->GetBinWidth(iBin));
    exl[iBin-1] = 0.5*(raw->GetBinWidth(iBin));
//    cout << " Arm = " << iarm << " tprep bin = " << iBin-1 << " t = " << x[iBin-1] << " dsigma/dt = " << y[iBin-1] << "\n";    
   
   if(a > 0 && e > 0){
    Double_t d = e*a;
    y[iBin-1] = t/d/xL;
    if(y[iBin-1]<0) cout << "Issue: negative cross section = " << y[iBin-1] << " Events = " << t << "\n";
    Double_t dp=sqrt(a*a*ep*ep+e*e*ap*ap); 
    Double_t dm=sqrt(a*a*em*em+e*e*am*am);
    eyh[iBin-1] = sqrt(dt*dt/d/d + t*t*dp*dp/d/d/d/d);
    eyl[iBin-1] = sqrt(dt*dt/d/d + t*t*dm*dm/d/d/d/d);   
    eyh[iBin-1]=sqrt(eyh[iBin-1]*eyh[iBin-1]/xL/xL + y[iBin-1]*y[iBin-1]*exL*exL/xL/xL/xL/xL);
    eyl[iBin-1]=sqrt(eyl[iBin-1]*eyl[iBin-1]/xL/xL + y[iBin-1]*y[iBin-1]*exL*exL/xL/xL/xL/xL);
    
//    cout << " tprep bin = " << iBin-1 << " error " << max(eyh[iBin-1],eyl[iBin-1])  << "\n";
//    cout << " tprep bin = " << iBin-1 << " error first " << sqrt(dt*dt/d/d/xL/xL)  << " second " << sqrt(t*t*dp*dp/d/d/d/d/xL/xL) << " third " << 
//    sqrt(y[iBin-1]*y[iBin-1]*exL*exL/xL/xL/xL/xL) << "\n";
  }
  else{
   y[iBin-1]=0.0,eyh[iBin-1]=0.0,eyl[iBin-1]=0.0;   
  }
//  cout << iBin-1 << " gr x = " << x[iBin-1] << " y = " << y[iBin-1] << " error = " << max(eyh[iBin-1],eyl[iBin-1]) << "\n";
//  cout << iBin << " t = " << tt  << " raw " << t << " Background = " << b << " acceptance = " << a << " efficiency = " << e << " corrected = " << y[iBin-1] <<"\n";
}

TGraphAsymmErrors *gr = new TGraphAsymmErrors(nbins,x,y,exl,exh,eyl,eyh);
//Double_t xmin = gr->GetXaxis()->GetXmin(), xmax = gr->GetXaxis()->GetXmax();
//cout << " Min = " << xmin << " Max = " << xmax << "\n";
return gr;
}


// Unfolded version data, acceptance , cut efficiency, t-dependent reco efficiency, t-center correction
TGraphAsymmErrors* tprep_tdep(TH1D *raw, TEfficiency *acc,TEfficiency *eff, Int_t iarm, TProfile *prot, TF1 *emodel){
raw->Sumw2();

Int_t nbins = raw->GetNbinsX();
Double_t x[nf]={0},y[nf]={0},exl[nf]={0},exh[nf]={0},eyl[nf]={0},eyh[nf]={0};
Double_t effBin,errUp,errLo,centrT;

//cout << " fitprep : Nbins = " << nbins << "\n";


  for (int iBin = 1; iBin <= nbins; ++iBin) {
   Int_t j=iBin-1;
   
   Double_t t=raw->GetBinContent(iBin);
   Double_t dt=raw->GetBinError(iBin);
   Double_t a=acc->GetEfficiency(iBin);
   Double_t ap=acc->GetEfficiencyErrorUp(iBin);
   Double_t am=acc->GetEfficiencyErrorLow(iBin);
   Double_t e=eff->GetEfficiency(iBin);
   Double_t ep=eff->GetEfficiencyErrorUp(iBin);
   Double_t em=eff->GetEfficiencyErrorLow(iBin);
   Double_t r=emodel->Eval(prot->GetBinContent(iBin));
   Double_t rp=deps[iarm],rm=deps[iarm];
   
   Double_t tc=raw->GetBinCenter(iBin);
   Double_t tb=raw->GetBinWidth(iBin);
   Double_t tl=tc-tb*0.5, th=tc+tb*0.5;
   
    ep=sqrt(r*r*ep*ep+e*e*rp*rp);
    em=sqrt(r*r*em*em+e*e*rm*rm);
    e=e*r;
   
    y[iBin-1] = t;
    eyl[iBin-1] = dt;
    eyh[iBin-1] = dt;
    x[iBin-1] = prot->GetBinContent(iBin);
    exh[iBin-1] = 0.5*(raw->GetBinWidth(iBin));
    exl[iBin-1] = 0.5*(raw->GetBinWidth(iBin));
//    cout << " Arm = " << iarm << " tprep bin = " << iBin-1 << " t = " << x[iBin-1] << " dsigma/dt = " << y[iBin-1] << "\n";    
   
   if(a > 0 && e > 0){
    Double_t d = e*a;
    y[iBin-1] = t/d/xL;
    if(y[iBin-1]<0) cout << "Issue: negative cross section = " << y[iBin-1] << " Events = " << t << "\n";
    Double_t dp=sqrt(a*a*ep*ep+e*e*ap*ap); 
    Double_t dm=sqrt(a*a*em*em+e*e*am*am);
    eyh[iBin-1] = sqrt(dt*dt/d/d + t*t*dp*dp/d/d/d/d);
    eyl[iBin-1] = sqrt(dt*dt/d/d + t*t*dm*dm/d/d/d/d);   
    eyh[iBin-1]=sqrt(eyh[iBin-1]*eyh[iBin-1]/xL/xL + y[iBin-1]*y[iBin-1]*exL*exL/xL/xL/xL/xL);
    eyl[iBin-1]=sqrt(eyl[iBin-1]*eyl[iBin-1]/xL/xL + y[iBin-1]*y[iBin-1]*exL*exL/xL/xL/xL/xL);
    
  }
  else{
   y[iBin-1]=0.0,eyh[iBin-1]=0.0,eyl[iBin-1]=0.0;   
  }
}

TGraphAsymmErrors *gr = new TGraphAsymmErrors(nbins,x,y,exl,exh,eyl,eyh);
return gr;
}

// not unfolded, no acceptance nor efficiency but cut efficiency reco efficiency, t-center correction
TGraphAsymmErrors* fold_prep(TH1D *raw, TEfficiency *acc,TEfficiency *eff, Int_t iarm, TProfile *prot){
raw->Sumw2();

Int_t nbins = raw->GetNbinsX();
Double_t x[nf],y[nf],exl[nf],exh[nf],eyl[nf],eyh[nf];
Double_t effBin,errUp,errLo,centrT;

//cout << " fitprep : Nbins = " << nbins << "\n";


  for (int iBin = 1; iBin <= nbins; ++iBin) {
   Int_t j=iBin-1;
   
   Double_t t=raw->GetBinContent(iBin);
   Double_t dt=raw->GetBinError(iBin);
   Double_t r=eps[iarm],rp=deps[iarm],rm=deps[iarm];
   
   Double_t tc=raw->GetBinCenter(iBin);
   Double_t tb=raw->GetBinWidth(iBin);
   Double_t tl=tc-tb*0.5, th=tc+tb*0.5;
//   cout << " t " << tl << " - " << th << " acceptance = " << a << "\n";   
   
//   cout << iBin << " t = " << t << " dt = " << dt << " a = " << a << " da = " << ap << "\n";
   
    y[iBin-1] = t;
    eyl[iBin-1] = dt;
    eyh[iBin-1] = dt;
    x[iBin-1] = prot->GetBinContent(iBin);
    exh[iBin-1] = 0.5*(raw->GetBinWidth(iBin));
    exl[iBin-1] = 0.5*(raw->GetBinWidth(iBin));
   
   if(r>0){
    y[iBin-1] = t/r;
    Double_t dr=rp; 
    eyh[iBin-1] = sqrt(dt*dt/r/r + t*t*dr*dr/r/r/r/r);
    eyl[iBin-1] = sqrt(dt*dt/r/r + t*t*dr*dr/r/r/r/r);    
  }
//  cout << iBin-1 << " gr x = " << x[iBin-1] << "\n";
//  cout << iBin << " t = " << tt  << " raw " << t << " Background = " << b << " acceptance = " << a << " efficiency = " << e << " corrected = " << y[iBin-1] <<"\n";
}

TGraphAsymmErrors *gr = new TGraphAsymmErrors(nbins,x,y,exl,exh,eyl,eyh);
//Double_t xmin = gr->GetXaxis()->GetXmin(), xmax = gr->GetXaxis()->GetXmax();
//cout << " Min = " << xmin << " Max = " << xmax << "\n";
return gr;
}


// combine statistical covariance matrix from unfolding with diagonal uncorrelated statistical errors e.g. from acceptance 
TMatrixD cprep(TH2D *cov,TH1D *raw, TEfficiency *acc,TEfficiency *eff, Int_t iarm, Double_t zlow, Double_t zhigh){
TMatrixD CM(nf,nf);
Double_t f[nf]={0},df[nf]={0},t[nf]={0.0};

   
  Int_t ndim=0;
  for (int iBin = 1; iBin <= nf; ++iBin) {
   
   t[iBin-1]=raw->GetBinContent(iBin);
   Double_t xlow=raw->GetBinLowEdge(iBin);
   Double_t xhigh=(raw->GetBinLowEdge(iBin))+(raw->GetBinWidth(iBin));
   Double_t a=acc->GetEfficiency(iBin);
   Double_t ap=acc->GetEfficiencyErrorUp(iBin);
   Double_t am=acc->GetEfficiencyErrorLow(iBin);
   Double_t da=(ap+am)/2.;
   Double_t e=eff->GetEfficiency(iBin);
   Double_t ep=eff->GetEfficiencyErrorUp(iBin);
   Double_t em=eff->GetEfficiencyErrorLow(iBin);
   Double_t de=(ep+em)/2.;
   Double_t r=eps[iarm],dr=deps[iarm];
   
//   cout << "cprep arm = " << iarm << " eps = " << eps[iarm] << " luminosity = " << xL << "\n";
   if(xlow >= zlow && xhigh <= zhigh) ndim++; // finding dimension of reduced covariance matrix inside fit range
 
   f[iBin-1] = a*e*r; // incorporating all corrections, acceptance, cut efficiency, reconstruction effciiency
   df[iBin-1] = sqrt(da*da*e*e*r*r + de*de*a*a*r*r + dr*dr*a*a*e*e ); // the error square on f
  }
  
  for (int iBin = 1; iBin <= nf; ++iBin) {
   // the corrected t value is t/f
   if(f[iBin-1]!=0.0){
   for (int jj = 1; jj <= nf; ++jj) {
    CM[iBin-1][jj-1]=0.0;
     if(f[jj-1]!=0.0){
       CM[iBin-1][jj-1]=(cov->GetBinContent(iBin,jj))/f[iBin-1]/f[jj-1]/xL/xL;
//       cout << " Cell i = " << iBin-1 << " j = " << jj-1 << " Covariance = " << CM[iBin-1][jj-1] << "\n";
//       cout << "Symmetry test raw covariance on pair " << iBin << " , " << jj << " (i,j) = " << cov->GetBinContent(iBin,jj) << " (j,i) = " << cov->GetBinContent(jj,iBin) << "\n";
      if(iBin == jj){
//       cout << "Cprep arms bin = " << iBin-1 << " Cov diag error first = " << sqrt(CM[iBin-1][jj-1]) << " second = " << sqrt(t[iBin-1]*t[jj-1]*df[iBin-1]*df[jj-1]/f[iBin-1]/f[iBin-1]/f[jj-1]/f[jj-1]/xL/xL) << " third = " << sqrt(t[iBin-1]*t[jj-1]*exL*exL/f[iBin-1]/f[jj-1]/xL/xL/xL/xL) << "\n";
       CM[iBin-1][jj-1]=CM[iBin-1][jj-1] + t[iBin-1]*t[jj-1]*df[iBin-1]*df[jj-1]/f[iBin-1]/f[iBin-1]/f[jj-1]/f[jj-1]/xL/xL + t[iBin-1]*t[jj-1]*exL*exL/f[iBin-1]/f[jj-1]/xL/xL/xL/xL;
//	cout << " Cprep bin = " << iBin-1 << " error = " << sqrt(CM[iBin-1][jj-1]) << "\n";
//       CM[iBin-1][jj-1]=t*t*df*df/f/f/f/f/xL/xL;
//       cout << "Cprep arms bin = " << iBin-1 << " Cov diag error = " <<  sqrt(cov->GetBinContent(iBin,jj)) << "\n";
      }
     }
    }
   }    
  }
//  cout << iBin-1 << " gr x = " << x[iBin-1] << "\n";
//  cout << iBin << " t = " << tt  << " raw " << t << " Background = " << b << " acceptance = " << a << " efficiency = " << e << " corrected = " << y[iBin-1] <<"\n";
// cout << " Dimension of covariance matrix = " << ndim << "\n";
//  for (int iBin = 0; iBin < nf; ++iBin) {
//   for (int jBin = 0; jBin < nf; ++jBin) {
//     cout << " Symmetry test cov mat " << iBin << " , " << jBin << " (i,j) = " << CM[iBin][jBin] << " (j,i) = " << CM[jBin][iBin] << "\n";
//   }
//  }

 TMatrixD Cnew(ndim,ndim);
 Int_t nn1=0,nn2=0;
 for (Int_t ii = 0; ii < nf; ++ii) {
  Double_t xlow1=raw->GetBinLowEdge(ii+1);
  Double_t xhigh1=(raw->GetBinLowEdge(ii+1))+(raw->GetBinWidth(ii+1));
  if(xlow1>=zlow && xhigh1 <= zhigh){
//   cout << " Filling element ii = " << nn1 << " jj " << nn2 << " Bin 1 from " <<  xlow1 << " to " << xhigh1 << "\n";
    nn2=0;
    for (Int_t jj = 0; jj < nf; ++jj) {
     Double_t xlow2=raw->GetBinLowEdge(jj+1);
     Double_t xhigh2=(raw->GetBinLowEdge(jj+1))+(raw->GetBinWidth(jj+1));
     if(xlow2>=zlow && xhigh2 <= zhigh){
      Cnew[nn1][nn2]=CM[ii][jj];
      nn2++;
    }
   }
   nn1++;
  }
 }
  
//  cout << "Cprep diag error first bin = " << Cnew[0][0] << "\n";
// cout << " Matrix determinant = " << Cnew.Determinant() << "\n";


return Cnew;
}

// combine statistical covariance matrix from unfolding with diagonal uncorrelated statistical errors e.g. from acceptance. Here with t-dependent reconstruction efficiency 
TMatrixD cprep_tdep(TH2D *cov,TH1D *raw, TEfficiency *acc,TEfficiency *eff, Int_t iarm, Double_t zlow, Double_t zhigh, TF1 *emodel){
TMatrixD CM(nf,nf);
Double_t f[nf]={0},df[nf]={0},t[nf]={0.0};

   
  Int_t ndim=0;
  for (int iBin = 1; iBin <= nf; ++iBin) {
   
   t[iBin-1]=raw->GetBinContent(iBin);
   Double_t xlow=raw->GetBinLowEdge(iBin);
   Double_t xhigh=(raw->GetBinLowEdge(iBin))+(raw->GetBinWidth(iBin));
   Double_t a=acc->GetEfficiency(iBin);
   Double_t ap=acc->GetEfficiencyErrorUp(iBin);
   Double_t am=acc->GetEfficiencyErrorLow(iBin);
   Double_t da=(ap+am)/2.;
   Double_t e=eff->GetEfficiency(iBin);
   Double_t ep=eff->GetEfficiencyErrorUp(iBin);
   Double_t em=eff->GetEfficiencyErrorLow(iBin);
   Double_t de=(ep+em)/2.;
   Double_t dr=deps[iarm];
   Double_t r=emodel->Eval((tf[iBin-1]+tf[iBin])*0.5);
   
//   cout << "cprep arm = " << iarm << " eps = " << eps[iarm] << " luminosity = " << xL << "\n";
   if(xlow >= zlow && xhigh <= zhigh) ndim++; // finding dimension of reduced covariance matrix inside fit range
 
   f[iBin-1] = a*e*r; // incorporating all corrections, acceptance, cut efficiency, reconstruction effciiency
   df[iBin-1] = sqrt(da*da*e*e*r*r + de*de*a*a*r*r + dr*dr*a*a*e*e ); // the error square on f
  }
  
  for (int iBin = 1; iBin <= nf; ++iBin) {
   // the corrected t value is t/f
   if(f[iBin-1]!=0.0){
   for (int jj = 1; jj <= nf; ++jj) {
    CM[iBin-1][jj-1]=0.0;
     if(f[jj-1]!=0.0){
       CM[iBin-1][jj-1]=(cov->GetBinContent(iBin,jj))/f[iBin-1]/f[jj-1]/xL/xL;
//       cout << " Cell i = " << iBin-1 << " j = " << jj-1 << " Covariance = " << CM[iBin-1][jj-1] << "\n";
//       cout << "Symmetry test raw covariance on pair " << iBin << " , " << jj << " (i,j) = " << cov->GetBinContent(iBin,jj) << " (j,i) = " << cov->GetBinContent(jj,iBin) << "\n";
      if(iBin == jj){
//       cout << "Cprep arms bin = " << iBin-1 << " Cov diag error first = " << sqrt(CM[iBin-1][jj-1]) << " second = " << sqrt(t[iBin-1]*t[jj-1]*df[iBin-1]*df[jj-1]/f[iBin-1]/f[iBin-1]/f[jj-1]/f[jj-1]/xL/xL) << " third = " << sqrt(t[iBin-1]*t[jj-1]*exL*exL/f[iBin-1]/f[jj-1]/xL/xL/xL/xL) << "\n";
       CM[iBin-1][jj-1]=CM[iBin-1][jj-1] + t[iBin-1]*t[jj-1]*df[iBin-1]*df[jj-1]/f[iBin-1]/f[iBin-1]/f[jj-1]/f[jj-1]/xL/xL + t[iBin-1]*t[jj-1]*exL*exL/f[iBin-1]/f[jj-1]/xL/xL/xL/xL;
//	cout << " Cprep bin = " << iBin-1 << " error = " << sqrt(CM[iBin-1][jj-1]) << "\n";
//       CM[iBin-1][jj-1]=t*t*df*df/f/f/f/f/xL/xL;
//       cout << "Cprep arms bin = " << iBin-1 << " Cov diag error = " <<  sqrt(cov->GetBinContent(iBin,jj)) << "\n";
      }
     }
    }
   }    
  }
//  cout << iBin-1 << " gr x = " << x[iBin-1] << "\n";
//  cout << iBin << " t = " << tt  << " raw " << t << " Background = " << b << " acceptance = " << a << " efficiency = " << e << " corrected = " << y[iBin-1] <<"\n";
// cout << " Dimension of covariance matrix = " << ndim << "\n";
//  for (int iBin = 0; iBin < nf; ++iBin) {
//   for (int jBin = 0; jBin < nf; ++jBin) {
//     cout << " Symmetry test cov mat " << iBin << " , " << jBin << " (i,j) = " << CM[iBin][jBin] << " (j,i) = " << CM[jBin][iBin] << "\n";
//   }
//  }

 TMatrixD Cnew(ndim,ndim);
 Int_t nn1=0,nn2=0;
 for (Int_t ii = 0; ii < nf; ++ii) {
  Double_t xlow1=raw->GetBinLowEdge(ii+1);
  Double_t xhigh1=(raw->GetBinLowEdge(ii+1))+(raw->GetBinWidth(ii+1));
  if(xlow1>=zlow && xhigh1 <= zhigh){
//   cout << " Filling element ii = " << nn1 << " jj " << nn2 << " Bin 1 from " <<  xlow1 << " to " << xhigh1 << "\n";
    nn2=0;
    for (Int_t jj = 0; jj < nf; ++jj) {
     Double_t xlow2=raw->GetBinLowEdge(jj+1);
     Double_t xhigh2=(raw->GetBinLowEdge(jj+1))+(raw->GetBinWidth(jj+1));
     if(xlow2>=zlow && xhigh2 <= zhigh){
      Cnew[nn1][nn2]=CM[ii][jj];
      nn2++;
    }
   }
   nn1++;
  }
 }
  
//  cout << "Cprep diag error first bin = " << Cnew[0][0] << "\n";
// cout << " Matrix determinant = " << Cnew.Determinant() << "\n";


return Cnew;
}


// returns stat error for arms from covariance matrix
TH2D* dump_prep(TH2D *cov,TH1D *raw, TEfficiency *acc,TEfficiency *eff, Int_t iarm){
Double_t f[nf]={0},df[nf]={0},t[nf]={0.0};
TH2D *covn=(TH2D*)cov->Clone("covn");

   
  Int_t ndim=0;
  for (int iBin = 1; iBin <= nf; ++iBin) {
   
   t[iBin-1]=raw->GetBinContent(iBin);
   Double_t xlow=raw->GetBinLowEdge(iBin);
   Double_t xhigh=(raw->GetBinLowEdge(iBin))+(raw->GetBinWidth(iBin));
   Double_t a=acc->GetEfficiency(iBin);
   Double_t ap=acc->GetEfficiencyErrorUp(iBin);
   Double_t am=acc->GetEfficiencyErrorLow(iBin);
   Double_t da=(ap+am)/2.;
   Double_t e=eff->GetEfficiency(iBin);
   Double_t ep=eff->GetEfficiencyErrorUp(iBin);
   Double_t em=eff->GetEfficiencyErrorLow(iBin);
   Double_t de=(ep+em)/2.;
   Double_t r=eps[iarm],dr=deps[iarm];
   
   // resetting all errors except from data
   da=0.0,de=0.0,dr=0.0,exL=0.0;
   
   
//  cout << "Cprep arms bin = " << iBin-1 << " a = " << a << " e = " << e << " r = " << r << "\n";   
   
   f[iBin-1] = a*e*r; // incorporating all corrections, acceptance, cut efficiency, reconstruction effciiency
   df[iBin-1] = sqrt(da*da*e*e*r*r + de*de*a*a*r*r + dr*dr*a*a*e*e ); // the error square on f
  }
  
  for (int iBin = 1; iBin <= nf; ++iBin) {
   // the corrected t value is t/f
   if(f[iBin-1]!=0.0){
   for (int jj = 1; jj <= nf; ++jj) {
    covn->SetBinContent(iBin,jj,0.0);
     if(f[jj-1]!=0.0){
       covn->SetBinContent(iBin,jj,(cov->GetBinContent(iBin,jj))/f[iBin-1]/f[jj-1]/xL/xL);
//       cout << " Cell i = " << iBin-1 << " j = " << jj-1 << " Covariance = " << CM[iBin-1][jj-1] << "\n";
//       cout << "Symmetry test raw covariance on pair " << iBin << " , " << jj << " (i,j) = " << cov->GetBinContent(iBin,jj) << " (j,i) = " << cov->GetBinContent(jj,iBin) << "\n";
      if(iBin == jj){
//       cout << "Cprep arms bin = " << iBin-1 << " Cov diag error first = " << sqrt(CM[iBin-1][jj-1]) << " second = " << sqrt(t[iBin-1]*t[jj-1]*df[iBin-1]*df[jj-1]/f[iBin-1]/f[iBin-1]/f[jj-1]/f[jj-1]/xL/xL) << " third = " << sqrt(t[iBin-1]*t[jj-1]*exL*exL/f[iBin-1]/f[jj-1]/xL/xL/xL/xL) << "\n";
       covn->SetBinContent(iBin,jj,
			   (cov->GetBinContent(iBin,jj))/f[iBin-1]/f[jj-1]/xL/xL + 
			   t[iBin-1]*t[jj-1]*df[iBin-1]*df[jj-1]/f[iBin-1]/f[iBin-1]/f[jj-1]/f[jj-1]/xL/xL + 
			   t[iBin-1]*t[jj-1]*exL*exL/f[iBin-1]/f[jj-1]/xL/xL/xL/xL);
//	cout << " Cprep bin = " << iBin-1 << " error = " << sqrt(CM[iBin-1][jj-1]) << "\n";
//       CM[iBin-1][jj-1]=t*t*df*df/f/f/f/f/xL/xL;
//       cout << "Cprep arms bin = " << iBin-1 << " Cov diag error = " <<  sqrt(cov->GetBinContent(iBin,jj)) << "\n";
      }
     }
    }
   }    
  }

return covn;
}


// reduce systematic covariance matrix to bins inside fit range 
TMatrixD cpsys(TH2D *cov,Double_t zlow, Double_t zhigh){
   
  Int_t ndim=0;
  for (int iBin = 1; iBin <= nf; ++iBin) {
   Int_t j=iBin-1;  
   Double_t xlow=tf[j];
   Double_t xhigh=tf[j+1];
   if(xlow >= zlow && xhigh <= zhigh) ndim++; // finding dimension of reduced covariance matrix inside fit range
  }

 TMatrixD Cnew(ndim,ndim);
 Int_t nn1=0,nn2=0;
 for (Int_t ii = 0; ii < nf; ++ii) {
  Double_t xlow1=tf[ii];
  Double_t xhigh1=tf[ii+1];
  if(xlow1>=zlow && xhigh1 <= zhigh){
    nn2=0;
    for (Int_t jj = 0; jj < nf; ++jj) {
     Double_t xlow2=tf[jj];
     Double_t xhigh2=tf[jj+1];
     if(xlow2>=zlow && xhigh2 <= zhigh){
      Cnew[nn1][nn2]=cov->GetBinContent(ii+1,jj+1);
//      cout << " Filling element ii = " << nn1 << " jj " << nn2 << " Bin 1 from " <<  xlow1 << " to " << xhigh1 << "\n";
      nn2++;
    }
   }
   nn1++;
  }
 }
  
// cout << " Matrix determinant = " << Cnew.Determinant() << "\n";


return Cnew;
}


void change_sys(TGraphAsymmErrors *gr, TH1D *h){

  Int_t np = gr->GetN();
//  cout << " Npoints = " << np << " Nbins = " << nf << "\n";
  for (Int_t ii = 0; ii < np; ii++) {
//    cout << " ii = " << ii << " Error = " << h->GetBinError(ii+2) << "\n";
    gr->SetPointEYhigh(ii,h->GetBinError(ii+2));
    gr->SetPointEYlow(ii,h->GetBinError(ii+2));
  }    
 return;
}


TGraphAsymmErrors* grerror(TGraphAsymmErrors *gr){
  // given differential elastic cross section in gr, returns error in percent in fr

  Int_t np = gr->GetN();
  Double_t x[nf],y[nf],exl[nf],exh[nf],eyl[nf],eyh[nf],fit[nf];

  for (Int_t ii = 0; ii < np; ii++) {
    gr->GetPoint(ii,x[ii],y[ii]);
     exl[ii]=gr->GetErrorXlow(ii),exh[ii]=gr->GetErrorXhigh(ii);
     eyl[ii]=gr->GetErrorYlow(ii),eyh[ii]=gr->GetErrorYhigh(ii);
    if(y[ii]>0.){
     eyl[ii]=100.*eyl[ii]/y[ii];
     eyh[ii]=100.*eyh[ii]/y[ii];
     y[ii]=0.0;
    }  
//    cout << ii << " x = " << x[ii] << " y = " << y[ii] << " error = " << eyh[ii] << "\n";
  }
  
 TGraphAsymmErrors *fr = new TGraphAsymmErrors(np,x,y,exl,exh,eyl,eyh);
 return fr;

}

TH1D* adjust_N(TGraphAsymmErrors *gr, Double_t *nui, TH2D *hnui){

Int_t np = gr->GetN();
//cout << "Pull_prep number of points = " <<  np << "\n";

Double_t x,y,y0,exl,exh,eyl,eyh,th,dy;

   TH1D* pull   = new TH1D("pull",";#it{-t}[GeV^{2}];Ratio",nf,tf);

  for (Int_t ii = 0; ii < np; ii++) {
    gr->GetPoint(ii,x,y0);
    y=y0;
//    cout << " Bin " << ii << " Old y = " << y << "\n";
    exl=gr->GetErrorXlow(ii),exh=gr->GetErrorXhigh(ii);
    eyl=gr->GetErrorYlow(ii),eyh=gr->GetErrorYhigh(ii);
    dy=(eyl+eyh)*0.5;
    for(Int_t kk=1; kk<24; kk++){
     if(kk!=21 && kk!=20){
       y=y-nui[kk-1]*(hnui->GetBinContent(kk,ii+1)); //modifiers to data
     }
    }
    pull->SetBinContent(ii+1,y);
   
  }
  
return pull;
}

TGraphAsymmErrors* adjust_G(TGraphAsymmErrors *gr, Double_t *nui, TH2D *hnui){

  Int_t np = gr->GetN();
  Double_t x[nf],y[nf]={0.0},exl[nf],exh[nf],eyl[nf],eyh[nf];
  Double_t y0;


  for (Int_t ii = 0; ii < np; ii++) {
    gr->GetPoint(ii,x[ii],y0);
    y[ii]=y0;
//    cout << " Bin " << ii << " Old y = " << y << "\n";
    exl[ii]=gr->GetErrorXlow(ii),exh[ii]=gr->GetErrorXhigh(ii);
    eyl[ii]=gr->GetErrorYlow(ii),eyh[ii]=gr->GetErrorYhigh(ii);
    for(Int_t kk=1; kk<24; kk++){
     if(kk!=21 && kk!=20){
       y[ii]=y[ii]-nui[kk-1]*(hnui->GetBinContent(kk,ii+1)); //modifiers to data
     }
    }
  }
   TGraphAsymmErrors *fr = new TGraphAsymmErrors(np,x,y,exl,exh,eyl,eyh);
 return fr;
}

TH1D* IntegralBin(TH1D *Bin, TF1 *Func){
    // return TH1D with function Integral over bins 
    
    TH1D *NewHist = (TH1D*)Bin->Clone();     
    Int_t nbin=Bin->GetNbinsX();
    for (Int_t ii = 1; ii <= nbin; ii++) {
        Double_t low=Bin->GetBinLowEdge(ii);
        Double_t width=Bin->GetBinWidth(ii);
        Double_t high=low+width;
        Double_t xint=0.0;
        if(width>0.0) xint=Func->Integral(low,high)/width;
        NewHist->SetBinContent(ii,xint);
    }
    return NewHist;
}
