Number of events used = 26103721
processing event 3800000 Lumiblock 19
processing event 8600000 Lumiblock 13
processing event 13400000 Lumiblock 6
processing event 14400000 Lumiblock 20
processing event 17500000 Lumiblock 28
processing event 18800000 Lumiblock 10
processing event 21700000 Lumiblock 14
processing event 25800000 Lumiblock 34
 Total number of events = 26103721
 Number of events in good luminosity blocks 645893
 Number of events in high life fraction LBs 0
 Number of events with lvl1 elast_15 or 18 0
 Of which elast_15 0
 Of which elast_18 0
 Events with ALFA_ANY 0
 Number of triggered events at det 0 = 195020 reconstructed = 26283 trig+rec 26227 rec+other_trig 56
 Number of triggered events at det 1 = 1015 reconstructed = 653 trig+rec 90 rec+other_trig 563
 Number of triggered events at det 2 = 191013 reconstructed = 21333 trig+rec 21311 rec+other_trig 22
 Number of triggered events at det 3 = 257867 reconstructed = 37708 trig+rec 23325 rec+other_trig 14383
 Number of triggered events at det 4 = 185479 reconstructed = 458703 trig+rec 152241 rec+other_trig 306462
 Number of triggered events at det 5 = 349 reconstructed = 156971 trig+rec 187 rec+other_trig 156784
 Number of triggered events at det 6 = 457184 reconstructed = 80014 trig+rec 79281 rec+other_trig 733
 Number of triggered events at det 7 = 157829 reconstructed = 15162 trig+rec 12703 rec+other_trig 2459
 After trigger and reconstruction selection : 
 Golden 1 3439 % 0.532441
 Golden 2 41 % 0.0063478
 Global Golden  3480 % 0.538789
Single-side selection
Armlet 1 = 715
Armlet 2 = 4
Armlet 3 = 51988
Armlet 4 = 3632

